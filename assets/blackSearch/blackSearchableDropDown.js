﻿/*

Black Searchable DropDown 
Author: Shafikul Islam
shafikul233anik@gmail.com

Open Source

*/

function searchDropDown(searchKey, allValues, dropDownId) {
    var foundIndex = [];
    var foundedIndex = [];
    var topIn, topOp = [];
    for (var j = 0; j < allValues.length; j++) {
        var xx = (allValues[j].split("&"));

        var x = xx[1];
        var v = xx[0];

        searchKey = searchKey.toUpperCase();
        x = x.toUpperCase();

        var foundOn = x.indexOf(searchKey);
        if (foundOn > -1) {
            foundIndex.push(foundOn);
            foundedIndex.push(foundOn + "&" + v);
        }

    }

    //var topIn = Math.min.apply(Math, foundIndex);
    topIn = Math.min.apply(Math, foundIndex);

    for (var i = 0; i < foundedIndex.length; i++) {
        if (foundedIndex[i].indexOf(topIn + "&") > -1) {
            //alert(foundedIndex[i].indexOf(topIn + "&"));
            topOp.push(foundedIndex[i].substr(foundedIndex[i].indexOf("&") + 1));
        }
    }

    for (var k = 0; k < allValues.length; k++) {
        var yy = (allValues[k].split("&"));
        var y = yy[0];

        $(dropDownId + ' option[value="' + y + '"]').hide();

        for (var l = 0; l < topOp.length; l++) {
            $(dropDownId + ' option[value="' + topOp[l] + '"]').show();
        }
    }
    //alert("min val " + topOp);
    var fOp = Math.min.apply(Math, topOp);
    if (searchKey == "") {
        for (k = 0; k < allValues.length; k++) {
            yy = (allValues[k].split("&"));
            y = yy[0];

            $(dropDownId + ' option[value="' + y + '"]').show();
            return 0;
        }
        $(dropDownId).val(1);
    } else {
        $(dropDownId).val(fOp);
        return 1;
    }
    if (fOp == Infinity) {
        return 0;
    }
}

$(document).ready(function() {
	
        $(".searchable").on('click', function () {
            if ($(this).hasClass('multiselect')) {
                var hasSrBx = $(this).parent().find('.searchBox');
                $('.searchBox').hide(300);
                $('.searchBox').remove();
                if (hasSrBx.length == 1 || hasSrBx.length < 1) {

                    var allValues = $(this).find("option").text();
                    $('select').attr('size', 1);

                    $(this).attr('size', '8');

                    $(this).parent().prepend('<textarea class="form-control has-feedback-left searchBox multiSearchBox" placeholder="' + $(this).find("option:eq(0)").text() + '" />');
                    $('.searchBox').focus();

                    $('.searchBox').parent().find("select option").show();
                    $('.searchBox').parent().find("select option[value=\"0\"]").hide();
                }

                if (hasSrBx.length >= 1) {
                    $('.searchBox').hide(300);
                    $('.searchBox').remove();
                    $(this).attr('size', 8);
                }
				
            } else {
                var hasSrBx = $(this).parent().find('.searchBox');
                $('.searchBox').hide(300);
                $('.searchBox').remove();
                if (hasSrBx.length == 1 || hasSrBx.length < 1) {

                    var allValues = $(this).find("option").text();
                    $('select').attr('size', 1);

                    $(this).attr('size', '8');
					
					$(this).css('position', 'absolute');
                    
					$(this).parent().prepend('<input type="text" class="form-control has-feedback-left searchBox" placeholder="' + $(this).find("option:eq(0)").text() + '" />');
                    $('.searchBox').focus();

                    $('.searchBox').parent().find("select option").show();
                    $('.searchBox').parent().find("select option[value=\"0\"]").hide();
                }

                if (hasSrBx.length >= 1) {
                    $('.searchBox').hide(300);
                    $('.searchBox').remove();
                    $(this).attr('size', 1);
					
					$(this).css('position', 'relative');
                }
				
            }
        });

	
    $('body').on('keyup', '.searchBox', function () {
        var x = $(this).val();
        if ($(this).hasClass("searchBox")) {
            var searchKey = $(this).val();
            var dropDownId = $(this).parent().find("select").attr('id');
            dropDownId = "#" + dropDownId;
            var dF = $(dropDownId).find('option');
            var allValues = [];
            for (var i = 1; i < dF.length; i++) {
                allValues.push($(dF[i]).val() + "&" + $(dF[i]).text());
            }
            var av = searchDropDown(searchKey, allValues, dropDownId);

            if (av == 0) {
                $(dropDownId).parent().closest("button").remove();
            }
        }

    });

    $("input, textarea, button").on('click', function () {
        if (!$(this).hasClass('searchBox')) {
            $('.searchBox').parent().find("select").attr('size', 1);
            $('.searchBox').hide(300);
            $('.searchBox').remove();
            $('.searchBox').parent().find("select option[value=\"0\"]").show();
        }
    });
    
});


/*

Black Searchable DropDown 
Author: Shafikul Islam
shafikul233anik@gmail.com

Open Source

*/

