<?php

$host         = "localhost";
$username     = "root";

$password     = "";

$dbname       = "hospital";
$result_array = array();

/* Create connection */
$conn = new mysqli($host, $username, $password, $dbname);

/* Check connection  */
if ($conn->connect_error) {

    die("Connection to database failed: " . $conn->connect_error);
}

/* SQL query to get results from database */
$placeId = $_POST['place_Id'];
$sql = "SELECT * FROM ipr WHERE reg_no=".$placeId;

$result = $conn->query($sql);

/* If there are results from database push to result array */

if ($result->num_rows > 0) {

    while($row = $result->fetch_assoc()) {

        array_push($result_array, $row);

    }

}
//<?php
//
//
//
// $con;
// $user = 'root';
// $pass = '';
//
//
//
//try {
//    $this->con = new PDO('mysql:host=localhost;dbname=hospital', $this->user, $this->pass);
//    $placeId = $_POST['placeId'];
//    $stm =  $this->con->prepare("SELECT * FROM `ipr` WHERE id = :id");
//    $stm->bindValue(':id', $placeId, PDO::PARAM_STR);
//    $stm->execute();
//
//    while ($result = $stm->fetch(PDO::FETCH_ASSOC)) {
//
//        if ($placeId == $result['id']){
//            echo json_encode($result);
//        }
//    }
//
//
//} catch (PDOException $e) {
//    print "Error!: " . $e->getMessage() . "<br/>";
//    die();
//}

/* send a JSON encded array to client */
echo json_encode($result_array);

$conn->close();

?>