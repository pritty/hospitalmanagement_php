<?php

namespace App\admin\Patient_daily_expance;
if(!isset($_SESSION)){
    session_start();
}
use App\Connection;
use PDOException;
use PDO;
class Patient_daily_expance extends Connection
{
    private $regNo;
    private $reg_no;
    private $date;
    private $date_time;
    private $Ecode;
    private $Ename;
    private $description;
    private $amount;
    private $id;
    private $name;
    private $f_h_name;
    private $reg_fee;
    private $srent_fee;
    private $con_fee;
    private $op_sur_fee;
    private $op_as_fee;
    private $op_ans_fee;
    private $op_ot_fee;
    private $pathology;
    private $ecg_charge;
    private $x_ray;
    private $oxygen;
    private $monitoring;
    private $ambulance;
    private $dresing;
    private $blood;
    private $ultra;
    private $endoscopy;
    private $service;
    private $medicine;
    private $others;
    private $vat;
    private $advance;
    private $balance;

    public function set($data = array()){

        if(array_key_exists('reg_no',$data)){
            $this->reg_no = $data['reg_no'];
        }
        if(array_key_exists('date',$data)){
            $this->date = $data['date'];
        }
        if(array_key_exists('date_time',$data)){
            $this->date_time = $data['date_time'];
        }
        if(array_key_exists('Ecode',$data)){
            $this->Ecode = $data['Ecode'];
        }
        if(array_key_exists('Ename',$data)){
            $this->Ename = $data['Ename'];
        }
        if(array_key_exists('description',$data)){
            $this->description = $data['description'];
        }
        if(array_key_exists('amount',$data)){
            $this->amount = $data['amount'];
        }
        if(array_key_exists('id',$data)){
            $this->id = $data['id'];
        }
        return $this;
    }



    public function store(){
        try {



            $stm =  $this->con->prepare("INSERT INTO `p_daily_expance`(`reg_no`, `date`, `date_time`, `Ecode`, `Ename`, `description`, `amount`) 
                                        
                                        VALUES(:reg_no, :date, :date_time, :Ecode, (SELECT exist_list.Ename FROM `exist_list` WHERE exist_list.Ecode = :Ename), :description, :amount) ");
            $stm->execute(array(
                ':reg_no' => $this->reg_no,
                ':date' => $this->date,
                ':date_time' => $this->date_time,
                ':Ecode' => $this->Ecode,
                ':Ename' => $this->Ename,
                ':description' => $this->description,
                ':amount' => $this->amount

            ));

            $id = $this->con->lastInsertId('id');

            //echo $id;


            $query = "SELECT * FROM `p_daily_expance` WHERE `id` = '$id'";
           // echo $query;
            $stm1 =  $this->con->prepare($query);

            $stm1->execute();
            $result = $stm1->fetch(PDO::FETCH_ASSOC);
            $regNo = $result['reg_no'];
            $query2 = "SELECT * FROM `p_daily_expance` WHERE `deleted_at` = '0000-00-00 00:00:00' and `reg_no` = '$regNo'";
            $stm2 =  $this->con->prepare($query2);
            $stm2->execute();
            $_SESSION['regNo'] = $this->reg_no;
            return $stm2->fetchAll(PDO::FETCH_ASSOC);


           // $result = $stm->fetch(PDO::FETCH_ASSOC);
           // echo $result['reg_no'];

            //$id = $stm->fetch(PDO::FETCH_ASSOC);

//            if($result){
//                $_POST['regNo'] = $result['reg_no'];
//               // return $id["reg_no"];
//                //$inserted_reg_no = $this->con->lastInsertId(['reg_no']);
//                //echo $inserted_reg_no;
//                $_SESSION['msg'] = 'Data successfully Inserted !!!';
//
//                //$_SESSION['regNo'] = $this->reg_no;
//                header('location:index.php');
//
//            }
        } catch (PDOException $e) {
            $this->con->rollBack();
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function last_id(){
        try {
            $query = "SELECT * FROM p_daily_expance WHERE `deleted_at` = '0000-00-00 00:00:00'  ORDER BY id DESC LIMIT 1 ";

            $stm =  $this->con->prepare($query);
            $stm->execute();
            $result = $stm->fetch(PDO::FETCH_ASSOC);
            $regNo = $result['reg_no'];
            $_POST['regNo'] =$result['reg_no'];

            $query2 = "SELECT `paitent_name`, `f_or_h_name` FROM ipr WHERE reg_no='$regNo' and `deleted_at` = '0000-00-00 00:00:00' ";

            $stm1 =  $this->con->prepare($query2);
            $stm1->execute();
            $result2 = $stm1->fetch(PDO::FETCH_ASSOC);
            $_POST['name'] = $result2['paitent_name'];
            $_POST['f_h_name'] = $result2['f_or_h_name'];


            $sql = "select Ename,Ecode, sum(amount) as sum from p_daily_expance where reg_no = '$regNo' group by Ecode";

            $stmt = $this->con->prepare($sql);
            $stmt->execute();
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
//var_dump($results);
            /* If there are results from database push to result array */

            $reg_fee='registration charge';
            $srent_fee='Seat Rent (General Bed/Cabin)';
            //$dresing='Adjustment For Seat Rent-';
            $con_fee='Consultation Fee-';
            //$dresing='Medical Board Fee-';
            $op_sur_fee='Surgeon/ Surgical Team Fee-';
            $op_as_fee='Asst. Surgeon Fee-';
            $op_ans_fee='Anaesthetist Fee-';
            $op_ot_fee='O.T. Charge-';
            $pathology='Pathology Charge-';
            $ecg_charge='E. C. G. Charge-';
            $x_ray='X-Ray Charg-';
            $oxygen='Oxygen Charge-';
            $monitoring='Monitoring Charge';
            $ambulance='Ambulance Fare-';
            $dresing='Dressing / Stich Remove Charge-';
            $blood='Blood Transfusion / Nebulizer Charge-';
            $ultra='Ultrasonography Charge-';
            $endoscopy='Endoscopy Charge-';
            $service='Service Traction-';
            $medicine='Medicine Cost-';





            foreach ($results as $result){

                if( $result['Ecode']=='111'){
                    $result['Ename']=$reg_fee;
                    $_POST['reg_fee']= $result['sum'];


                }
                else if( $result['Ecode']=='123'){
                    $result['Ename']=$ambulance;
                    $_POST['ambulance']= $result['sum'];

                }

                else if( $result['Ecode']=='136'){
                    $result['Ename']=$ecg_charge;
                    $_POST['ecg_charge']= $result['sum'];

                }

                else if( $result['Ecode']=='678'){
                    $result['Ename']=$dresing;
                    $_POST['dresing']= $result['sum'];


                }

                else if( $result['Ecode']=='567'){
                    $result['Ename']=$medicine;
                    $_POST['medicine']= $result['sum'];

                }

                else if( $result['Ecode']=='7765'){
                    $result['Ename']=$blood;
                    $_POST['blood']= $result['sum'];

                }

                else if( $result['Ecode']=='345'){
                    $result['Ename']=$oxygen;
                    $_POST['oxygen']= $result['sum'];

                }
                else if( $result['Ecode']=='1456'){
                    $result['Ename']=$endoscopy;
                    $_POST['endoscopy']= $result['sum'];

                }

                else if( $result['Ecode']=='6780'){
                    $result['Ename']=$x_ray;
                    $_POST['x_ray']= $result['sum'];

                }

                else if( $result['Ecode']=='456'){
                    $result['Ename']=$monitoring;
                    $_POST['monitoring']= $result['sum'];

                }


                else if( $result['Ecode']=='6544'){
                    $result['Ename']=$ultra;
                    $_POST['ultra']= $result['sum'];

                }
                else{
                    echo '';
                }

                //var_dump($_POST);

            }


           // return $_POST;


            //var_dump($_POST);
            $p_bill = $this->set_p_bill($_POST);
            //var_dump($p_bill);

            $query = "SELECT * FROM p_bill WHERE `deleted_at` = '0000-00-00 00:00:00' and reg_no = '$regNo' ";

            $stm =  $this->con->prepare($query);
            $stm->execute();
            $result = $stm->fetch(PDO::FETCH_ASSOC);
            if($result == 0) {

                $p_bills = $this->store_p_bill();
            } else {
                $p_bills = $this->update_p_bill();
                //var_dump($p_bill);
            }






        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function index($reg_no){
        try {
            $query = "SELECT * FROM `p_daily_expance` WHERE `deleted_at` = '0000-00-00 00:00:00' and `reg_no` = '$reg_no'";
            $stm =  $this->con->prepare($query);
//            $stm->bindValue(':reg_no', $reg_no, PDO::PARAM_STR);
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
    public function sum($reg_no){

    }


    public function view($id){
        try {

            $stm =  $this->con->prepare("SELECT * FROM `p_daily_expance` WHERE id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();
            return $stm->fetch(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function trash(){
        try {

            $stm =  $this->con->prepare("SELECT * FROM `p_daily_expance` WHERE `deleted_at` != '0000-00-00 00:00:00'");

            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }



    public function tmp_delete($id){
        try {

            $stm =  $this->con->prepare("UPDATE `p_daily_expance` SET `deleted_at` = NOW() WHERE id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();
            if($stm){
                $_SESSION['delete'] = 'Data successfully Deleted !!!';
                header('location:index.php');
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function restore($id){
        try {

            $stm =  $this->con->prepare("UPDATE `p_daily_expance` SET `deleted_at` = '0000-00-00 00:00:00' WHERE id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();
            if($stm){
                $_SESSION['delete'] = 'Data Restore successfully !!!';
                header('location:trash.php');
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function delete($id){
        try {

            $stm =  $this->con->prepare("DELETE FROM `p_daily_expance` WHERE id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();
            if($stm){
                $_SESSION['delete'] = 'Data successfully Deleted !!!';
                header('location:index.php');
            }

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function update(){
        try {

            $stmt = $this->con->prepare("UPDATE `p_daily_expance` SET `name` = :name, `bed_type`= :bed_type, `rate`= :rate, `mark`= :mark WHERE `p_daily_expance`.`id` = :id;");
            $stmt->bindValue(':name', $this->name, PDO::PARAM_STR);
            $stmt->bindValue(':bed_type', $this->bed_type, PDO::PARAM_STR);
            $stmt->bindValue(':rate', $this->rate, PDO::PARAM_STR);
            $stmt->bindValue(':mark', $this->mark, PDO::PARAM_STR);
            $stmt->bindValue(':id', $this->id, PDO::PARAM_STR);
            $stmt->execute();
            if($stmt){
                $_SESSION['update'] = 'Data successfully Updated !!!';
                header('location:index.php');
            }

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }




    public function set_p_bill($data = array()){

        if(array_key_exists('regNo',$data)){
            $this->regNo = $data['regNo'];
        }
        if(array_key_exists('name',$data)){
            $this->name = $data['name'];
        }
        if(array_key_exists('f_h_name',$data)){
            $this->f_h_name = $data['f_h_name'];
        }
        if(array_key_exists('date',$data)){
            $this->date = $data['date'];
        }
        if(array_key_exists('date_time',$data)){
            $this->date_time = $data['date_time'];
        }
        if(array_key_exists('Ecode',$data)){
            $this->Ecode = $data['Ecode'];
        }
        if(array_key_exists('Ename',$data)){
            $this->Ename = $data['Ename'];
        }
        if(array_key_exists('description',$data)){
            $this->description = $data['description'];
        }
        if(array_key_exists('amount',$data)){
            $this->amount = $data['amount'];
        }
        if(array_key_exists('reg_fee',$data)){
            $this->reg_fee = $data['reg_fee'];
        }
        if(array_key_exists('srent_fee',$data)){
            $this->srent_fee = $data['srent_fee'];
        }
        if(array_key_exists('con_fee',$data)){
            $this->con_fee = $data['con_fee'];
        }
        if(array_key_exists('op_sur_fee',$data)){
            $this->op_sur_fee = $data['op_sur_fee'];
        }
        if(array_key_exists('op_as_fee',$data)){
            $this->op_as_fee = $data['op_as_fee'];
        }
        if(array_key_exists('op_ans_fee',$data)){
            $this->op_ans_fee = $data['op_ans_fee'];
        }
        if(array_key_exists('op_ot_fee',$data)){
            $this->op_ot_fee = $data['op_ot_fee'];
        }
        if(array_key_exists('pathology',$data)){
            $this->pathology = $data['pathology'];
        }

        if(array_key_exists('ecg_charge',$data)){
            $this->ecg_charge = $data['ecg_charge'];
        }
        if(array_key_exists('x_ray',$data)){
            $this->x_ray = $data['x_ray'];
        }
        if(array_key_exists('oxygen',$data)){
            $this->oxygen = $data['oxygen'];
        }
        if(array_key_exists('monitoring',$data)){
            $this->monitoring = $data['monitoring'];
        }

        if(array_key_exists('ambulance',$data)){
            $this->ambulance = $data['ambulance'];
        }
        if(array_key_exists('dresing',$data)){
            $this->dresing = $data['dresing'];
        }

        if(array_key_exists('blood',$data)){
            $this->blood = $data['blood'];
        }

        if(array_key_exists('ultra',$data)){
            $this->ultra = $data['ultra'];
        }
        if(array_key_exists('endoscopy',$data)){
            $this->endoscopy = $data['endoscopy'];
        }
        if(array_key_exists('service',$data)){
            $this->service = $data['service'];
        }
        if(array_key_exists('medicine',$data)){
            $this->medicine = $data['medicine'];
        }
        if(array_key_exists('others',$data)){
            $this->others = $data['others'];
        }
        if(array_key_exists('vat',$data)){
            $this->vat = $data['vat'];
        }

        if(array_key_exists('advance',$data)){
            $this->advance = $data['advance'];
        }
        if(array_key_exists('balance',$data)){
            $this->balance = $data['balance'];
        }
        if(array_key_exists('id',$data)){
            $this->id = $data['id'];
        }
        return $this;
    }


    public function store_p_bill(){
        try {

            $stm =  $this->con->prepare("INSERT INTO `p_bill`(`reg_no`, `name`, `f_h_name`, `reg_fee`, `srent_fee`, `con_fee`, `op_sur_fee`, `op_as_fee`, `op_ans_fee`, `op_ot_fee`, `pathology`, `ecg_charge`, `x_ray`, `oxygen`, `monitoring`, `ambulance`, `dresing`, `blood`, `ultra`, `endoscopy`, `service`, `medicine`, `others`, `vat`, `advance`, `balance`) 
                                        
                                        VALUES(:reg_no, :name, :f_h_name, :reg_fee, :srent_fee, :con_fee, :op_sur_fee, :op_as_fee, :op_ans_fee, :op_ot_fee, :pathology, :ecg_charge, :x_ray, :oxygen, :monitoring, :ambulance, :dresing, :blood, :ultra, :endoscopy, :service, :medicine, :others, :vat, :advance, :balance)");
            $result =$stm->execute(array(
                ':reg_no' => $this->regNo,
                ':name' => $this->name,
                ':f_h_name' => $this->f_h_name,
                ':reg_fee' => $this->reg_fee,
                ':srent_fee' => $this->srent_fee,
                ':con_fee' => $this->con_fee,
                ':op_sur_fee' => $this->op_sur_fee,
                ':op_as_fee' => $this->op_as_fee,
                ':op_ans_fee' => $this->op_ans_fee,
                ':op_ot_fee' => $this->op_ot_fee,
                ':pathology' => $this->pathology,
                ':ecg_charge' => $this->ecg_charge,
                ':x_ray' => $this->x_ray,
                ':oxygen' => $this->oxygen,
                ':monitoring' => $this->monitoring,
                ':ambulance' => $this->ambulance,
                ':dresing' => $this->dresing,
                ':blood' => $this->blood,
                ':ultra' => $this->ultra,
                ':endoscopy' => $this->endoscopy,
                ':service' => $this->service,
                ':medicine' => $this->medicine,
                ':others' => $this->others,
                ':vat' => $this->vat,
                ':advance' => $this->advance,
                ':balance' => $this->balance



            ));
            if($result){
               // $_SESSION['msg'] = 'Data successfully Inserted !!!';
               // header('location:index.php');

               echo '';
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }




    public function update_p_bill(){
        try {

            $stmt = $this->con->prepare("UPDATE `p_bill` SET `reg_no` = :reg_no, `name`= :name, `f_h_name`= :f_h_name, `reg_fee`= :reg_fee, `srent_fee`= :srent_fee, `con_fee`= :con_fee, `op_sur_fee`= :op_sur_fee, `op_as_fee`= :op_as_fee, `op_ans_fee`= :op_ans_fee, `op_ot_fee`= :op_ot_fee, `pathology`= :pathology, `ecg_charge`= :ecg_charge, `x_ray`= :x_ray, `oxygen`= :oxygen, `monitoring`= :monitoring, `ambulance`= :ambulance, `dresing`= :dresing, `blood`= :blood, `ultra`= :ultra, `endoscopy`= :endoscopy, `service`= :service, `medicine`= :medicine, `others`= :others, `vat`= :vat, `advance`= :advance, `balance`= :balance WHERE `p_bill`.`reg_no` = :reg_no;");
            $stmt->bindValue(':reg_no', $this->regNo, PDO::PARAM_STR);
            $stmt->bindValue(':name', $this->name, PDO::PARAM_STR);
            $stmt->bindValue(':f_h_name', $this->f_h_name, PDO::PARAM_STR);
            $stmt->bindValue(':reg_fee', $this->reg_fee, PDO::PARAM_STR);
            $stmt->bindValue(':srent_fee', $this->srent_fee, PDO::PARAM_STR);
            $stmt->bindValue(':con_fee', $this->con_fee, PDO::PARAM_STR);
            $stmt->bindValue(':op_sur_fee', $this->op_sur_fee, PDO::PARAM_STR);
            $stmt->bindValue(':op_as_fee', $this->op_as_fee, PDO::PARAM_STR);
            $stmt->bindValue(':op_ans_fee', $this->op_ans_fee, PDO::PARAM_STR);
            $stmt->bindValue(':op_ot_fee', $this->op_ot_fee, PDO::PARAM_STR);
            $stmt->bindValue(':pathology', $this->pathology, PDO::PARAM_STR);
            $stmt->bindValue(':ecg_charge', $this->ecg_charge, PDO::PARAM_STR);
            $stmt->bindValue(':x_ray', $this->x_ray, PDO::PARAM_STR);
            $stmt->bindValue(':oxygen', $this->oxygen, PDO::PARAM_STR);
            $stmt->bindValue(':monitoring', $this->monitoring, PDO::PARAM_STR);
            $stmt->bindValue(':ambulance', $this->ambulance, PDO::PARAM_STR);
            $stmt->bindValue(':dresing', $this->dresing, PDO::PARAM_STR);
            $stmt->bindValue(':blood', $this->blood, PDO::PARAM_STR);
            $stmt->bindValue(':ultra', $this->ultra, PDO::PARAM_STR);
            $stmt->bindValue(':endoscopy', $this->endoscopy, PDO::PARAM_STR);
            $stmt->bindValue(':service', $this->service, PDO::PARAM_STR);
            $stmt->bindValue(':medicine', $this->medicine, PDO::PARAM_STR);
            $stmt->bindValue(':others', $this->others, PDO::PARAM_STR);
            $stmt->bindValue(':vat', $this->vat, PDO::PARAM_STR);
            $stmt->bindValue(':advance', $this->advance, PDO::PARAM_STR);
            $stmt->bindValue(':balance', $this->balance, PDO::PARAM_STR);
            //$stmt->bindValue(':id', $this->id, PDO::PARAM_STR);
            $stmt->execute();
            if($stmt){
                //$_SESSION['update'] = 'Data successfully Updated !!!';
                //header('location:index.php');
                echo '';
            }

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


}
