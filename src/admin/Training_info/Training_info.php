<?php

namespace App\admin\Training_info;
if(!isset($_SESSION)){
    session_start();
}
use App\Connection;
use PDOException;
use PDO;
class Training_info extends Connection
{
    private $doc_id;
    private $institute_name1;
    private $subject1;
    private $join_year1;
    private $institute_name2;
    private $subject2;
    private $join_year2;
    private $institute_name3;
    private $subject3;
    private $join_year3;
    private $id;
    public function set($data = array()){

        if(array_key_exists('doc_id',$data)){
            $this->doc_id = $data['doc_id'];
        }
        if(array_key_exists('institute_name1',$data)){
            $this->institute_name1 = $data['institute_name1'];
        }
        if(array_key_exists('subject1',$data)){
            $this->subject1 = $data['subject1'];
        }
        if(array_key_exists('join_year1',$data)){
            $this->join_year1 = $data['join_year1'];
        }
        if(array_key_exists('institute_name2',$data)){
            $this->institute_name2 = $data['institute_name2'];
        }

        if(array_key_exists('subject2',$data)){
            $this->subject2 = $data['subject2'];
        }

        if(array_key_exists('join_year2',$data)){
            $this->join_year2 = $data['join_year2'];
        }
        if(array_key_exists('institute_name3',$data)){
            $this->institute_name3 = $data['institute_name3'];
        }
        if(array_key_exists('subject3',$data)){
            $this->subject3 = $data['subject3'];
        }
        if(array_key_exists('join_year3',$data)){
            $this->join_year3 = $data['join_year3'];
        }
        if(array_key_exists('id',$data)){
            $this->id = $data['id'];
        }
        return $this;
    }

    public function store(){
        try {

            $stm =  $this->con->prepare("INSERT INTO `training_info`(`doc_id`, `institute_name1`, `subject1`, `join_year1`, `institute_name2`, `subject2`, `join_year2`, `institute_name3`, `subject3`, `join_year3`) 
                                        VALUES(:doc_id, :institute_name1, :subject1, :join_year1, :institute_name2, :subject2, :join_year2, :institute_name3, :subject3, :join_year3)");
            $result =$stm->execute(array(
                ':doc_id' => $this->doc_id,
                ':institute_name1' => $this->institute_name1,
                ':subject1' => $this->subject1,
                ':join_year1' => $this->join_year1,
                ':institute_name2' => $this->institute_name2,
                ':subject2' => $this->subject2,
                ':join_year2' => $this->join_year2,
                ':institute_name3' => $this->institute_name3,
                ':subject3' => $this->subject3,
                ':join_year3' => $this->join_year3
            ));
            if($result){
                $_SESSION['msg'] = 'Data successfully Inserted !!!';
                header('location:index.php');
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
    public function index(){
        try {

            $stm =  $this->con->prepare("SELECT * FROM `training_info` WHERE `deleted_at` = '0000-00-00 00:00:00'");

            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function view($id){
        try {

            $stm =  $this->con->prepare("SELECT * FROM `training_info` WHERE id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();
            return $stm->fetch(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function trash(){
        try {

            $stm =  $this->con->prepare("SELECT * FROM `training_info` WHERE `deleted_at` != '0000-00-00 00:00:00'");

            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }



    public function tmp_delete($id){
        try {

            $stm =  $this->con->prepare("UPDATE `training_info` SET `deleted_at` = NOW() WHERE id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();
            if($stm){
                $_SESSION['delete'] = 'Data successfully Deleted !!!';
                header('location:index.php');
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function restore($id){
        try {

            $stm =  $this->con->prepare("UPDATE `training_info` SET `deleted_at` = '0000-00-00 00:00:00' WHERE id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();
            if($stm){
                $_SESSION['msg'] = 'Data Restore successfully !!!';
                header('location:trash.php');
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function delete($id){
        try {

            $stm =  $this->con->prepare("DELETE FROM `training_info` WHERE id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();
            if($stm){
                $_SESSION['delete'] = 'Data successfully Deleted !!!';
                header('location:index.php');
            }

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function update(){
        try {

            $stmt = $this->con->prepare("UPDATE `training_info` SET `doc_id` = :doc_id, `institute_name1`= :institute_name1, `subject1`= :subject1, `join_year1`= :join_year1, `institute_name2`= :institute_name2, `subject2`= :subject2, `join_year2`= :join_year2, `institute_name3`= :institute_name3, `subject3`= :subject3, `join_year3`= :join_year3 WHERE `training_info`.`id` = :id;");
            $stmt->bindValue(':doc_id', $this->doc_id, PDO::PARAM_STR);
            $stmt->bindValue(':institute_name1', $this->institute_name1, PDO::PARAM_STR);
            $stmt->bindValue(':subject1', $this->subject1, PDO::PARAM_STR);
            $stmt->bindValue(':join_year1', $this->join_year1, PDO::PARAM_STR);
            $stmt->bindValue(':institute_name2', $this->institute_name2, PDO::PARAM_STR);
            $stmt->bindValue(':subject2', $this->subject2, PDO::PARAM_STR);
            $stmt->bindValue(':join_year2', $this->join_year2, PDO::PARAM_STR);
            $stmt->bindValue(':institute_name3', $this->institute_name3, PDO::PARAM_STR);
            $stmt->bindValue(':subject3', $this->subject3, PDO::PARAM_STR);
            $stmt->bindValue(':join_year3', $this->join_year3, PDO::PARAM_STR);
            $stmt->bindValue(':id', $this->id, PDO::PARAM_STR);
            $stmt->execute();
            if($stmt){
                $_SESSION['update'] = 'Data successfully Updated !!!';
                header('location:index.php');
            }

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }



}
