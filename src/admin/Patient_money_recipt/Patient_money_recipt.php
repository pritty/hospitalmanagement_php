<?php

namespace App\admin\Patient_money_recipt;
if(!isset($_SESSION)){
    session_start();
}
use App\Connection;
use PDOException;
use PDO;
class Patient_money_recipt extends Connection
{
    private $invc_no;
    private $s_invc;
    private $reg_no;
    private $bed_no;
    private $date;
    private $time;
    private $name;
    private $total;
    private $discount;
    private $advance;
    private $balance;
    private $chktype;
    private $bank_name;
    private $chq_no;
    private $chq_date;
    private $amount;
    private $id;
    public function set($data = array()){

        if(array_key_exists('invc_no',$data)){
            $this->invc_no = $data['invc_no'];
        }
        if(array_key_exists('s_invc',$data)){
            $this->s_invc = $data['s_invc'];
        }
        if(array_key_exists('reg_no',$data)){
            $this->reg_no = $data['reg_no'];
        }
        if(array_key_exists('bed_no',$data)){
            $this->bed_no = $data['bed_no'];
        }
        if(array_key_exists('date',$data)){
            $this->date = $data['date'];
        }
        if(array_key_exists('time',$data)){
            $this->time = $data['time'];
        }

        if(array_key_exists('name',$data)){
            $this->name = $data['name'];
        }

        if(array_key_exists('total',$data)){
            $this->total = $data['total'];
        }
        if(array_key_exists('discount',$data)){
            $this->discount = $data['discount'];
        }
        if(array_key_exists('advance',$data)){
            $this->advance = $data['advance'];
        }
        if(array_key_exists('balance',$data)){
            $this->balance = $data['balance'];
        }

        if(array_key_exists('chktype',$data)){
            $this->chktype = $data['chktype'];
        }

        if(array_key_exists('bank_name',$data)){
            $this->bank_name = $data['bank_name'];
        }

        if(array_key_exists('chq_no',$data)){
            $this->chq_no = $data['chq_no'];
        }
        if(array_key_exists('chq_date',$data)){
            $this->chq_date = $data['chq_date'];
        }

        if(array_key_exists('amount',$data)){
            $this->amount = $data['amount'];
        }

        if(array_key_exists('id',$data)){
            $this->id = $data['id'];
        }
        return $this;
    }

    public function store(){
        try {

            $stm =  $this->con->prepare("INSERT INTO `p_money_recipt`(`invc_no`, `s_invc`, `reg_no`, `bed_no`, `date`, `time`, `name`, `total`, `discount`, `advance`, `balance`, `chktype`, `bank_name`, `chq_no`, `chq_date`, `amount`) 
                                        VALUES(:invc_no, :s_invc, :reg_no, :bed_no, :date, :time, :name, :total, :discount, :advance, :balance, :chktype, :bank_name, :chq_no, :chq_date, :amount)");
            $result =$stm->execute(array(
                ':invc_no' => $this->invc_no,
                ':s_invc' => $this->s_invc,
                ':reg_no' => $this->reg_no,
                ':bed_no' => $this->bed_no,
                ':date' => $this->date,
                ':time' => $this->time,
                ':name' => $this->name,
                ':total' => $this->total,
                ':discount' => $this->discount,
                ':advance' => $this->advance,
                ':balance' => $this->balance,
                ':chktype' => $this->chktype,
                ':bank_name' => $this->bank_name,
                ':chq_no' => $this->chq_no,
                ':chq_date' => $this->chq_date,
                ':amount' => $this->amount

            ));
            if($result){
                $_SESSION['msg'] = 'Data successfully Inserted !!!';
                header('location:index.php');
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
    public function index(){
        try {

            $stm =  $this->con->prepare("SELECT * FROM `p_money_recipt` WHERE `deleted_at` = '0000-00-00 00:00:00'");

            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function view($id){
        try {

            $stm =  $this->con->prepare("SELECT * FROM `p_money_recipt` WHERE id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();
            return $stm->fetch(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function trash(){
        try {

            $stm =  $this->con->prepare("SELECT * FROM `p_money_recipt` WHERE `deleted_at` != '0000-00-00 00:00:00'");

            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }



    public function tmp_delete($id){
        try {

            $stm =  $this->con->prepare("UPDATE `p_money_recipt` SET `deleted_at` = NOW() WHERE id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();
            if($stm){
                $_SESSION['delete'] = 'Data successfully Deleted !!!';
                header('location:index.php');
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function restore($id){
        try {

            $stm =  $this->con->prepare("UPDATE `ipr` SET `deleted_at` = '0000-00-00 00:00:00' WHERE id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();
            if($stm){
                $_SESSION['msg'] = 'Data Restore successfully !!!';
                header('location:trash.php');
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function delete($id){
        try {

            $stm =  $this->con->prepare("DELETE FROM `p_money_recipt` WHERE id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();
            if($stm){
                $_SESSION['delete'] = 'Data successfully Deleted !!!';
                header('location:index.php');
            }

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function update(){
        try {

            $stmt = $this->con->prepare("UPDATE `p_money_recipt` SET `invc_no` = :invc_no, `s_invc`= :s_invc, `reg_no`= :reg_no, `bed_no`= :bed_no, `date`= :date, `time`= :time, `name`= :name, `total`= :total, `discount`= :discount, `advance`= :advance, `balance`= :balance, `chktype`= :chktype, `bank_name`= :bank_name, `chq_no`= :chq_no, `chq_date`= :chq_date, `amount`= :amount WHERE `p_money_recipt`.`id` = :id;");
            $stmt->bindValue(':invc_no', $this->invc_no, PDO::PARAM_STR);
            $stmt->bindValue(':s_invc', $this->s_invc, PDO::PARAM_STR);
            $stmt->bindValue(':reg_no', $this->reg_no, PDO::PARAM_STR);
            $stmt->bindValue(':bed_no', $this->bed_no, PDO::PARAM_STR);
            $stmt->bindValue(':date', $this->date, PDO::PARAM_STR);
            $stmt->bindValue(':time', $this->time, PDO::PARAM_STR);
            $stmt->bindValue(':name', $this->name, PDO::PARAM_STR);
            $stmt->bindValue(':total', $this->total, PDO::PARAM_STR);
            $stmt->bindValue(':discount', $this->discount, PDO::PARAM_STR);
            $stmt->bindValue(':advance', $this->advance, PDO::PARAM_STR);
            $stmt->bindValue(':balance', $this->balance, PDO::PARAM_STR);
            $stmt->bindValue(':chktype', $this->chktype, PDO::PARAM_STR);
            $stmt->bindValue(':bank_name', $this->bank_name, PDO::PARAM_STR);
            $stmt->bindValue(':chq_no', $this->chq_no, PDO::PARAM_STR);
            $stmt->bindValue(':chq_date', $this->chq_date, PDO::PARAM_STR);
            $stmt->bindValue(':amount', $this->amount, PDO::PARAM_STR);
            $stmt->bindValue(':id', $this->id, PDO::PARAM_STR);
            $stmt->execute();
            if($stmt){
                $_SESSION['update'] = 'Data successfully Updated !!!';
                header('location:index.php');
            }

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }



}
