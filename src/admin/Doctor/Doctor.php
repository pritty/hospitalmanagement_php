<?php

namespace App\admin\Doctor;
if(!isset($_SESSION)){
    session_start();
}
use App\Connection;
use PDOException;
use PDO;
class Doctor extends Connection
{
    private $doc_id;
    private $name;
    private $image;
    private $specialist_op;
    private $contact_no;
    private $address;
    private $email;
    private $contact_no_office;
    private $join_date;
    private $leave_status;
    private $id;
    public function set($data = array()){

        if(array_key_exists('doc_id',$data)){
            $this->doc_id = $data['doc_id'];
        }

        if(array_key_exists('name',$data)){
            $this->name = $data['name'];
        }
        if(array_key_exists('image',$data)){
            $this->image = $data['image'];
        }
        if(array_key_exists('specialist_op',$data)){
            $this->specialist_op = $data['specialist_op'];
        }
        if(array_key_exists('contact_no',$data)){
            $this->contact_no = $data['contact_no'];
        }
        if(array_key_exists('address',$data)){
            $this->address = $data['address'];
        }
        if(array_key_exists('email',$data)){
            $this->email = $data['email'];
        }
        if(array_key_exists('contact_no_office',$data)){
            $this->contact_no_office = $data['contact_no_office'];
        }
        if(array_key_exists('join_date',$data)){
            $this->join_date = $data['join_date'];
        }
        if(array_key_exists('leave_status',$data)){
            $this->leave_status = $data['leave_status'];
        }
        if(array_key_exists('id',$data)){
            $this->id = $data['id'];
        }
        return $this;
    }

    public function store(){
        try {

            $stm =  $this->con->prepare("INSERT INTO `doctors`(`doc_id`, `name`, `image`, `specialist_op`, `contact_no`, `address`, `email`, `contact_no_office`, `join_date`, `leave_status`) 
                                        VALUES(:doc_id, :name, :image, :specialist_op, :contact_no, :address, :email, :contact_no_office, :join_date, :leave_status)");
            $result =$stm->execute(array(
                ':doc_id' => $this->doc_id,
                ':name' => $this->name,
                ':image' => $this->image,
                ':specialist_op' => $this->specialist_op,
                ':contact_no' => $this->contact_no,
                ':address' => $this->address,
                ':email' => $this->email,
                ':contact_no_office' => $this->contact_no_office,
                ':join_date' => $this->join_date,
                ':leave_status' => $this->leave_status

            ));
            if($result){
                $_SESSION['msg'] = 'Data successfully Inserted !!!';
                header('location:index.php');
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function image_upload(){
        $_POST['image'] = $_FILES['image']['name'];
        $image_tmp_name = $_FILES['image']['tmp_name'];
        $name =  substr(md5(time()),'0','10');
        $data = explode('.',$_POST['image']);
        $_POST['image'] = $name.'.'.end($data);

        move_uploaded_file($image_tmp_name,'../uploads/doctors/'.$_POST['image']);
        return $_POST['image'];
    }


    public function delete_img($id){
        try {
            $stmt = $this->con->prepare("SELECT `image` FROM `doctors` WHERE id = :id");
            $stmt->bindValue(':id', $id, PDO::PARAM_INT);
            $stmt->execute();
            $data = $stmt->fetch(PDO::FETCH_ASSOC);
            if(isset($data['image'])){
                //var_dump($data['image']);
                unlink('../uploads/doctors/'.$data['image']);
            }

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function index(){
        try {

            $stm =  $this->con->prepare("SELECT * FROM `doctors` WHERE `deleted_at` = '0000-00-00 00:00:00'");

            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function view($id){
        try {

            $stm =  $this->con->prepare("SELECT * FROM `doctors` WHERE id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();
            return $stm->fetch(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function trash(){
        try {

            $stm =  $this->con->prepare("SELECT * FROM `doctors` WHERE `deleted_at` != '0000-00-00 00:00:00'");

            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }



    public function tmp_delete($id){
        try {

            $stm =  $this->con->prepare("UPDATE `doctors` SET `deleted_at` = NOW() WHERE id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();
            if($stm){
                $_SESSION['delete'] = 'Data successfully Deleted !!!';
                header('location:index.php');
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function restore($id){
        try {

            $stm =  $this->con->prepare("UPDATE `doctors` SET `deleted_at` = '0000-00-00 00:00:00' WHERE id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();
            if($stm){
                $_SESSION['msg'] = 'Data Restore successfully !!!';
                header('location:trash.php');
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function delete($id){
        try {

            $stm =  $this->con->prepare("DELETE FROM `doctors` WHERE id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();
            if($stm){
                $_SESSION['delete'] = 'Data successfully Deleted !!!';
                header('location:index.php');
            }

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function update(){
        try {

            $stmt = $this->con->prepare("UPDATE `doctors` SET `doc_id`= :doc_id, `name` = :name, `image`= :image, `specialist_op`= :specialist_op, `contact_no`= :contact_no, `address`= :address, `email`= :email, `contact_no_office`= :contact_no_office, `join_date`= :join_date, `leave_status`= :leave_status WHERE `doctors`.`id` = :id;");
            $stmt->bindValue(':doc_id', $this->doc_id, PDO::PARAM_STR);
            $stmt->bindValue(':name', $this->name, PDO::PARAM_STR);
            $stmt->bindValue(':image', $this->image, PDO::PARAM_STR);
            $stmt->bindValue(':specialist_op', $this->specialist_op, PDO::PARAM_STR);
            $stmt->bindValue(':contact_no', $this->contact_no, PDO::PARAM_STR);
            $stmt->bindValue(':address', $this->address, PDO::PARAM_STR);
            $stmt->bindValue(':email', $this->email, PDO::PARAM_STR);
            $stmt->bindValue(':contact_no_office', $this->contact_no_office, PDO::PARAM_STR);
            $stmt->bindValue(':join_date', $this->join_date, PDO::PARAM_STR);
            $stmt->bindValue(':leave_status', $this->leave_status, PDO::PARAM_STR);
            $stmt->bindValue(':id', $this->id, PDO::PARAM_STR);
            $stmt->execute();
            if($stmt){
                $_SESSION['update'] = 'Data successfully Updated !!!';
                header('location:index.php');
            }

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }



}
