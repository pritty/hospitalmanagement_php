<?php

namespace App\admin\Experiance_detail;
if(!isset($_SESSION)){
    session_start();
}
use App\Connection;
use PDOException;
use PDO;
class Experiance_detail extends Connection
{
    private $doc_id;
    private $institute_name1;
    private $post1;
    private $year1;
    private $join_year1;
    private $institute_name2;
    private $post2;
    private $year2;
    private $join_year2;
    private $institute_name3;
    private $post3;
    private $year3;
    private $join_year3;
    private $institute_name4;
    private $post4;
    private $year4;
    private $join_year4;
    private $id;
    public function set($data = array()){

        if(array_key_exists('doc_id',$data)){
            $this->doc_id = $data['doc_id'];
        }
        if(array_key_exists('institute_name1',$data)){
            $this->institute_name1 = $data['institute_name1'];
        }
        if(array_key_exists('post1',$data)){
            $this->post1 = $data['post1'];
        }
        if(array_key_exists('year1',$data)){
            $this->year1 = $data['year1'];
        }
        if(array_key_exists('join_year1',$data)){
            $this->join_year1 = $data['join_year1'];
        }
        if(array_key_exists('institute_name2',$data)){
            $this->institute_name2 = $data['institute_name2'];
        }

        if(array_key_exists('post2',$data)){
            $this->post2 = $data['post2'];
        }

        if(array_key_exists('year2',$data)){
            $this->year2 = $data['year2'];
        }
        if(array_key_exists('join_year2',$data)){
            $this->join_year2 = $data['join_year2'];
        }
        if(array_key_exists('institute_name3',$data)){
            $this->institute_name3 = $data['institute_name3'];
        }
        if(array_key_exists('post3',$data)){
            $this->post3 = $data['post3'];
        }
        if(array_key_exists('year3',$data)){
            $this->year3 = $data['year3'];
        }
        if(array_key_exists('join_year3',$data)){
            $this->join_year3 = $data['join_year3'];
        }
        if(array_key_exists('institute_name4',$data)){
            $this->institute_name4 = $data['institute_name4'];
        }
        if(array_key_exists('post4',$data)){
            $this->post4 = $data['post4'];
        }

        if(array_key_exists('year4',$data)){
            $this->year4 = $data['year4'];
        }

        if(array_key_exists('join_year4',$data)){
            $this->join_year4 = $data['join_year4'];
        }
        if(array_key_exists('id',$data)){
            $this->id = $data['id'];
        }
        return $this;
    }

    public function store(){
        try {

            $stm =  $this->con->prepare("INSERT INTO `experiance_detail`(`doc_id`, `institute_name1`, `post1`, `year1`, `join_year1`, `institute_name2`, `post2`, `year2`, `join_year2`, `institute_name3`, `post3`, `year3`,`join_year3`, `institute_name4`, `post4`, `year4`, `join_year4`) 
                                        VALUES(:doc_id, :institute_name1, :post1, :year1, :join_year1, :institute_name2, :post2, :year2, :join_year2, :institute_name3, :post3, :year3, :join_year3, :institute_name4, :post4, :year4, :join_year4)");
            $result =$stm->execute(array(
                ':doc_id' => $this->doc_id,
                ':institute_name1' => $this->institute_name1,
                ':post1' => $this->post1,
                ':year1' => $this->year1,
                ':join_year1' => $this->join_year1,
                ':institute_name2' => $this->institute_name2,
                ':post2' => $this->post2,
                ':year2' => $this->year2,
                ':join_year2' => $this->join_year2,
                ':institute_name3' => $this->institute_name3,
                ':post3' => $this->post3,
                ':year3' => $this->year3,
                ':join_year3' => $this->join_year3,
                ':institute_name4' => $this->institute_name4,
                ':year4' => $this->year4,
                ':post4' => $this->post4,
                ':join_year4' => $this->join_year4

            ));
            if($result){
                $_SESSION['msg'] = 'Data successfully Inserted !!!';
                header('location:index.php');
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
    public function index(){
        try {

            $stm =  $this->con->prepare("SELECT * FROM `experiance_detail` WHERE `deleted_at` = '0000-00-00 00:00:00'");

            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function view($id){
        try {

            $stm =  $this->con->prepare("SELECT * FROM `experiance_detail` WHERE id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();
            return $stm->fetch(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function trash(){
        try {

            $stm =  $this->con->prepare("SELECT * FROM `experiance_detail` WHERE `deleted_at` != '0000-00-00 00:00:00'");

            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }



    public function tmp_delete($id){
        try {

            $stm =  $this->con->prepare("UPDATE `experiance_detail` SET `deleted_at` = NOW() WHERE id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();
            if($stm){
                $_SESSION['delete'] = 'Data successfully Deleted !!!';
                header('location:index.php');
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function restore($id){
        try {

            $stm =  $this->con->prepare("UPDATE `experiance_detail` SET `deleted_at` = '0000-00-00 00:00:00' WHERE id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();
            if($stm){
                $_SESSION['msg'] = 'Data Restore successfully !!!';
                header('location:trash.php');
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function delete($id){
        try {

            $stm =  $this->con->prepare("DELETE FROM `experiance_detail` WHERE id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();
            if($stm){
                $_SESSION['delete'] = 'Data successfully Deleted !!!';
                header('location:index.php');
            }

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function update(){
        try {

            $stmt = $this->con->prepare("UPDATE `experiance_detail` SET `doc_id` = :doc_id, `institute_name1`= :institute_name1, `post1`= :post1, `year1`= :year1, `join_year1`= :join_year1, `institute_name2`= :institute_name2, `post2`= :post2, `year2`= :year2, `join_year2`= :join_year2, `institute_name3`= :institute_name3, `post3`= :post3, `year3`= :year3, `join_year3`= :join_year3, `institute_name4`= :institute_name4, `post4`= :post4, `year4`= :year4, `join_year4`= :join_year4 WHERE `experiance_detail`.`id` = :id;");
            $stmt->bindValue(':doc_id', $this->doc_id, PDO::PARAM_STR);
            $stmt->bindValue(':institute_name1', $this->institute_name1, PDO::PARAM_STR);
            $stmt->bindValue(':post1', $this->post1, PDO::PARAM_STR);
            $stmt->bindValue(':year1', $this->year1, PDO::PARAM_STR);
            $stmt->bindValue(':join_year1', $this->join_year1, PDO::PARAM_STR);
            $stmt->bindValue(':institute_name2', $this->institute_name2, PDO::PARAM_STR);
            $stmt->bindValue(':post2', $this->post2, PDO::PARAM_STR);
            $stmt->bindValue(':year2', $this->year2, PDO::PARAM_STR);
            $stmt->bindValue(':join_year2', $this->join_year2, PDO::PARAM_STR);
            $stmt->bindValue(':institute_name3', $this->institute_name3, PDO::PARAM_STR);
            $stmt->bindValue(':post3', $this->post3, PDO::PARAM_STR);
            $stmt->bindValue(':year3', $this->year3, PDO::PARAM_STR);
            $stmt->bindValue(':join_year3', $this->join_year3, PDO::PARAM_STR);
            $stmt->bindValue(':institute_name4', $this->institute_name4, PDO::PARAM_STR);
            $stmt->bindValue(':post4', $this->post4, PDO::PARAM_STR);
            $stmt->bindValue(':year4', $this->year4, PDO::PARAM_STR);
            $stmt->bindValue(':join_year4', $this->join_year4, PDO::PARAM_STR);
            $stmt->bindValue(':id', $this->id, PDO::PARAM_STR);
            $stmt->execute();
            if($stmt){
                $_SESSION['update'] = 'Data successfully Updated !!!';
                header('location:index.php');
            }

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }



}
