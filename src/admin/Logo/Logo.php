<?php

namespace App\admin\Logo;
if(!isset($_SESSION)){
    session_start();
}
use App\Connection;
use PDOException;
use PDO;
class Logo extends Connection
{
    private $image;
    private $logo_name;
    private $id;
    public function set($data = array()){
        if(array_key_exists('image',$data)){
            $this->image = $data['image'];
        }
        if(array_key_exists('logo_name',$data)){
            $this->logo_name = $data['logo_name'];
        }
        if(array_key_exists('id',$data)){
            $this->id = $data['id'];
        }
        return $this;
    }

    public function store(){
        try {

            $stm =  $this->con->prepare("INSERT INTO `doctors`(`doctors`,`logo_name`) 
                                        VALUES(:doctors,:logo_name)");
            $result =$stm->execute(array(
                ':doctors' => $this->image,
                ':logo_name' => $this->logo_name
            ));
            if($result){
                $_SESSION['msg'] = 'Data successfully Inserted !!!';
                header('location:index2.php');
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function image_upload(){
        $_POST['image'] = $_FILES['image']['name'];
        $image_tmp_name = $_FILES['image']['tmp_name'];
        $name =  substr(md5(time()),'0','10');
        $data = explode('.',$_POST['image']);
        $_POST['image'] = $name.'.'.end($data);

        move_uploaded_file($image_tmp_name,'../uploads/doctors/'.$_POST['image']);
        return $_POST['image'];
    }


    public function delete_img($id){
        try {
            $stmt = $this->con->prepare("SELECT `doctors` FROM `doctors` WHERE id = :id");
            $stmt->bindValue(':id', $id, PDO::PARAM_INT);
            $stmt->execute();
            $data = $stmt->fetch(PDO::FETCH_ASSOC);
            if(isset($data['doctors'])){
                var_dump($data['doctors']);
                unlink('../uploads/doctors/'.$data['doctors']);
            }

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function index(){
        try {

            $stm =  $this->con->prepare("SELECT * FROM `doctors` WHERE `deleted_at`= '0000-00-00 00:00:00'");

            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }







    public function trash(){
        try {

            $stm =  $this->con->prepare("SELECT * FROM `doctors` WHERE `deleted_at` != '0000-00-00 00:00:00'");

            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }



    public function view($id){
        try {

            $stm =  $this->con->prepare("SELECT * FROM `doctors` WHERE id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();
            return $stm->fetch(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function tmp_delete($id){
        try {

            $stm =  $this->con->prepare("UPDATE `doctors` SET `deleted_at` = NOW() WHERE id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();
            if($stm){
                $_SESSION['delete'] = 'Data successfully Deleted !!!';
                header('location:index2.php');
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function restore($id){
        try {

            $stm =  $this->con->prepare("UPDATE `doctors` SET `deleted_at` = '0000-00-00 00:00:00' WHERE id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();
            if($stm){
                $_SESSION['msg'] = 'Data Successfully Restored !!!';
                header('location:trash.php');
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function delete($id){
        try {

            $stm =  $this->con->prepare("DELETE FROM `doctors` WHERE id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();
            if($stm){
                $_SESSION['delete'] = 'Data successfully Deleted !!!';
                header('location:trash.php');
            }

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function update(){
        try {

            $stmt = $this->con->prepare("UPDATE `doctors` SET `logo_name` = :logo_name, `doctors` = :doctors WHERE `doctors`.`id` = :id;");
            $stmt->bindValue(':logo_name', $this->logo_name, PDO::PARAM_STR);
            $stmt->bindValue(':doctors', $this->image, PDO::PARAM_STR);
            $stmt->bindValue(':id', $this->id, PDO::PARAM_STR);
            $stmt->execute();
            if($stmt){
                $_SESSION['update'] = 'Data successfully Updated !!!';
                header('location:index2.php');
            }

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }



}
