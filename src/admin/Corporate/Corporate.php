<?php

namespace App\admin\Corporate;
if(!isset($_SESSION)){
    session_start();
}
use App\Connection;
use PDOException;
use PDO;
class Corporate extends Connection
{
    private $c_id;
    private $name;
    private $address;
    private $contact_no;
    private $email;
    private $contact_person;
    private $contact_person2;
    private $balance;
    private $id;
    public function set($data = array()){

        if(array_key_exists('c_id',$data)){
            $this->c_id = $data['c_id'];
        }
        if(array_key_exists('name',$data)){
            $this->name = $data['name'];
        }
        if(array_key_exists('address',$data)){
            $this->address = $data['address'];
        }
        if(array_key_exists('contact_no',$data)){
            $this->contact_no = $data['contact_no'];
        }
        if(array_key_exists('email',$data)){
            $this->email = $data['email'];
        }
        if(array_key_exists('contact_person',$data)){
            $this->contact_person = $data['contact_person'];
        }
        if(array_key_exists('contact_person2',$data)){
            $this->contact_person2 = $data['contact_person2'];
        }
        if(array_key_exists('balance',$data)){
            $this->balance = $data['balance'];
        }
        if(array_key_exists('id',$data)){
            $this->id = $data['id'];
        }
        return $this;
    }

    public function store(){
        try {

            $stm =  $this->con->prepare("INSERT INTO `corporate`(`c_id`, `name`, `address`, `contact_no`, `email`, `contact_person`, `contact_person2`, `balance`) 
                                        VALUES(:c_id, :name, :address, :contact_no, :email, :contact_person, :contact_person2, :balance)");
            $result =$stm->execute(array(
                ':c_id' => $this->c_id,
                ':name' => $this->name,
                ':address' => $this->address,
                ':contact_no' => $this->contact_no,
                ':email' => $this->email,
                ':contact_person' => $this->contact_person,
                ':contact_person2' => $this->contact_person2,
                ':balance' => $this->balance

            ));
            if($result){
                $_SESSION['msg'] = 'Data successfully Inserted !!!';
                header('location:index.php');
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
    public function index(){
        try {

            $stm =  $this->con->prepare("SELECT * FROM `corporate` WHERE `deleted_at` = '0000-00-00 00:00:00'");

            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function view($id){
        try {

            $stm =  $this->con->prepare("SELECT * FROM `corporate` WHERE id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();
            return $stm->fetch(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function trash(){
        try {

            $stm =  $this->con->prepare("SELECT * FROM `corporate` WHERE `deleted_at` != '0000-00-00 00:00:00'");

            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }



    public function tmp_delete($id){
        try {

            $stm =  $this->con->prepare("UPDATE `corporate` SET `deleted_at` = NOW() WHERE id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();
            if($stm){
                $_SESSION['delete'] = 'Data successfully Deleted !!!';
                header('location:index.php');
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function restore($id){
        try {

            $stm =  $this->con->prepare("UPDATE `corporate` SET `deleted_at` = '0000-00-00 00:00:00' WHERE id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();
            if($stm){
                $_SESSION['msg'] = 'Data Successfully Restored !!!';
                header('location:trash.php');
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function delete($id){
        try {

            $stm =  $this->con->prepare("DELETE FROM `corporate` WHERE id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();
            if($stm){
                $_SESSION['delete'] = 'Data successfully Deleted !!!';
                header('location:index.php');
            }

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function update(){
        try {

            $stmt = $this->con->prepare("UPDATE `corporate` SET `c_id` = :c_id, `name`= :name, `address`= :address, `contact_no`= :contact_no, `email`= :email, `contact_person`=:contact_person, `contact_person2`= :contact_person2, `balance`= :balance WHERE `corporate`.`id` = :id;");
            $stmt->bindValue(':c_id', $this->c_id, PDO::PARAM_STR);
            $stmt->bindValue(':name', $this->name, PDO::PARAM_STR);
            $stmt->bindValue(':address', $this->address, PDO::PARAM_STR);
            $stmt->bindValue(':contact_no', $this->contact_no, PDO::PARAM_STR);
            $stmt->bindValue(':email', $this->email, PDO::PARAM_STR);
            $stmt->bindValue(':contact_person', $this->contact_person, PDO::PARAM_STR);
            $stmt->bindValue(':contact_person2', $this->contact_person2, PDO::PARAM_STR);
            $stmt->bindValue(':balance', $this->balance, PDO::PARAM_STR);
            $stmt->bindValue(':id', $this->id, PDO::PARAM_STR);
            $stmt->execute();
            if($stmt){
                $_SESSION['update'] = 'Data successfully Updated !!!';
                header('location:index.php');
            }

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }



}
