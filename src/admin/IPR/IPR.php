<?php

namespace App\admin\IPR;
if(!isset($_SESSION)){
    session_start();
}
use App\Connection;
use PDOException;
use PDO;
class IPR extends Connection
{
    private $client_type;
    private $company_name;
    private $paitent_name;
    private $age;
    private $gender;
    private $f_or_h_name;
    private $guardian_name;
    private $address;
    private $religion;
    private $contact_no;
    private $department;
    private $admission_doc;
    private $consultant;
    private $diagnosis;
    private $reffered_by;
    private $occupation;
    private $reg_no;
    private $bed_no;
    private $date_time;
    private $time;
    private $height;
    private $weight;
    private $sbp;
    private $dbp;
    private $placeId;
    private $id;
    public function set($data = array()){

        if(array_key_exists('client_type',$data)){
            $this->client_type = $data['client_type'];
        }
        if(array_key_exists('company_name',$data)){
            $this->company_name = $data['company_name'];
        }
        if(array_key_exists('paitent_name',$data)){
            $this->paitent_name = $data['paitent_name'];
        }
        if(array_key_exists('age',$data)){
            $this->age = $data['age'];
        }
        if(array_key_exists('gender',$data)){
            $this->gender = $data['gender'];
        }
        if(array_key_exists('f_or_h_name',$data)){
            $this->f_or_h_name = $data['f_or_h_name'];
        }

        if(array_key_exists('guardian_name',$data)){
            $this->guardian_name = $data['guardian_name'];
        }

        if(array_key_exists('address',$data)){
            $this->address = $data['address'];
        }
        if(array_key_exists('religion',$data)){
            $this->religion = $data['religion'];
        }
        if(array_key_exists('contact_no',$data)){
            $this->contact_no = $data['contact_no'];
        }
        if(array_key_exists('department',$data)){
            $this->department = $data['department'];
        }

        if(array_key_exists('admission_doc',$data)){
            $this->admission_doc = $data['admission_doc'];
        }

        if(array_key_exists('consultant',$data)){
            $this->consultant = $data['consultant'];
        }

        if(array_key_exists('diagnosis',$data)){
            $this->diagnosis = $data['diagnosis'];
        }
        if(array_key_exists('reffered_by',$data)){
            $this->reffered_by = $data['reffered_by'];
        }

        if(array_key_exists('occupation',$data)){
            $this->occupation = $data['occupation'];
        }

        if(array_key_exists('reg_no',$data)){
            $this->reg_no = $data['reg_no'];
        }
        if(array_key_exists('bed_no',$data)){
            $this->bed_no = $data['bed_no'];
        }

        if(array_key_exists('date_time',$data)){
            $this->date_time = $data['date_time'];
        }
        if(array_key_exists('time',$data)){
            $this->time = $data['time'];
        }

        if(array_key_exists('height',$data)){
            $this->height = $data['height'];
        }
        if(array_key_exists('weight',$data)){
            $this->weight = $data['weight'];
        }
        if(array_key_exists('sbp',$data)){
            $this->sbp = $data['sbp'];
        }
        if(array_key_exists('dbp',$data)){
            $this->dbp = $data['dbp'];
        }
        if(array_key_exists('placeId',$data)){
            $this->placeId = $data['placeId'];
        }
        if(array_key_exists('id',$data)){
            $this->id = $data['id'];
        }
        return $this;
    }

    public function store(){
        try {

            $stm =  $this->con->prepare("INSERT INTO `ipr`(`client_type`, `company_name`, `paitent_name`, `age`, `gender`, `f_or_h_name`, `guardian_name`, `address`, `religion`, `contact_no`, `department`, `admission_doc`, `consultant`, `diagnosis`, `reffered_by`, `occupation`, `reg_no`, `bed_no`, `date_time`, `time`, `height`, `weight`, `sbp`, `dbp`) 
                                        VALUES(:client_type, :company_name, :paitent_name, :age, :gender, :f_or_h_name, :guardian_name, :address, :religion, :contact_no, :department, :admission_doc, :consultant, :diagnosis, :reffered_by, :occupation, :reg_no, :bed_no, :date_time, :time, :height, :weight, :sbp, :dbp)");
            $result =$stm->execute(array(
                ':client_type' => $this->client_type,
                ':company_name' => $this->company_name,
                ':paitent_name' => $this->paitent_name,
                ':age' => $this->age,
                ':gender' => $this->gender,
                ':f_or_h_name' => $this->f_or_h_name,
                ':guardian_name' => $this->guardian_name,
                ':address' => $this->address,
                ':religion' => $this->religion,
                ':contact_no' => $this->contact_no,
                ':department' => $this->department,
                ':admission_doc' => $this->admission_doc,
                ':consultant' => $this->consultant,
                ':diagnosis' => $this->diagnosis,
                ':reffered_by' => $this->reffered_by,
                ':occupation' => $this->occupation,
                ':reg_no' => $this->reg_no,
                ':bed_no' => $this->bed_no,
                ':date_time' => $this->date_time,
                ':time' => $this->time,
                ':height' => $this->height,
                ':weight' => $this->weight,
                ':sbp' => $this->sbp,
                ':dbp' => $this->dbp

            ));
            if($result){
                $_SESSION['msg'] = 'Data successfully Inserted !!!';
                header('location:index.php');
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
    public function index(){
        try {

            $stm =  $this->con->prepare("SELECT * FROM `ipr` WHERE `deleted_at` = '0000-00-00 00:00:00'");

            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }



    public function view($id){
        try {


            $stm =  $this->con->prepare("SELECT * FROM `ipr` WHERE id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();
            return $stm->fetch(PDO::FETCH_ASSOC);



        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function trash(){
        try {

            $stm =  $this->con->prepare("SELECT * FROM `ipr` WHERE `deleted_at` != '0000-00-00 00:00:00'");

            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }



    public function tmp_delete($id){
        try {

            $stm =  $this->con->prepare("UPDATE `ipr` SET `deleted_at` = NOW() WHERE id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();
            if($stm){
                $_SESSION['delete'] = 'Data successfully Deleted !!!';
                header('location:index.php');
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function restore($id){
        try {

            $stm =  $this->con->prepare("UPDATE `ipr` SET `deleted_at` = '0000-00-00 00:00:00' WHERE id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();
            if($stm){
                $_SESSION['msg'] = 'Data Restore successfully !!!';
                header('location:trash.php');
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function delete($id){
        try {

            $stm =  $this->con->prepare("DELETE FROM `ipr` WHERE id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();
            if($stm){
                $_SESSION['delete'] = 'Data successfully Deleted !!!';
                header('location:index.php');
            }

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function update(){
        try {

            $stmt = $this->con->prepare("UPDATE `ipr` SET `client_type` = :client_type, `company_name`= :company_name, `paitent_name`= :paitent_name, `age`= :age, `gender`= :gender, `f_or_h_name`= :f_or_h_name, `guardian_name`= :guardian_name, `address`= :address, `religion`= :religion, `contact_no`= :contact_no, `department`= :department, `admission_doc`= :admission_doc, `consultant`= :consultant, `diagnosis`= :diagnosis, `reffered_by`= :reffered_by, `occupation`= :occupation, `reg_no`= :reg_no, `bed_no`= :bed_no, `date_time`= :date_time, `height`= :height, `weight`= :weight, `sbp`= :sbp, `dbp`= :dbp WHERE `ipr`.`id` = :id;");
            $stmt->bindValue(':client_type', $this->client_type, PDO::PARAM_STR);
            $stmt->bindValue(':company_name', $this->company_name, PDO::PARAM_STR);
            $stmt->bindValue(':paitent_name', $this->paitent_name, PDO::PARAM_STR);
            $stmt->bindValue(':age', $this->age, PDO::PARAM_STR);
            $stmt->bindValue(':gender', $this->gender, PDO::PARAM_STR);
            $stmt->bindValue(':f_or_h_name', $this->f_or_h_name, PDO::PARAM_STR);
            $stmt->bindValue(':guardian_name', $this->guardian_name, PDO::PARAM_STR);
            $stmt->bindValue(':address', $this->address, PDO::PARAM_STR);
            $stmt->bindValue(':religion', $this->religion, PDO::PARAM_STR);
            $stmt->bindValue(':contact_no', $this->contact_no, PDO::PARAM_STR);
            $stmt->bindValue(':department', $this->department, PDO::PARAM_STR);
            $stmt->bindValue(':admission_doc', $this->admission_doc, PDO::PARAM_STR);
            $stmt->bindValue(':consultant', $this->consultant, PDO::PARAM_STR);
            $stmt->bindValue(':diagnosis', $this->diagnosis, PDO::PARAM_STR);
            $stmt->bindValue(':reffered_by', $this->reffered_by, PDO::PARAM_STR);
            $stmt->bindValue(':occupation', $this->occupation, PDO::PARAM_STR);
            $stmt->bindValue(':reg_no', $this->reg_no, PDO::PARAM_STR);
            $stmt->bindValue(':bed_no', $this->bed_no, PDO::PARAM_STR);
            $stmt->bindValue(':date_time', $this->date_time, PDO::PARAM_STR);
            $stmt->bindValue(':height', $this->height, PDO::PARAM_STR);
            $stmt->bindValue(':weight', $this->weight, PDO::PARAM_STR);
            $stmt->bindValue(':sbp', $this->sbp, PDO::PARAM_STR);
            $stmt->bindValue(':dbp', $this->dbp, PDO::PARAM_STR);
            $stmt->bindValue(':id', $this->id, PDO::PARAM_STR);
            $stmt->execute();
            if($stmt){
                $_SESSION['update'] = 'Data successfully Updated !!!';
                header('location:index.php');
            }

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }



}
