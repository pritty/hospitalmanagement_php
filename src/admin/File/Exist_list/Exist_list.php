<?php

namespace App\admin\File\Exist_list;
if(!isset($_SESSION)){
    session_start();
}
use App\Connection;
use PDOException;
use PDO;
class Exist_list extends Connection
{
    private $Ecode;
    private $Ename;
    private $id;
    public function set($data = array()){

        if(array_key_exists('Ecode',$data)){
            $this->Ecode = $data['Ecode'];
        }
        if(array_key_exists('Ename',$data)){
            $this->Ename = $data['Ename'];
        }
        if(array_key_exists('id',$data)){
            $this->id = $data['id'];
        }
        return $this;
    }

    public function store(){
        try {

            $stm =  $this->con->prepare("INSERT INTO `exist_list`(`Ecode`, `Ename`) 
                                        VALUES(:Ecode, :Ename)");
            $result =$stm->execute(array(
                ':Ecode' => $this->Ecode,
                ':Ename' => $this->Ename

            ));
            if($result){
                $_SESSION['msg'] = 'Data successfully Inserted !!!';
                header('location:index.php');
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
    public function index(){
        try {

            $stm =  $this->con->prepare("SELECT * FROM `exist_list` WHERE `deleted_at` = '0000-00-00 00:00:00'");

            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function view($id){
        try {

            $stm =  $this->con->prepare("SELECT * FROM `exist_list` WHERE id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();
            return $stm->fetch(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function trash(){
        try {

            $stm =  $this->con->prepare("SELECT * FROM `exist_list` WHERE `deleted_at` != '0000-00-00 00:00:00'");

            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }



    public function tmp_delete($id){
        try {

            $stm =  $this->con->prepare("UPDATE `exist_list` SET `deleted_at` = NOW() WHERE id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();
            if($stm){
                $_SESSION['delete'] = 'Data successfully Deleted !!!';
                header('location:index.php');
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function restore($id){
        try {

            $stm =  $this->con->prepare("UPDATE `exist_list` SET `deleted_at` = '0000-00-00 00:00:00' WHERE id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();
            if($stm){
                $_SESSION['msg'] = 'Data Successfully Restored !!!';
                header('location:trash.php');
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function delete($id){
        try {

            $stm =  $this->con->prepare("DELETE FROM `exist_list` WHERE id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();
            if($stm){
                $_SESSION['delete'] = 'Data successfully Deleted !!!';
                header('location:index.php');
            }

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function update(){
        try {

            $stmt = $this->con->prepare("UPDATE `exist_list` SET `Ecode` = :Ecode, `Ename`= :Ename WHERE `exist_list`.`id` = :id;");
            $stmt->bindValue(':Ecode', $this->Ecode, PDO::PARAM_STR);
            $stmt->bindValue(':Ename', $this->Ename, PDO::PARAM_STR);
            $stmt->bindValue(':id', $this->id, PDO::PARAM_STR);
            $stmt->execute();
            if($stmt){
                $_SESSION['update'] = 'Data successfully Updated !!!';
                header('location:index.php');
            }

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }



}
