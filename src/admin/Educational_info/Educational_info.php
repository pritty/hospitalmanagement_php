<?php

namespace App\admin\Educational_info;
if(!isset($_SESSION)){
    session_start();
}
use App\Connection;
use PDOException;
use PDO;
class Educational_info extends Connection
{
    private $doc_id;
    private $ssc_school_name;
    private $ssc_passing_year;
    private $hsc_college_name;
    private $hsc_passing_year;
    private $mbbs_college_name;
    private $mbbs_passing_year;
    private $fcps_inst_name;
    private $fcps_passing_year;
    private $other_inst_name;
    private $other_passing_year;
    private $id;
    public function set($data = array()){

        if(array_key_exists('doc_id',$data)){
            $this->doc_id = $data['doc_id'];
        }
        if(array_key_exists('ssc_school_name',$data)){
            $this->ssc_school_name = $data['ssc_school_name'];
        }
        if(array_key_exists('ssc_passing_year',$data)){
            $this->ssc_passing_year = $data['ssc_passing_year'];
        }
        if(array_key_exists('hsc_college_name',$data)){
            $this->hsc_college_name = $data['hsc_college_name'];
        }
        if(array_key_exists('hsc_passing_year',$data)){
            $this->hsc_passing_year = $data['hsc_passing_year'];
        }
        if(array_key_exists('mbbs_college_name',$data)){
            $this->mbbs_college_name = $data['mbbs_college_name'];
        }

        if(array_key_exists('mbbs_passing_year',$data)){
            $this->mbbs_passing_year = $data['mbbs_passing_year'];
        }

        if(array_key_exists('fcps_inst_name',$data)){
            $this->fcps_inst_name = $data['fcps_inst_name'];
        }
        if(array_key_exists('fcps_passing_year',$data)){
            $this->fcps_passing_year = $data['fcps_passing_year'];
        }
        if(array_key_exists('other_inst_name',$data)){
            $this->other_inst_name = $data['other_inst_name'];
        }
        if(array_key_exists('other_passing_year',$data)){
            $this->other_passing_year = $data['other_passing_year'];
        }
        if(array_key_exists('id',$data)){
            $this->id = $data['id'];
        }
        return $this;
    }

    public function store(){
        try {

            $stm =  $this->con->prepare("INSERT INTO `educational_info`(`doc_id`, `ssc_school_name`, `ssc_passing_year`, `hsc_college_name`, `hsc_passing_year`, `mbbs_college_name`, `mbbs_passing_year`, `fcps_inst_name`, `fcps_passing_year`, `other_inst_name`, `other_passing_year`) 
                                        VALUES(:doc_id, :ssc_school_name, :ssc_passing_year, :hsc_college_name, :hsc_passing_year, :mbbs_college_name, :mbbs_passing_year, :fcps_inst_name, :fcps_passing_year, :other_inst_name, :other_passing_year)");
            $result =$stm->execute(array(
                ':doc_id' => $this->doc_id,
                ':ssc_school_name' => $this->ssc_school_name,
                ':ssc_passing_year' => $this->ssc_passing_year,
                ':hsc_college_name' => $this->hsc_college_name,
                ':hsc_passing_year' => $this->hsc_passing_year,
                ':mbbs_college_name' => $this->mbbs_college_name,
                ':mbbs_passing_year' => $this->mbbs_passing_year,
                ':fcps_inst_name' => $this->fcps_inst_name,
                ':fcps_passing_year' => $this->fcps_passing_year,
                ':other_inst_name' => $this->other_inst_name,
                ':other_passing_year' => $this->other_passing_year

            ));
            if($result){
                $_SESSION['msg'] = 'Data successfully Inserted !!!';
                header('location:index.php');
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
    public function index(){
        try {

            $stm =  $this->con->prepare("SELECT * FROM `educational_info` WHERE `deleted_at` = '0000-00-00 00:00:00'");

            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function view($id){
        try {

            $stm =  $this->con->prepare("SELECT * FROM `educational_info` WHERE id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();
            return $stm->fetch(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function trash(){
        try {

            $stm =  $this->con->prepare("SELECT * FROM `educational_info` WHERE `deleted_at` != '0000-00-00 00:00:00'");

            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }



    public function tmp_delete($id){
        try {

            $stm =  $this->con->prepare("UPDATE `educational_info` SET `deleted_at` = NOW() WHERE id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();
            if($stm){
                $_SESSION['delete'] = 'Data successfully Deleted !!!';
                header('location:index.php');
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function restore($id){
        try {

            $stm =  $this->con->prepare("UPDATE `educational_info` SET `deleted_at` = '0000-00-00 00:00:00' WHERE id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();
            if($stm){
                $_SESSION['msg'] = 'Data Restore successfully !!!';
                header('location:trash.php');
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function delete($id){
        try {

            $stm =  $this->con->prepare("DELETE FROM `educational_info` WHERE id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();
            if($stm){
                $_SESSION['delete'] = 'Data successfully Deleted !!!';
                header('location:index.php');
            }

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function update(){
        try {

            $stmt = $this->con->prepare("UPDATE `educational_info` SET `doc_id` = :doc_id, `ssc_school_name`= :ssc_school_name, `ssc_passing_year`= :ssc_passing_year, `hsc_college_name`= :hsc_college_name, `hsc_passing_year`= :hsc_passing_year, `mbbs_college_name`= :mbbs_college_name, `mbbs_passing_year`= :mbbs_passing_year, `fcps_inst_name`= :fcps_inst_name, `fcps_passing_year`= :fcps_passing_year, `other_inst_name`= :other_inst_name, `other_passing_year`= :other_passing_year WHERE `educational_info`.`id` = :id;");
            $stmt->bindValue(':doc_id', $this->doc_id, PDO::PARAM_STR);
            $stmt->bindValue(':ssc_school_name', $this->ssc_school_name, PDO::PARAM_STR);
            $stmt->bindValue(':ssc_passing_year', $this->ssc_passing_year, PDO::PARAM_STR);
            $stmt->bindValue(':hsc_college_name', $this->hsc_college_name, PDO::PARAM_STR);
            $stmt->bindValue(':hsc_passing_year', $this->hsc_passing_year, PDO::PARAM_STR);
            $stmt->bindValue(':mbbs_college_name', $this->mbbs_college_name, PDO::PARAM_STR);
            $stmt->bindValue(':mbbs_passing_year', $this->mbbs_passing_year, PDO::PARAM_STR);
            $stmt->bindValue(':fcps_inst_name', $this->fcps_inst_name, PDO::PARAM_STR);
            $stmt->bindValue(':fcps_passing_year', $this->fcps_passing_year, PDO::PARAM_STR);
            $stmt->bindValue(':other_inst_name', $this->other_inst_name, PDO::PARAM_STR);
            $stmt->bindValue(':other_passing_year', $this->other_passing_year, PDO::PARAM_STR);
            $stmt->bindValue(':id', $this->id, PDO::PARAM_STR);
            $stmt->execute();
            if($stmt){
                $_SESSION['update'] = 'Data successfully Updated !!!';
                header('location:index.php');
            }

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }



}
