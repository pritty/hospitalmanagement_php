<?php
if(!isset($_SESSION)){
    session_start();
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Hospital Management</title>

    <base href="http://localhost:8080/pro/hospital/"  />

    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="assets/admin2/apple-touch-icon.png">
    <!-- Place favicon.ico in the root directory -->

    <link rel="stylesheet" href="assets/admin2/css/normalize.css">
    <link rel="stylesheet" href="assets/admin2/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/admin2/css/main.css">
    <link rel="stylesheet" href="assets/admin2/css/select2.min.css">





    <script src="assets/admin2/js/vendor/modernizr-2.8.3.min.js"></script>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="assets/admin/bower_components/font-awesome/css/font-awesome.min.css">

    <link rel="stylesheet" href="assets/admin/dist/css/AdminLTE.min.css">
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="assets/admin/plugins/timepicker/bootstrap-timepicker.min.css">
    <!-- Bootstrap Date Picker -->
    <link rel="stylesheet" href="assets/admin/plugins/datepicker/bootstrap-datepicker.min.css">

    <!-- jQuery UI 1.11.4 -->
    <link rel="stylesheet" href="assets/admin/bower_components/jquery-ui/themes/base/jquery-ui.css"/>
    <script src="assets/admin/bower_components/jquery-ui/jquery-ui.min.js"></script>
    <!-- DataTables -->
    <link rel="stylesheet" href="assets/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="assets/admin2/style.css" type="text/css" media="all" />
    <link rel="stylesheet" href="assets/blackSearch/blackSearch.css" type="text/css" media="all" />

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<!-- Add your site or application content here -->

<style>

</style>

<div class="full-wrapper">
    <header class="header">

        <div class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <a class="navbar-brand" href="#"></a>
                </div>

                <div class="collapse navbar-collapse">

                    <ul class="nav navbar-nav">
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">File <b class="caret"></b></a>
                            <ul class="dropdown-menu multi-level">
                                <li><a href="view/admin/file/collection_heads_entry/index.php">Collection Heads Entry</a></li>
                                <li><a href="view/admin/file/expenses_heads_entry/index.php">Expenses Heads Entry</a></li>
                                <li><a href="view/admin/file/bank_a_c_setup/index.php">Bank A/C setup</a></li>
                                <li><a href="view/admin/file/daily_expenditure_entry/index.php">Daily Expenditure Entry</a></li>
                                <li><a href="view/admin/file/daily_collection_entry/index.php">Daily Collection Entry</a></li>
                                <li><a href="view/admin/file/daily_expenditure_ledgre/index.php">Daily Expenditure Ledgre</a></li>
                                <li><a href="view/admin/file/daily_collection_expenditure/index.php">Daily Collection Expenditure Sheet</a></li>
                                <li><a href="view/admin/file/daily_collection_expenditure_ledgre/index.php">Daily Collection Expenditure Ledgre</a></li>
                                <li><a href="view/admin/file/collection_history/index.php">Collection History</a></li>
                                <li><a href="view/admin/file/money_receipt/index.php">Money Receipt</a></li>
                                <li><a href="view/admin/file/exist_list/index.php">Exist List</a></li>

                                <li class="divider"></li>
                                <li><a href="#">Vacant/ Booked Cabin</a></li>
                                <li><a href="view/admin/file/birth_certificate/index.php">Birth Certificate</a></li>
                                <li><a href="view/admin/file/death_certificate/index.php">Death Certificate</a></li>

                                <li class="divider"></li>
                                <li><a href="view/admin/file/paitent_record/index.php">Paitent Admission Register Record</a></li>

                                <li class="dropdown-submenu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Paitent Outstanding Bill <b class=""></b></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="view/admin/file/paitent_outstanding_bill/index.php">Paitent Outstanding Bill List</a></li>
                                        <li><a href="">Paitent Outstanding Money Recipt</a></li>
                                        <li><a href="view/admin/file/discharge_paitent_outstanding_bill/index.php">Discharge Paitent Outstanding Bill List</a></li>
                                        <li><a href="view/admin/file/vat_payment/index.php">Vat Payment</a></li>
                                    </ul>
                                </li>

                                <li><a href="#">Printer Setup</a></li>
                                <li><a href="#">Country</a></li>
                                <li><a href="#">Special Menu</a></li>
                            </ul>
                        </li>
                        <!--                ===========end file menu===========-->
                        <!--                ===========start medical menu===========-->
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Medical <b class="caret"></b></a>
                            <ul class="dropdown-menu multi-level">
                                <li><a href="view/admin/call_history/index.php">Call Tracking</a></li>

                                <li class="divider"></li>
                                <li><a href="view/admin/on_call_consultant/index.php">On Call Consultant</a></li>
                                <li><a href="view/admin/consultancy/index.php">Consultancy/Medical Board Fees</a></li>
                                <li class="dropdown-submenu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Outdoor</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="view/admin/outdoor_registration/index.php">Outdoor Registration</a></li>
                                        <li><a href="view/admin/opd_bill/index.php">OPD Bill</a></li>
                                        <li class="divider"></li>
                                        <li class="dropdown-submenu">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Report</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="view/admin/outdoor_patient_slip/index.php">Patient Reg Slip</a></li>
                                                <li><a href="view/admin/outdoor_patient_invoice/index.php">Patient Invoice</a></li>
                                                <li class="divider"></li>
                                                <li><a href="view/admin/outdoor_patient_history/index.php">Outdoor Details(Date Wise)</a></li>
                                                <li><a href="view/admin/doctorwise_outdoor_patient_history/index.php">Outdoor Doctor Details(Date Wise)</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li class="divider"></li>
                                <li class="dropdown-submenu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Indoor</a>
                                    <ul class="dropdown-menu">
                                        <li><a>Searching Paitent</a></li>
                                        <li><a href="view/admin/IPR/index.php">Patient Registration</a></li>
                                        <li><a>Patient Reopen</a></li>
                                        <li><a href="view/admin/patient_admission_ticket/index.php">Patient Admission Ticket (View)</a></li>
                                        <li class="divider"></li>
                                        <li><a href="view/admin/patient_seat_rent_entry">Patient Seat Rent Entry</a></li>
                                        <li><a href="view/admin/patient_daily_expance/index.php">Patient Daily Expencess Entry</a></li>
                                        <li class="divider"></li>
                                        <li><a href="view/admin/patient_bill/index.php">Patient Bill</a></li>

                                        <li class="divider"></li>
                                        <li class="dropdown-submenu">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Collection</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="view/admin/patient_discount/index.php">Patient Discount Screen</a></li>
                                                <li><a href="view/admin/close_patient_discount/index.php">Close Patient Discount Screen</a></li>

                                                <li class="divider"></li>
                                                <li><a href="view/admin/patient_money_recipt/index.php">Patient Money Receipt</a></li>
                                                <li><a href="view/admin/close_patient_money_recipt/index.php">Close Patient Money Receipt</a></li>
                                                <li><a href="view/admin/corporate_money_recipt/index.php">Corporate Money Receipt</a></li>

                                            </ul>
                                        </li>


                                        <li class="dropdown-submenu">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Patient Discharge</a>
                                            <ul class="dropdown-menu">
                                                <li class="dropdown-submenu">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Additional Setup</a>
                                                    <ul class="dropdown-menu">

                                                        <li><a href="view/admin/discharge_advice/index.php">Discharge Advice</a></li>
                                                        <li><a href="view/admin/diet_detail/index.php">Diet Details</a></li>
                                                        <li class="divider"></li>
                                                        <li><a href="#">Bengali Instruction-1</a></li>
                                                        <li><a href="#">Bengali Instruction-2</a></li>
                                                        <li><a href="#">Bengali Instruction-3</a></li>
                                                        <li><a href="#">English Instruction-4</a></li>
                                                    </ul>
                                                </li>

                                                <li><a href="view/admin/patient_discharge_note_treatment/index.php">Patient Discharge Note Treatment</a></li>
                                                <li><a href="view/admin/patient_discharge_certificate/index.php">Patient Discharge Certificate</a></li>
                                                <li><a href="view/admin/patient_discharge_certificate_old/index.php">Patient Discharge Certificate (Old)</a></li>
                                            </ul>
                                        </li>


                                        <li class="dropdown-submenu">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Report</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="view/admin/admitted_patient_datewise/index.php">Admitted Patient(Datewise)</a></li>
                                                <li><a>Patient Daily Expences</a></li>
                                                <li><a href="view/admin/discharge_patient_datewise/index.php">Discharge Patient List</a></li>
                                                <li><a href="view/admin/corporate_ledger/index.php">Corporate ledger</a></li>
                                                <li><a>Current Patient List</a></li>
                                                <li><a href="view/admin/patient_bed_change_history/index.php">Patient Bed/Cabin Change History</a></li>

                                            </ul>
                                        </li>

                                        <li class="divider"></li>
                                        <li><a href="view/admin/change_patient_bedcabin/index.php">Change Patient Bed/Cabin</a></li>
                                        <li><a href="view/admin/edit_bed_time/index.php">Edit Bed Time</a></li>
                                        <li><a href="view/admin/bed_cabin/index.php">Bed/Cabin Preparation</a></li>

                                        <li class="divider"></li>
                                        <li><a href="view/admin/corporate/index.php">Corporate Clint Setup</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <!--================end menu medical============-->


                        <!--================start menu medicine============-->
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Medicines <b class="caret"></b></a>
                            <ul class="dropdown-menu multi-level">
                                <li class="dropdown-submenu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Vendors</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="view/admin/medicines/vendors_setup/index.php">Vendors Setup</a></li>
                                        <li><a href="view/admin/medicines/vendors_purcchase_payment/index.php">Vendors Purcchase Payment</a></li>

                                    </ul>
                                </li>
                                <li class="dropdown-submenu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Stocking Purchase</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="view/admin/medicines/stock_preparations/index.php">Stock Preparations</a></li>
                                        <li><a href="view/admin/medicines/medicines_purchase/index.php">Medicines Purchase</a></li>
                                    </ul>
                                </li>
                                <li class="divider"></li>
                                <li class="dropdown-submenu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Sales</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="view/admin/medicines/indoor_sales/index.php">Indoor Sales</a></li>
                                        <li><a href="view/admin/medicines/outdoor_sales/index.php">Sales Outdoor</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown-submenu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Return</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="view/admin/medicines/sales_return/index.php">Sales Return</a></li>
                                        <li><a href="view/admin/medicines/purchase_return/index.php">Purchase Return</a></li>
                                    </ul>
                                </li>
                                <li><a href="">Medicine Transfer</a></li>
                                <li class="dropdown-submenu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Report</a>
                                    <ul class="dropdown-menu">
                                        <li class="dropdown-submenu">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Vendor Purchase</a>
                                            <ul class="dropdown-menu">
                                                <li class="dropdown-submenu">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Vendor</a>
                                                    <ul class="dropdown-menu">
                                                        <li><a>Vendor Ledger</a></li>
                                                        <li><a>Creditor List</a></li>
                                                    </ul>
                                                </li>

                                                <li class="dropdown-submenu">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Purchase</a>
                                                </li>
                                            </ul>
                                        </li>


                                        <li><a href="view/admin/medicines/sales/index.php">Sales</a></li>
                                        <li><a href="">Medicine Sales (Date Wise)</a></li>
                                    </ul>

                                </li>
                            </ul>
                        </li>
                        <!--================end medicines menu============-->

                        <!--================start Diagnosis Point menu============-->
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Diagnosis Point <b class="caret"></b></a>
                            <ul class="dropdown-menu multi-level">

                                <li class="dropdown-submenu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Setup</a>
                                    <ul class="dropdown-menu">
                                        <li><a href=""view/admin/diagnosis/doctor/index.php"">Doctors Information</a></li>
                                    </ul>
                                </li>

                                <li class="dropdown-submenu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Stock IN</a>
                                    <ul class="dropdown-menu">
                                        <li><a>Purchase Entry</a></li>
                                        <li><a>Pathology Stock Setup</a></li>
                                    </ul>
                                </li>

                                <li><a href="#">Patient Diagnosis Test</a></li>

                                <li class="divider"></li>
                                <li><a href="#">Pathology and Others</a></li>
                                <li><a href="#">Febraille Antigen</a></li>
                                <li><a href="#">Haematology</a></li>
                                <li><a href="#">Lipid & Electro</a></li>
                                <li><a href="#">Stool</a></li>
                                <li><a href="#">Tuberculin</a></li>
                                <li><a href="#">Urinalysis</a></li>
                                <li><a href="#">UrinalysisM</a></li>
                                <li><a href="#">Widal</a></li>

                                <li class="divider"></li>
                                <li><a href="#">Diagnosis Report Delivary</a></li>

                                <li class="dropdown-submenu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Outdoor Reg</a>
                                    <ul class="dropdown-menu">
                                        <li><a>Outdoor Registrations</a></li>

                                        <li class="dropdown-submenu">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Reports</a>
                                            <ul class="dropdown-menu">
                                                <li><a>Patient Reg Slip</a></li>
                                                <li><a>Patient Invoice</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>

                                <li class="dropdown-submenu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Report</a>
                                    <ul class="dropdown-menu">
                                        <li><a>Patient Test Report (Date Wise)</a></li>
                                        <li><a>Diagnosis Collection History</a></li>
                                        <li><a>Pathology Stock Used (Date Wise)</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>

                        <!--================End Diagnosis Point menu============-->


                        <!--================Start O.T Management  menu============-->
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">O.T Management<b class="caret"></b></a>
                            <ul class="dropdown-menu multi-level">
                                <li><a href="#">Doctors Information</a></li>
                                <li><a href="#">Operation Setup</a></li>
                                <li><a href="#">Operation Schedule</a></li>
                                <li><a href="#">Operation Tracking</a></li>
                                <li><a href="#">Operation Register Book</a></li>
                                <li><a href="#">Operation Payment Book</a></li>
                                <li><a href="#">Doctors Payment</a></li>

                            </ul>
                        </li>
                        <!--================End O.T Management menu============-->

                        <!--================Start Payroll  menu============-->
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Payroll<b class="caret"></b></a>
                            <ul class="dropdown-menu multi-level">
                                <li class="dropdown-submenu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Setup</a>
                                    <ul class="dropdown-menu">
           
                                    </ul>
                                </li>


                                <li><a href="#">Doctor's Ledger</a></li>
                                <li><a href="#">Doctor's Payment</a></li>

                            </ul>
                        </li>
                        <!--================End Payroll  menu============-->


                        <!--================Start Maintenance System  menu============-->
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Maintenance System<b class="caret"></b></a>
                            <ul class="dropdown-menu multi-level">
                                <li><a href="#">Check System</a></li>

                                <li class="dropdown-submenu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Special Menu</a>
                                    <ul class="dropdown-menu">
                                        <li><a>Open Regtration</a></li>
                                    </ul>
                                </li>

                                <li><a href="#">Backup / Restore</a></li>

                                <li class="dropdown-submenu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Security</a>
                                    <ul class="dropdown-menu">
                                        <li><a>New User</a></li>
                                        <li><a>Vat Processing</a></li>
                                        <li><a>Set Serial No (Vat)</a></li>
                                    </ul>
                                </li>

                            </ul>
                        </li>
                        <!--================End Maintenance System  menu============-->


                        <!--================Start Others  menu============-->
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Others<b class="caret"></b></a>
                            <ul class="dropdown-menu multi-level">
                                <li><a href="#">About</a></li>
                                <li><a href="#">Change Password</a></li>
                                <li><a href="#">Set Bed Time</a></li>
                                <li><a href="#">Clear Memory</a></li>
                                <li><a href="#">Log Off</a></li>
                                <li><a href="#">Exit</a></li>

                            </ul>
                        </li>


                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </div>

    </header>
