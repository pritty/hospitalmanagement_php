<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li class="active treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="active"><a href="index.php"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class=""></i>
                    <span>Bed Cabin</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="view/admin/bed_cabin/create.php"><i class="fa fa-circle-o"></i> Add</a></li>
                    <li><a href="view/admin/bed_cabin/index.php"><i class="fa fa-circle-o"></i>View</a></li>
                    <li><a href="view/admin/bed_cabin/trash.php"><i class="fa fa-trash"></i>Trash</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class=""></i>
                    <span>Corporate</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="view/admin/corporate/create.php"><i class="fa fa-circle-o"></i> Add </a></li>
                    <li><a href="view/admin/corporate/index.php"><i class="fa fa-circle-o"></i>View </a></li>
                    <li><a href="view/admin/corporate/trash.php"><i class="fa fa-trash"></i> Trash</a></li>
                </ul>
            </li>





            <li class="treeview">
                <a href="#">
                    <i class=""></i>
                    <span>Department</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="view/admin/department/create.php"><i class="fa fa-circle-o"></i> Add </a></li>
                    <li><a href="view/admin/department/index.php"><i class="fa fa-circle-o"></i>View </a></li>
                    <li><a href="view/admin/department/trash.php"><i class="fa fa-trash"></i>Trash</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class=""></i>
                    <span>Doctor</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="view/admin/doctor/create.php"><i class="fa fa-circle-o"></i> Add </a></li>
                    <li><a href="view/admin/doctor/index.php"><i class="fa fa-circle-o"></i>View </a></li>
                    <li><a href="view/admin/doctor/trash.php"><i class="fa fa-trash"></i>Trash</a></li>
                </ul>
            </li>


            <li class="treeview">
                <a href="#">
                    <i class=""></i> <span>Educational Info</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="view/admin/educational_info/create.php"><i class="fa fa-circle-o"></i> Add </a></li>
                    <li><a href="view/admin/educational_info/index.php"><i class="fa fa-circle-o"></i>View </a></li>
                    <li><a href="view/admin/educational_info/trash.php"><i class="fa fa-trash"></i>Trash</a></li>
                </ul>
            </li>


            <li class="treeview">
                <a href="#">
                    <i class=""></i> <span>Experiance Detail</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="view/admin/experiance_detail/create.php"><i class="fa fa-circle-o"></i> Add </a></li>
                    <li><a href="view/admin/experiance_detail/index.php"><i class="fa fa-circle-o"></i>View </a></li>
                    <li><a href="view/admin/experiance_detail/trash.php"><i class="fa fa-trash"></i>Trash</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class=""></i> <span>Traininf Info</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="view/admin/training_info/create.php"><i class="fa fa-circle-o"></i> Add </a></li>
                    <li><a href="view/admin/training_info/index.php"><i class="fa fa-circle-o"></i>View </a></li>
                    <li><a href="view/admin/training_info/trash.php"><i class="fa fa-trash"></i>Trash</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class=""></i> <span>IPR</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="view/admin/IPR/create.php"><i class="fa fa-circle-o"></i> Add </a></li>
                    <li><a href="view/admin/IPR/index.php"><i class="fa fa-circle-o"></i>View </a></li>
                    <li><a href="view/admin/IPR/trash.php"><i class="fa fa-trash"></i>Trash</a></li>
                </ul>
            </li>



        </ul>
    </section>
    <!-- /.sidebar -->
</aside>