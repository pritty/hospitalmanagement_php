<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; 2017 Sumsunnahar Pritty </strong> All rights
    reserved.
</footer>
</div>

<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
<script>
    window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')
</script>

<script src="assets/admin2/js/jquery-3.2.1.min.js"></script>
<script src="assets/admin/bower_components/jquery-ui/jquery-ui.min.js"></script>
<!--<script src="assets/admin2/js/jquery.min.js"></script>-->
<script src="assets/admin2/js/plugins.js"></script>
<script src="assets/admin2/js/bootstrap.min.js"></script>

<script src="assets/admin2/js/select2.min.js"></script>


<!-- DataTables -->
<script src="assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="assets/admin2/js/main.js"></script>
<script src="assets/blackSearch/blackSearchableDropDown.js"></script>
<!-- daterangepicker -->
<script src="assets/admin/bower_components/moment/min/moment.min.js"></script>
<!-- bootstrap time picker -->
<script src="assets/admin/plugins/timepicker/bootstrap-timepicker.min.js"></script>

<!-- Bootstrap Date Picker -->
<link rel="stylesheet" href="assets/admin/plugins/datepicker/bootstrap-datepicker.min.js">

<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
    (function (b, o, i, l, e, r) {
        b.GoogleAnalyticsObject = l;
        b[l] || (b[l] =
            function () {
                (b[l].q = b[l].q || []).push(arguments)
            });
        b[l].l = +new Date;
        e = o.createElement(i);
        r = o.getElementsByTagName(i)[0];
        e.src = 'https://www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e, r)
    }(window, document, 'script', 'ga'));
    ga('create', 'UA-XXXXX-X', 'auto');
    ga('send', 'pageview');
</script>

<script>
    $(document).ready(function(){
        $("#fade").fadeOut(function () {
            speed:5000
        })

//        $( "#fade" ).fadeOut( "slow", function() {
//            // Animation complete.
////            var self  = $(this),
////                speed = 300,
//        });
    });
</script>

<script>
    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            'paging'      : true,
            'lengthChange': false,
            'searching'   : false,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false
        })
    })
</script>

</body>

</html>