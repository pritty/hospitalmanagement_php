
<?php
include_once '../../../vendor/autoload.php';
$dept = new \App\admin\Department\Department();
$dept=$dept->view($_GET['id']);
//var_dump($bed);
?>
<?php include_once '../include/header.php'?>

<div class="content-wrapper" style="">
    <section class="content-header">
        <h1>
            Corporate Registration

        </h1>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- Default box -->
            <div class="col-md-12">

                <div class="box box-primary">
                    <form role="form" action="view/admin/department/update.php" method="POST" enctype="multipart/form-data">
                        <div class="col-md-6">


                            <div class="form-group">
                                <label>Department ID</label>
                                <input name="dept_id" class="form-control" value="<?php echo $dept['dept_id'];?>" >
                                <input name="id" class="form-control" type="hidden" value="<?php echo $dept['id'];?>" >
                            </div>

                            <div class="form-group">
                                <label>Name</label>
                                <input name="name" class="form-control" value="<?php echo $dept['name'];?>">
                            </div>


                            <button type="submit" class="btn btn-primary">Update</button>
                            <a href="view/admin/department/index.php" type="submit" class="btn btn-default pull-right" style="margin-left:5px; ">Cancel</a>
                        </div>

                    </form>
                    <!-- /.box-footer-->
                </div>
                <!-- /.box -->
            </div>

        </div>

    </section>
    <!-- /.content -->

</div>
<!-- /.content-wrapper -->
<?php include_once '../include/footer.php'?>
