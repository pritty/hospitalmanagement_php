<?php

include_once '../../../vendor/autoload.php';
$experiance_detail = new \App\admin\Experiance_detail\Experiance_detail();
$experiance_detail=$experiance_detail->view($_GET['id']);
//var_dump($experiance_detail);

?>
<?php
include_once '../include/header.php';

?>

<?php
include_once '../include/sidebar.php';

?>


    <div class="content-wrapper">

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Exmerimental Information</h3>

            </div>

        </div>

        <!-- Main content -->
        <section class="content " style="min-height: 902.8px;">
            <!-- Small boxes (Stat box) -->

            <!-- /.row -->
            <!-- Main row -->
            <div class="row">
                <div class="col-lg-12">
                    <!-- Left col -->
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">Details of Experiances</h3>
                        </div>



                        <div class="box-body">


                            <div class="col-sm-6 invoice-col">

                                <b>Doctor ID:</b><?php echo $experiance_detail['doc_id'];?> <br>
                                <br>


                                <b>Experiance 1</b> <br>
                                <br>
                                <b>Institute Name:</b> <?php echo $experiance_detail['institute_name1'];?> <br>
                                <b>Post Name:</b><?php echo $experiance_detail['post1'];?> <br>
                                <b>Year of Experiance:</b> <?php echo $experiance_detail['year1'];?> <br>
                                <b>Joining Year:</b> <?php echo $experiance_detail['join_year1'];?> <br>


                                <b>Experiance 2</b> <br>
                                <br>
                                <b>Institute Name:</b> <?php echo $experiance_detail['institute_name2'];?> <br>
                                <b>Post Name:</b><?php echo $experiance_detail['post2'];?> <br>
                                <b>Year of Experiance:</b> <?php echo $experiance_detail['year2'];?> <br>
                                <b>Joining Year:</b> <?php echo $experiance_detail['join_year2'];?> <br>

                                <b>Experiance 3</b> <br>
                                <br>
                                <b>Institute Name:</b> <?php echo $experiance_detail['institute_name3'];?> <br>
                                <b>Post Name:</b><?php echo $experiance_detail['post3'];?> <br>
                                <b>Year of Experiance:</b> <?php echo $experiance_detail['year3'];?> <br>
                                <b>Joining Year:</b> <?php echo $experiance_detail['join_year3'];?> <br>

                                <b>Experiance 4</b> <br>
                                <br>
                                <b>Institute Name:</b> <?php echo $experiance_detail['institute_name4'];?> <br>
                                <b>Post Name:</b><?php echo $experiance_detail['post4'];?> <br>
                                <b>Year of Experiance:</b> <?php echo $experiance_detail['year4'];?> <br>
                                <b>Joining Year:</b> <?php echo $experiance_detail['join_year4'];?> <br>




                                <div  style="padding: 4%">
                                    <a class="btn btn-info" href="view/admin/experiance_detail/edit.php?id=<?php echo $experiance_detail['id']?>">Edit</a>
                                    <a class="btn btn-danger" data-toggle="modal" data-target="#myModal" href="" data-id="<?php echo $experiance_detail['id']?>">Delete</a>
                                </div>


                            </div>



                            </div>


                        <!-- /.form-box -->
                        <div>


                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <form action="view/admin/experiance_detail/tmp_delete.php" method="get">
                                        <input id="delete" type="hidden" name="id" value="<?php echo $experiance_detail['id'];?>">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title" id="myModalLabel">Are you sure want to Delete ?</h4>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                                                <button type="submit" class="btn btn-danger">Delete</button>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>


                        </div>
                    </div>
                </div>
                <!-- right col (We are only adding the ID to make the widgets sortable)-->

                <!-- right col -->
            </div>
            <!-- /.row (main row) -->

        </section>
        <!-- /.content -->
    </div>

<?php
include_once '../include/footer.php';
?>