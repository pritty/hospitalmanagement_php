

<?php
include_once '../../../vendor/autoload.php';
$experiance_detail = new \App\admin\Experiance_detail\Experiance_detail();
$experiance_details = $experiance_detail->trash();
//var_dump($edu_infos);
?>

<?php include_once '../include/header.php'?>
<?php include_once '../include/sidebar.php'?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Menus Tables
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <!-- /.box -->
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Menu Table With Full Features</h3>
                    </div>


                    <div style="position: fixed; right: 35px; top: 100px; z-index: 111">
                        <?php
                        if(isset($_SESSION['msg'])){
                            echo "<div class='alert alert-success'>".$_SESSION['msg']."</div>";
                            session_unset();
                        }
                        if(isset($_SESSION['delete'])){
                            echo "<div class='alert alert-danger'>".$_SESSION['delete']."</div>";
                            session_unset();
                        }
                        if(isset($_SESSION['update'])){
                            echo "<div class='alert alert-info'>".$_SESSION['update']."</div>";
                            session_unset();
                        }


                        ?>
                    </div>


                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Doctor Id</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody style="text-align: center">

                            <?php foreach ($experiance_details as $experiance_detail){?>
                                <tr>
                                    <td><?php echo $experiance_detail['doc_id'];?></td>
                                    <td>
                                        <a class="btn btn-info" href="view/admin/experiance_detail/restore.php?id=<?php echo $experiance_detail['id']?>">Restore</a>
                                        <a class="btn btn-info" href="view/admin/experiance_detail/trash_view.php?id=<?php echo $experiance_detail['id']?>">Detail</a>
                                    </td>
                                </tr>

                            <?php }?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>





<?php include_once '../include/footer.php'?>