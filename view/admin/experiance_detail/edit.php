<?php

include_once '../../../vendor/autoload.php';
$experiance_detail = new \App\admin\Experiance_detail\Experiance_detail();
$experiance_detail=$experiance_detail->view($_GET['id']);
//var_dump($experiance_detail);

?>

<?php
include_once '../include/header.php';

?>

<?php
include_once '../include/sidebar.php';

?>


    <div class="content-wrapper">

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Exmerimental Information</h3>

            </div>

        </div>

        <!-- Main content -->
        <section class="content " style="min-height: 902.8px;">
            <!-- Small boxes (Stat box) -->

            <!-- /.row -->
            <!-- Main row -->
            <div class="row">
                <div class="col-lg-12">
                    <!-- Left col -->
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">Enter Educational Info</h3>
                        </div>

                        <div class="box-body">
                            <div class="row">
                                <form role="form" action="view/admin/experiance_detail/update.php" method="POST" enctype="multipart/form-data">
                                    <div class="col-md-6">


                                        <div class="form-group">
                                            <label>Doctor ID</label>
                                            <select name="doc_id" id="" class="form-control">
                                                <option>Select One</option>
                                                <option value="1001">10001</option>
                                            </select>

                                        </div>

                                        <div>
                                            <label for="">Experiance 1</label>
                                            <div class="form-group">
                                                <label>Institute Name</label>
                                                <input name="institute_name1" class="form-control" value="<?php echo $experiance_detail['institute_name1'];?>">
                                                <input name="id" type="hidden" class="form-control" value="<?php echo $experiance_detail['id'];?>">
                                            </div>
                                            <div class="form-group">
                                                <label>Post Name</label>
                                                <input name="post1" class="form-control" value="<?php echo $experiance_detail['post1'];?>">
                                            </div>
                                            <div class="form-group">
                                                <label>Year of Experiance</label>
                                                <input name="year1" class="form-control" value="<?php echo $experiance_detail['year1'];?>">
                                            </div>
                                            <div class="form-group">
                                                <label>Joining Year</label>
                                                <input name="join_year1" class="form-control" value="<?php echo $experiance_detail['join_year1'];?>">
                                            </div>
                                        </div>

                                        <div>
                                            <label for="">Experiance 2</label>
                                            <div class="form-group">
                                                <label>Institute Name</label>
                                                <input name="institute_name2" class="form-control" value="<?php echo $experiance_detail['institute_name2'];?>">
                                            </div>
                                            <div class="form-group">
                                                <label>Post Name</label>
                                                <input name="post2" class="form-control" value="<?php echo $experiance_detail['post2'];?>">
                                            </div>
                                            <div class="form-group">
                                                <label>Year of Experiance</label>
                                                <input name="year2" class="form-control" value="<?php echo $experiance_detail['year2'];?>">
                                            </div>
                                            <div class="form-group">
                                                <label>Joining Year</label>
                                                <input name="join_year2" class="form-control" value="<?php echo $experiance_detail['join_year2'];?>">
                                            </div>
                                        </div>

                                        <div>

                                            <div>
                                                <label for="">Experiance 3</label>
                                                <div class="form-group">
                                                    <label>Institute Name</label>
                                                    <input name="institute_name3" class="form-control" value="<?php echo $experiance_detail['institute_name3'];?>">
                                                </div>
                                                <div class="form-group">
                                                    <label>Post Name</label>
                                                    <input name="post3" class="form-control" value="<?php echo $experiance_detail['post3'];?>">
                                                </div>
                                                <div class="form-group">
                                                    <label>Year of Experiance</label>
                                                    <input name="year3" class="form-control" value="<?php echo $experiance_detail['year3'];?>">
                                                </div>
                                                <div class="form-group">
                                                    <label>Joining Year</label>
                                                    <input name="join_year3" class="form-control" value="<?php echo $experiance_detail['join_year3'];?>">
                                                </div>
                                            </div>

                                            <div>
                                                <div>
                                                    <label for="">Experiance 4</label>
                                                    <div class="form-group">
                                                        <label>Institute Name</label>
                                                        <input name="institute_name4" class="form-control" value="<?php echo $experiance_detail['institute_name4'];?>">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Post Name</label>
                                                        <input name="post4" class="form-control" value="<?php echo $experiance_detail['post4'];?>">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Year of Experiance</label>
                                                        <input name="year4" class="form-control" value="<?php echo $experiance_detail['year4'];?>">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Joining Year</label>
                                                        <input name="join_year4" class="form-control" value="<?php echo $experiance_detail['join_year4'];?>">
                                                    </div>
                                                </div>

                                                <div>

                                                    <button type="submit" class="btn btn-primary">Update</button>
                                                </div>

                                </form>

                            </div>

                        </div>
                        <!-- /.form-box -->
                        <div>

                        </div>
                    </div>
                </div>
                <!-- right col (We are only adding the ID to make the widgets sortable)-->

                <!-- right col -->
            </div>
            <!-- /.row (main row) -->

        </section>
        <!-- /.content -->
    </div>

<?php
include_once '../include/footer.php';
?>