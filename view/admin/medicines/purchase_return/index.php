
<?php
include_once '../../../../vendor/autoload.php';


?>
<?php include_once '../../include/header.php'?>

<div class="content-wrapper" style="">
    <section class="content-header">
        <!--        <h1>-->
        <!--            Cabin/Bed Setup-->
        <!---->
        <!--        </h1>-->

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- Default box -->
            <div class="col-md-12">

                <div class="box box-primary">
                    <form class="form-horizontal" action="view/admin/medicines/purchase_return/store.php" method="POST" enctype="multipart/form-data">
                        <div class="box-header with-border">
                            <center>
                                <h2>Purchase Return Screen</h2>
                            </center>
                        </div>

                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-10 col-md-offset-2">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="form-box6">



                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-4 control-label">Company:</label>
                                                            <div class="col-sm-8">
                                                                <input type="" class="form-control" name="Reg_id" placeholder="Company" >
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-4 control-label">Return No:</label>
                                                            <div class="col-sm-8">
                                                                <input type="" class="form-control" name="Reg_id" placeholder="Return No" >
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-4 control-label">Bill No:</label>
                                                            <div class="col-sm-8">
                                                                <input type="" class="form-control" name="Reg_id" placeholder="Balance" >
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-4 control-label">Date:</label>
                                                            <div class="col-sm-8">
                                                                <input type="" class="form-control" name="Reg_id" placeholder="Date" >
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-4 control-label">P. Date:</label>
                                                            <div class="col-sm-8">
                                                                <input type="" class="form-control" name="Reg_id" placeholder="Balance" >
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>


                                                <div class="row">
                                                    <hr>

                                                    <div class="col-xs-2">
                                                        <label for="" class="">Code:</label>
                                                        <input class="form-control" placeholder="Code" type="text">
                                                    </div>

                                                    <div class="col-xs-3">
                                                        <label for="" class="">Description:</label>
                                                        <input class="form-control" placeholder="Description" type="text">
                                                    </div>
                                                    <div class="col-xs-3">
                                                        <label for="" class="">Quantity:</label>
                                                        <input class="form-control" placeholder="Quantity" type="text">
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <label for="" class="">Unit Price:</label>
                                                        <input class="form-control" placeholder="Price" type="text">
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <label for="" class="">Total TK:</label>
                                                        <input class="form-control" placeholder="TK" type="text">
                                                    </div>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>




                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="reset" class="btn btn-default pull-right" style="margin-left:5px; "><i class="fa fa-refresh"></i> Cancel</button>
                            <button  type="submit" class="btn btn-info pull-right"><i class="fa fa-save"></i> Save</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                    <!-- /.box-footer-->
                </div>
                <!-- /.box -->
            </div>

        </div>

    </section>
    <!-- /.content -->



    <section class="content">
        <div class="row">
            <!-- Default box -->
            <div class="col-md-12">

                <div style="position: fixed; right: 35px; top: 100px; z-index: 111">
                    <?php
                    if(isset($_SESSION['msg'])){
                        echo "<div class='alert alert-success'>".$_SESSION['msg']."</div>";
                        session_unset();
                    }
                    if(isset($_SESSION['delete'])){
                        echo "<div class='alert alert-danger'>".$_SESSION['delete']."</div>";
                        session_unset();
                    }
                    if(isset($_SESSION['update'])){
                        echo "<div class='alert alert-info'>".$_SESSION['update']."</div>";
                        session_unset();
                    }


                    ?>
                </div>

                <div class="box box-primary">

                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th  style="text-align: center">SL No</th>
                                <th  style="text-align: center">Name</th>
                                <th  style="text-align: center">Type</th>
                                <th  style="text-align: center">Rate</th>
                                <th  style="text-align: center">Mark</th>
                                <th  style="text-align: center">Action</th>
                            </tr>
                            </thead>
                            <tbody style="text-align: center">

                            <?php
                            $sl=1;
                            foreach ($beds as $bed){?>
                                <tr>
                                    <td><?php echo $sl++;?></td>
                                    <td><?php echo $bed['name'];?></td>
                                    <td><?php echo $bed['bed_type'];?></td>
                                    <td><?php echo $bed['rate'];?></td>
                                    <td><?php echo $bed['mark'];?></td>
                                    <td>
                                        <a class="btn btn-info" href="view/admin/medicines/purchase_return/edit.php?id=<?php echo $bed['id']?>">Edit</a>
                                        <a class="btn btn-danger" data-toggle="modal" data-target="#myModal" href="" data-id="<?php echo $bed['id']?>">Delete</a>
                                    </td>
                                </tr>

                            <?php }?>
                            </tbody>
                        </table>
                    </div>


                    <!-- /.box-body -->
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <form action="view/admin/medicines/purchase_return/tmp_delete.php" method="get">
                                <input id="delete" type="hidden" name="id" value="<?php ;?>">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" id="myModalLabel">Are you sure want to Delete ?</h4>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </div>
                                </div>
                            </form>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.box-footer-->
                </div>
                <!-- /.box -->
            </div>

        </div>

    </section>
</div>
<!-- /.content-wrapper -->
<?php include_once '../../include/footer.php'?>
