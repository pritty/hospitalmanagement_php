
<?php
include_once '../../../../vendor/autoload.php';


?>
<?php include_once '../../include/header.php'?>

<div class="content-wrapper" style="">
    <section class="content-header">
        <!--        <h1>-->
        <!--            Cabin/Bed Setup-->
        <!---->
        <!--        </h1>-->

    </section>

    <!-- Main content -->

    <!-- /.content -->



    <section class="content">
        <div class="row">
            <!-- Default box -->
            <div class="col-md-12">

                <div class="box box-primary">

                    <div class="box-header with-border">
                        <center>
                            <h2> Patient Outstanding List</h2>
                        </center>
                    </div>

                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th  style="text-align: center">SL No</th>
                                <th  style="text-align: center">Name</th>
                                <th  style="text-align: center">Type</th>
                                <th  style="text-align: center">Rate</th>
                                <th  style="text-align: center">Mark</th>
                                <th  style="text-align: center">Action</th>
                            </tr>
                            </thead>
                            <tbody style="text-align: center">

                            <?php
                            $sl=1;
                            foreach ($beds as $bed){?>
                                <tr>
                                    <td><?php echo $sl++;?></td>
                                    <td><?php echo $bed['name'];?></td>
                                    <td><?php echo $bed['bed_type'];?></td>
                                    <td><?php echo $bed['rate'];?></td>
                                    <td><?php echo $bed['mark'];?></td>
                                    <td>
                                        <a class="btn btn-info" href="view/admin/file/paitent_record/edit.php?id=<?php echo $bed['id']?>">Edit</a>
                                        <a class="btn btn-danger" data-toggle="modal" data-target="#myModal" href="" data-id="<?php echo $bed['id']?>">Delete</a>
                                    </td>
                                </tr>

                            <?php }?>
                            </tbody>
                        </table>
                    </div>


                    <!-- /.box-body -->
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <form action="view/admin/file/paitent_record/tmp_delete.php" method="get">
                                <input id="delete" type="hidden" name="id" value="<?php ;?>">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" id="myModalLabel">Are you sure want to Delete ?</h4>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </div>
                                </div>
                            </form>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.box-footer-->
                </div>
                <!-- /.box -->
            </div>

        </div>

    </section>
</div>
<!-- /.content-wrapper -->
<?php include_once '../../include/footer.php'?>
