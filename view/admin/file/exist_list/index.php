
<?php
include_once '../../../../vendor/autoload.php';
$exist_list = new \App\admin\File\Exist_list\Exist_list();
$exist_lists = $exist_list->index();
//var_dump($exist_lists);

?>
<?php include_once '../../include/header.php'?>

<div class="content-wrapper" style="">
    <section class="content-header">
        <h1>
            Exist List

        </h1>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- Default box -->
            <div class="col-md-12">

                <div class="box box-primary">
                    <form class="form-horizontal" action="view/admin/file/exist_list/store.php" method="POST" enctype="multipart/form-data">
                        <div class="box-header with-border">
                            <center>
                                <h2>Exist List</h2>
                            </center>
                        </div>

                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-10 col-md-offset-2">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="form-box6">


                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-4 control-label">Code</label>
                                                            <div class="col-sm-8">
                                                                <input type="" class="form-control" name="Ecode" id="Ecode" placeholder="Code" >
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-4 control-label">Name</label>
                                                            <div class="col-sm-8">
                                                                <input type="" class="form-control" name="Ename" id="Ename" placeholder="Name">
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>



                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>




                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="reset" class="btn btn-default pull-right" style="margin-left:5px; "><i class="fa fa-refresh"></i> Cancel</button>
                            <button  type="submit" class="btn btn-success pull-right"><i class="fa fa-save"></i> Save</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                    <!-- /.box-footer-->
                </div>
                <!-- /.box -->
            </div>

        </div>

    </section>
    <!-- /.content -->



    <section class="content">
        <div class="row">
            <!-- Default box -->
            <div class="col-md-12">

                <div id="fade" style="position: fixed; right: 35px; top: 100px; z-index: 111">
                    <?php
                    if(isset($_SESSION['msg'])){
                        echo "<div class='alert alert-success'>".$_SESSION['msg']."</div>";
                        //session_unset();
                        session_destroy();
                    }
                    if(isset($_SESSION['delete'])){
                        echo "<div class='alert alert-danger'>".$_SESSION['delete']."</div>";
                        //session_unset();
                        session_destroy();

                    }
                    if(isset($_SESSION['update'])){
                        echo "<div class='alert alert-info'>".$_SESSION['update']."</div>";
                        //session_unset();
                        session_destroy();

                    }


                    ?>
                </div>

                <div class="box box-primary">

                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>SL No</th>
                                <th>Ecode</th>
                                <th>Ename</th>

                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody style="">

                            <?php
                            $sl=1;
                            foreach ($exist_lists as $exist_list){?>
                                <tr>
                                    <td><?php echo $sl++?></td>
                                    <td><?php echo $exist_list['Ecode'];?></td>
                                    <td><?php echo $exist_list['Ename'];?></td>

                                    <td>
                                        <a class="btn btn-primary" href="view/admin/file/exist_list/edit.php?id=<?php echo $exist_list['id']?>"> <i class="fa fa-edit"></i> Edit</a>
                                        <a class="btn btn-danger" data-toggle="modal" data-target="#myModal" href="" data-id="<?php echo $exist_list['id']?>"> <i class="fa fa-trash"></i> Delete</a>

                                    </td>
                                </tr>

                            <?php }?>
                            </tbody>
                        </table>
                    </div>


                    <!-- /.box-body -->
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <form action="view/admin/file/exist_list/tmp_delete.php" method="get">
                                <input id="delete" type="hidden" name="id" value="<?php echo $exist_list['id'];?>">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" id="myModalLabel">Are you sure want to Delete ?</h4>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </div>
                                </div>
                            </form>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.box-footer-->
                </div>
                <!-- /.box -->
            </div>

        </div>

    </section>
</div>



<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>-->
<script src="assets/admin2/js/jquery-3.2.1.min.js"></script>
<script src="assets/admin/bower_components/jquery-ui/jquery-ui.min.js"></script>





<!-- /.content-wrapper -->
<?php include_once '../../include/footer.php'?>
