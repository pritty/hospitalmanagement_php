
<?php
include_once '../../../vendor/autoload.php';
$ipr = new App\admin\IPR\IPR();
$iprs = $ipr->index();
$exist_list = new \App\admin\File\Exist_list\Exist_list();
$exist_lists = $exist_list->index();


$p_daily_expance = new App\admin\Patient_daily_expance\Patient_daily_expance();
$p_daily_expances = $p_daily_expance->set($_POST)->store();
//var_dump($p_daily_expances);
//if(isset($_POST["btnSave"])){
//
//    exit();
//}
//var_dump($_SESSION);

//if(isset($_SESSION["regNo"])) {
//    $p_daily_expance = new App\admin\Patient_daily_expance\Patient_daily_expance();
//    $p_daily_expancess = $p_daily_expance->index($_SESSION["regNo"]);
////    $p_daily_expances = $p_daily_expance->index($_POST['reg_no']);
//    var_dump($p_daily_expancess);
////unset($_SESSION["regNo"]);
////exit();
//}

$last_id_reg = new App\admin\Patient_bill\Patient_bill();
$last_id_reg->last_id();
//var_dump($_POST['last_id_reg']);

//$p_bill = new App\admin\Patient_bill\Patient_bill();
//$p_bill->set($_POST['last_id_reg'])->store();


?>
<?php include_once '../include/header.php'?>

<div class="content-wrapper" style="">
    <section class="content-header">


    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- Default box -->
            <div class="col-md-12">

                <div class="box box-primary">
                    <form class="form-horizontal" action="view/admin/patient_daily_expance/index.php" method="POST" enctype="multipart/form-data">
                        <div class="box-header with-border">
                            <center>
                                <h2> Patient Daily Expence</h2>
                            </center>
                        </div>

                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12 ">
                                    <div class="form-box6">

                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-4 control-label">Registration No</label>
                                                        <div class="col-sm-6">

                                                            <select name="reg_no" id="fuzzOptionsList" class="form-control js-example-basic-single searchable" data-placeholder="Registration No" style="width: 85%!important;">

                                                                <option value="0">Select One</option>
                                                                <?php foreach ($iprs as $ipr){?>
                                                                    <option value="<?php echo $ipr['reg_no'];?>"> <?php echo $ipr['reg_no'];?> </option>
                                                                <?php }?>

                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-4 control-label">Bed/cabin No</label>
                                                        <div class="col-sm-6">
                                                            <input type="" class="form-control" name="bed_no" id="bed_no" placeholder="Bed/cabin No">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-3 control-label ">Name</label>
                                                    <div class="col-sm-6">
                                                        <input type="" class="form-control" name="name" id="name" placeholder="Name">
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group has-feedback">
                                                    <label for="" class="col-sm-4 control-label">Entry Date</label>
                                                    <div class="col-sm-6">
                                                        <input type="" class="form-control" name="date" id="date" placeholder="Entry Date">
                                                        <span class="glyphicon glyphicon-calendar form-control-feedback"></span>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Admitted Date</label>
                                                    <div class="col-sm-6">
                                                        <input type="" class="form-control" name="date_time" id="date_time" placeholder="Admitted Date">
                                                    </div>
                                                </div>

                                            </div>


                                        </div>

                                        <div class="row">
                                            <hr>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label ">Code</label>
                                                    <div class="col-sm-8">
                                                        <select name="Ecode" id="Ecode" class="form-control js-example-basic-single searchable" data-placeholder="Code" style="width: 85%!important;">

                                                            <option value="0">Code</option>
                                                            <?php foreach ($exist_lists as $exist_list){?>
                                                            <option value="<?php echo $exist_list['Ecode'];?>"><?php echo $exist_list['Ecode'];?></option>
                                                             <?php }?>
                                                        </select>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label ">Account Details</label>
                                                    <div class="col-sm-8">

                                                        <select name="Ename" id="Ename" class="form-control js-example-basic-single searchable" data-placeholder="Account Details">

                                                            <option value="0">Account Details</option>
                                                            <?php foreach ($exist_lists as $exist_list){?>
                                                                <option value="<?php echo $exist_list['Ecode'];?>"><?php echo $exist_list['Ename'];?></option>
                                                            <?php }?>

                                                        </select>
                                                    </div>
                                                </div>

                                            </div>

                                             <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label ">Description</label>
                                                    <div class="col-sm-8">
                                                        <input type="" class="form-control" name="description" id="description" placeholder="Description">
                                                    </div>
                                                </div>

                                            </div>

                                             <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label ">Ammount(TK)</label>
                                                    <div class="col-sm-6">
                                                        <input type="" class="form-control" name="amount" id="amount" placeholder="TK">
                                                    </div>
                                                </div>

                                            </div>


                                        </div>


                                    </div>

                                </div>

                            </div>

                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="reset" class="btn btn-default pull-right"  style="margin-left:5px; "><i class="fa fa-refresh"></i> Cancel</button>
                            <button  type="submit" class="btn btn-info pull-right" name="submit"><i class="fa fa-save"></i> Save</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                    <!-- /.box-footer-->
                </div>
                <!-- /.box -->
            </div>

        </div>

    </section>
    <!-- /.content -->



    <section class="content">
        <div class="row">
            <!-- Default box -->
            <div class="col-md-12">

                <div id="fade" style="position: fixed; right: 35px; top: 100px; z-index: 111">
                    <?php
                    if(isset($_SESSION['msg'])){
                        echo "<div class='alert alert-success'>".$_SESSION['msg']."</div>";
                        session_unset();
                    }
                    if(isset($_SESSION['delete'])){
                        echo "<div class='alert alert-danger'>".$_SESSION['delete']."</div>";
                        session_unset();
                    }
                    if(isset($_SESSION['update'])){
                        echo "<div class='alert alert-info'>".$_SESSION['update']."</div>";
                        session_unset();
                    }


                    ?>
                </div>

                <div class="box box-primary">

                    <div class="box-header with-border">

                          <h4>Registration No: <span id="reg" style="font-weight: 500; background-color: #f0f0f1"><?php echo $_SESSION['regNo'] ; unset($_SESSION["regNo"]);?></span></h4>

                    </div>
                    <div class="box-body">

                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th  style="text-align: center">Sl No</th>
                                <th  style="text-align: center">Date</th>
                                <th  style="text-align: center">Code</th>
                                <th  style="text-align: center">Name</th>
                                <th  style="text-align: center">Description</th>
                                <th  style="text-align: center">Amount</th>
                                <th  style="text-align: center">Action</th>
                            </tr>
                            </thead>
                            <tbody style="text-align: center">

                            <?php

                                $sl=1;
                                foreach ($p_daily_expances as $p_daily_expance){

                        ?>
                                <tr>
                                    <td><?php echo $sl++;?></td>
                                    <td id="date"><?php echo $p_daily_expance['date'];?></td>
                                    <td id="Ecode"><?php echo $p_daily_expance['Ecode'];?></td>
                                    <td id="Ename"><?php echo $p_daily_expance['Ename'];?></td>
                                    <td id="description"><?php echo $p_daily_expance['description'];?></td>
                                    <td id="amount"><?php echo $p_daily_expance['amount'];?></td>
                                    <td>
                                        <a class="btn btn-info" href="view/admin/patient_daily_expance/edit.php?id=<?php echo $p_daily_expance['id']?>">Edit</a>
                                        <a class="btn btn-danger" data-toggle="modal" data-target="#myModal" href="" data-id="<?php echo $p_daily_expance['id']?>">Delete</a>
                                    </td>
                                </tr>

                            <?php }?>
                            </tbody>
                        </table>
                    </div>


                    <!-- /.box-body -->
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <form action="view/admin/patient_daily_expance/tmp_delete.php" method="get">
                                <input id="delete" type="hidden" name="id" value="<?php ;?>">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" id="myModalLabel">Are you sure want to Delete ?</h4>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </div>
                                </div>
                            </form>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.box-footer-->
                </div>
                <!-- /.box -->
            </div>

        </div>

    </section>
</div>

<script src="assets/admin2/js/jquery.min.js"></script>
<script src="assets/admin/bower_components/jquery-ui/jquery-ui.min.js"></script>
<script>

    function clearForm(){
        $("#bed_no, #name, #date_time").val("");
    }
    var getValues, bed_no, patient_name, date_time, reg_no;
    function makeAjaxRequest(placeId){
        //url: "src/admin/IPR/IPR.php/json",
        $.ajax({
            type: 'POST',
            //url: "src/admin/json.php",
            url: "view/admin/patient_daily_expance/json.php",
            data: {"place_Id": placeId},
            success: function (response) {
                getValues = $.parseJSON(response)[0];
                bed_no = getValues.bed_no;
                patient_name = getValues.paitent_name;
                date_time = getValues.date_time;
                reg_no = getValues.reg_no;


                $("#bed_no").val(bed_no);
                $("#name").val(patient_name);
                $("#date_time").val(date_time);

            },
            error: function () {
                alert("Add Failed");
                return false;
            }
        });

    }

    $("#fuzzOptionsList").on("change", function(){
        var id = $("#fuzzOptionsList option:selected").val();
        if (id == "0"){
            clearForm();
        } else {
            makeAjaxRequest(id);
        }
    });
</script>


<script>
    $("#fuzzOptionsList").on("change", function(){
        document.getElementById("reg").innerHTML = $("#fuzzOptionsList option:selected").val();
        if (id == "0"){
            clearForm();
        }
    });
</script>

<script>

    function clearForm(){
        $("#Ecode, #Ename").val("");
    }
    var getValues, Ecode, Ename;
    function makeAjaxRequest2(placeId){
        $("#Ename").val(placeId);
        //url: "src/admin/IPR/IPR.php/json",
//        var td = "";
//        $.ajax({
//            type: 'POST',
//            url: "view/admin/file/exist_list/json_exist_list.php",
//            data: {"place_Id": placeId},
//            success: function (response) {
//                getValues = $.parseJSON(response);
//                $.each(getValues,function (key,value) {
//                    td += "<option value='"+ value.Ecode +"' selected>"+ value.Ename +"</option>";
//                });
//                $("#Ename").empty();
//                $("#Ename").append(td);
//            },
//            error: function () {
//                alert("Add Failed");
//                return false;
//            }
//        });

    }
    function makeAjaxRequest3(placeId){
        $("#Ecode").val(placeId);
    }

    $("#Ecode").on("change", function(){
        var id = $("#Ecode option:selected").val();
        if (id == "0"){
            clearForm();
        } else {
            makeAjaxRequest2(id);
        }
    });
    $("#Ename").on("change", function(){
        var id = $("#Ename option:selected").val();
        if (id == "0"){
            clearForm();
        } else {
            makeAjaxRequest3(id);
        }
    });
</script>

<script>
    $(document).ready(function () {

        $("#fuzzOptionsList").on("change", function(){
            var id = $('#fuzzOptionsList').val();
            if(id != ''){
                $.ajax({
                    url:"view/admin/patient_daily_expance/fetch.php",
                    type: 'POST',
                    data:{id:id},
                    dataType : "JSON",
                    success: function (data) {
//                        getValues = $.parseJSON(response)[0];
//                        date = getValues.date;
//                        Ecode = getValues.Ecode;
//                        Ename = getValues.Ename;
//                        description = getValues.description;
//                        amount = getValues.amount;
//                        reg_no = getValues.reg_no;



                        $("#date").text(data.date);

                        $("#example1 tbody").empty();

                        for(var i =0; i<data.length; i++){
                            $("#example1 tbody").append('<tr>' +
                                '<td  style="text-align: center">'+ (i+1) +'</td>'+
                                '<td  style="text-align: center">'+ data[i].date+'</td>'+
                                '<td  style="text-align: center">'+ data[i].Ecode+'</td>'+
                                '<td  style="text-align: center">'+ data[i].Ename+'</td>'+
                                '<td  style="text-align: center">'+ data[i].description+'</td>'+
                                '<td  style="text-align: center">'+ data[i].amount+'</td>'+
                                '<td>' +
                                    '<a class="btn btn-info" href="view/admin/patient_daily_expance/edit.php?id='+data[i].Ecode+'">Edit</a>'+
                                    '<a class="btn btn-danger" data-toggle="modal" data-target="#myModal" href="" data-id="'+data[i].Ecode+'">Delete</a>' +
                                '</td>'+
                            '</tr>');
                        }

                    },
                    error: function () {
                        alert("Add Failed");
                        return false;
                    }
                })
            }

            else{
                clearForm();
            }
        });
    });
</script>

<!--<script>-->
<!--    $(document).ready(function () {-->
<!---->
<!--        $("#fuzzOptionsList").on("change", function(){-->
<!--            var id = $('#fuzzOptionsList').val();-->
<!--            if(id != ''){-->
<!--                $.ajax({-->
<!--                    url:"view/admin/patient_daily_expance/p_bill_store.php",-->
<!--                    type: 'POST',-->
<!--                    data:{id:id},-->
<!--                    dataType : "JSON",-->
<!--                    success: function (data) {-->
<!---->
<!---->
<!--//                        $("#date").text(data.date);-->
<!---->
<!---->
<!--                        for(var i =0; i<data.length; i++){-->
<!---->
<!--                        }-->
<!---->
<!---->
<!--                    },-->
<!--                    error: function () {-->
<!--                        alert("Add Failed");-->
<!--                        return false;-->
<!--                    }-->
<!--                })-->
<!--            }-->
<!---->
<!--            else{-->
<!--                clearForm();-->
<!--            }-->
<!--        });-->
<!--    });-->
<!--</script>-->




<script>
    $("#date").datepicker(

        {
            dateFormat: "mm/dd/yy",
            showOtherMonths: true,
            selectOtherMonths: true,
            autoclose: true,
            changeMonth: true,
            changeYear: true
            //gotoCurrent: true
        }).datepicker("setDate", "0");

</script>
<!-- /.content-wrapper -->
<?php include_once '../include/footer.php'?>
