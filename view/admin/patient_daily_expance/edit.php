
<?php
include_once '../../../vendor/autoload.php';
$ipr = new App\admin\IPR\IPR();
$iprs = $ipr->index();
$exist_list = new \App\admin\File\Exist_list\Exist_list();
$exist_lists = $exist_list->index();


$p_daily_expance = new App\admin\Patient_daily_expance\Patient_daily_expance();
$p_daily_expances = $p_daily_expance->set($_POST)->store();

$last_id_reg = new App\admin\Patient_bill\Patient_bill();
$last_id_reg->last_id();


?>
<?php include_once '../include/header.php'?>

<div class="content-wrapper" style="">
    <section class="content-header">


    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- Default box -->
            <div class="col-md-12">

                <div class="box box-primary">
                    <form class="form-horizontal" action="view/admin/patient_daily_expance/index.php" method="POST" enctype="multipart/form-data">
                        <div class="box-header with-border">
                            <center>
                                <h2> Patient Daily Expence</h2>
                            </center>
                        </div>

                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12 ">
                                    <div class="form-box6">

                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-4 control-label">Registration No</label>
                                                        <div class="col-sm-6">

                                                            <select name="reg_no" id="fuzzOptionsList" class="form-control js-example-basic-single searchable" data-placeholder="Registration No" style="width: 85%!important;">

                                                                <option value="0">Select One</option>
                                                                <?php foreach ($iprs as $ipr){?>
                                                                    <option value="<?php echo $ipr['reg_no'];?>"> <?php echo $ipr['reg_no'];?> </option>
                                                                <?php }?>

                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-4 control-label">Bed/cabin No</label>
                                                        <div class="col-sm-6">
                                                            <input type="" class="form-control" name="bed_no" id="bed_no" placeholder="Bed/cabin No">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-3 control-label ">Name</label>
                                                    <div class="col-sm-6">
                                                        <input type="" class="form-control" name="name" id="name" placeholder="Name">
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group has-feedback">
                                                    <label for="" class="col-sm-4 control-label">Entry Date</label>
                                                    <div class="col-sm-6">
                                                        <input type="" class="form-control" name="date" id="date" placeholder="Entry Date">
                                                        <span class="glyphicon glyphicon-calendar form-control-feedback"></span>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Admitted Date</label>
                                                    <div class="col-sm-6">
                                                        <input type="" class="form-control" name="date_time" id="date_time" placeholder="Admitted Date">
                                                    </div>
                                                </div>

                                            </div>


                                        </div>

                                        <div class="row">
                                            <hr>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label ">Code</label>
                                                    <div class="col-sm-8">
                                                        <select name="Ecode" id="Ecode" class="form-control js-example-basic-single searchable" data-placeholder="Code" style="width: 85%!important;">

                                                            <option value="0">Code</option>
                                                            <?php foreach ($exist_lists as $exist_list){?>
                                                                <option value="<?php echo $exist_list['Ecode'];?>"><?php echo $exist_list['Ecode'];?></option>
                                                            <?php }?>
                                                        </select>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label ">Account Details</label>
                                                    <div class="col-sm-8">

                                                        <select name="Ename" id="Ename" class="form-control js-example-basic-single searchable" data-placeholder="Account Details">

                                                            <option value="0">Account Details</option>
                                                            <?php foreach ($exist_lists as $exist_list){?>
                                                                <option value="<?php echo $exist_list['Ecode'];?>"><?php echo $exist_list['Ename'];?></option>
                                                            <?php }?>

                                                        </select>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label ">Description</label>
                                                    <div class="col-sm-8">
                                                        <input type="" class="form-control" name="description" id="description" placeholder="Description">
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label ">Ammount(TK)</label>
                                                    <div class="col-sm-6">
                                                        <input type="" class="form-control" name="amount" id="amount" placeholder="TK">
                                                    </div>
                                                </div>

                                            </div>


                                        </div>


                                    </div>

                                </div>

                            </div>

                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="reset" class="btn btn-default pull-right"  style="margin-left:5px; "><i class="fa fa-refresh"></i> Cancel</button>
                            <button  type="submit" class="btn btn-info pull-right" name="submit"><i class="fa fa-save"></i> Save</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                    <!-- /.box-footer-->
                </div>
                <!-- /.box -->
            </div>

        </div>

    </section>
    <!-- /.content -->



    <section class="content">
        <div class="row">
            <!-- Default box -->
            <div class="col-md-12">

                <div id="fade" style="position: fixed; right: 35px; top: 100px; z-index: 111">
                    <?php
                    if(isset($_SESSION['msg'])){
                        echo "<div class='alert alert-success'>".$_SESSION['msg']."</div>";
                        session_unset();
                    }
                    if(isset($_SESSION['delete'])){
                        echo "<div class='alert alert-danger'>".$_SESSION['delete']."</div>";
                        session_unset();
                    }
                    if(isset($_SESSION['update'])){
                        echo "<div class='alert alert-info'>".$_SESSION['update']."</div>";
                        session_unset();
                    }


                    ?>
                </div>


                <!-- /.box -->
            </div>

        </div>

    </section>
</div>

<script src="assets/admin2/js/jquery.min.js"></script>
<script src="assets/admin/bower_components/jquery-ui/jquery-ui.min.js"></script>
<script>

    function clearForm(){
        $("#bed_no, #name, #date_time").val("");
    }
    var getValues, bed_no, patient_name, date_time, reg_no;
    function makeAjaxRequest(placeId){
        //url: "src/admin/IPR/IPR.php/json",
        $.ajax({
            type: 'POST',
            //url: "src/admin/json.php",
            url: "view/admin/patient_daily_expance/json.php",
            data: {"place_Id": placeId},
            success: function (response) {
                getValues = $.parseJSON(response)[0];
                bed_no = getValues.bed_no;
                patient_name = getValues.paitent_name;
                date_time = getValues.date_time;
                reg_no = getValues.reg_no;


                $("#bed_no").val(bed_no);
                $("#name").val(patient_name);
                $("#date_time").val(date_time);

            },
            error: function () {
                alert("Add Failed");
                return false;
            }
        });

    }

    $("#fuzzOptionsList").on("change", function(){
        var id = $("#fuzzOptionsList option:selected").val();
        if (id == "0"){
            clearForm();
        } else {
            makeAjaxRequest(id);
        }
    });
</script>


<script>
    $("#fuzzOptionsList").on("change", function(){
        document.getElementById("reg").innerHTML = $("#fuzzOptionsList option:selected").val();
        if (id == "0"){
            clearForm();
        }
    });
</script>

<script>

    function clearForm(){
        $("#Ecode, #Ename").val("");
    }
    var getValues, Ecode, Ename;
    function makeAjaxRequest2(placeId){
        $("#Ename").val(placeId);

    }
    function makeAjaxRequest3(placeId){
        $("#Ecode").val(placeId);
    }

    $("#Ecode").on("change", function(){
        var id = $("#Ecode option:selected").val();
        if (id == "0"){
            clearForm();
        } else {
            makeAjaxRequest2(id);
        }
    });
    $("#Ename").on("change", function(){
        var id = $("#Ename option:selected").val();
        if (id == "0"){
            clearForm();
        } else {
            makeAjaxRequest3(id);
        }
    });
</script>





<script>
    $("#date").datepicker(

        {
            dateFormat: "mm/dd/yy",
            showOtherMonths: true,
            selectOtherMonths: true,
            autoclose: true,
            changeMonth: true,
            changeYear: true
            //gotoCurrent: true
        }).datepicker("setDate", "0");

</script>
<!-- /.content-wrapper -->
<?php include_once '../include/footer.php'?>
