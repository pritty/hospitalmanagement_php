
<?php
include_once '../../../vendor/autoload.php';
$ipr = new \App\admin\IPR\IPR();
$iprs = $ipr->index();
//var_dump($iprs);
?>
<?php include_once '../include/header.php'?>

<div class="content-wrapper" style="">
    <section class="content-header">
        <h1>
            Paitent Admission Ticket

        </h1>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- Default box -->
            <div class="col-md-12">

                <div class="box box-primary">
                    <form class="form-horizontal" action="view/admin/patient_admission_ticket/store.php" method="POST" enctype="multipart/form-data">
                        <div class="box-header with-border">

                        </div>

                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-8 col-md-offset-2">
                                    <div class="row">
                                        <div class="col-md-10">
                                          <div class="form-box">

                                              <div class="form-group">
                                                  <label for="" class="col-sm-3 control-label">Registration ID</label>
                                                  <div class="col-sm-8">
                                                      <select name="reg_no" id="reg_no" class="form-control js-example-basic-single searchable" data-placeholder="Registration ID">

                                                          <option value="0">Registration ID</option>
                                                          <?php foreach ($iprs as $ipr){?>
                                                              <option value="<?php echo $ipr['reg_no'];?>"> <?php echo $ipr['reg_no'];?> </option>
                                                          <?php }?>

                                                      </select>
                                                  </div>
                                              </div>
                                              <div class="form-group">
                                                  <label for="" class="col-sm-3 control-label">Patient Name</label>
                                                  <div class="col-sm-8">

                                                      <select name="patient_name" id="patient_name" class="form-control js-example-basic-single searchable" data-placeholder="Patient Name">

                                                          <option value="0">Patient Name</option>
                                                          <?php foreach ($iprs as $ipr){?>
                                                              <option value="<?php echo $ipr['paitent_name'];?>"> <?php echo $ipr['paitent_name'];?> </option>
                                                          <?php }?>

                                                      </select>
                                                  </div>
                                              </div>

                                          </div>

                                        </div>

                                    </div>

                                </div>

                            </div>




                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="reset" class="btn btn-default pull-right" style="margin-left:5px; "><i class="fa fa-refresh"></i> Cancel</button>
                            <button  type="submit" class="btn btn-info pull-right"><i class="fa fa-save"></i> Save</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                    <!-- /.box-footer-->
                </div>
                <!-- /.box -->
            </div>

        </div>

    </section>
    <!-- /.content -->



    <section class="content">
        <div class="row">
            <!-- Default box -->
            <div class="col-md-12">

                <div style="position: fixed; right: 35px; top: 100px; z-index: 111">
                    <?php
                    if(isset($_SESSION['msg'])){
                        echo "<div class='alert alert-success'>".$_SESSION['msg']."</div>";
                        session_unset();
                    }
                    if(isset($_SESSION['delete'])){
                        echo "<div class='alert alert-danger'>".$_SESSION['delete']."</div>";
                        session_unset();
                    }
                    if(isset($_SESSION['update'])){
                        echo "<div class='alert alert-info'>".$_SESSION['update']."</div>";
                        session_unset();
                    }


                    ?>
                </div>

                <div class="box box-primary">

                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>SL No</th>
                                <th>Reg No</th>
                                <th>Paitent Name</th>
                                <th>Department</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody style="">

                            <?php
                            $sl=1;
                            foreach ($iprs as $ipr){?>
                                <tr>
                                    <td><?php echo $sl++?></td>
                                    <td><?php echo $ipr['reg_no'];?></td>
                                    <td><?php echo $ipr['date_time'];?></td>
                                    <td><?php echo $ipr['consultant'];?></td>

                                    <td>
                                        <a class="btn btn-primary" href="view/admin/patient_admission_ticket/edit.php?id=<?php echo $ipr['id']?>"> <i class="fa fa-edit"></i> Edit</a>
                                        <a class="btn btn-danger" data-toggle="modal" data-target="#myModal" href="" data-id="<?php echo $ipr['id']?>"> <i class="fa fa-trash"></i> Delete</a>
                                    </td>
                                </tr>

                            <?php }?>
                            </tbody>
                        </table>
                    </div>


                    <!-- /.box-body -->
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <form action="view/admin/patient_admission_ticket/tmp_delete.php" method="get">
                                <input id="delete" type="hidden" name="id" value="<?php echo $ipr['id'];?>">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" id="myModalLabel">Are you sure want to Delete ?</h4>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </div>
                                </div>
                            </form>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.box-footer-->
                </div>
                <!-- /.box -->
            </div>

        </div>

    </section>
</div>




<script src="assets/admin2/js/jquery.min.js"></script>
<script>
    function clearForm(){
        $("#patient_name, #reg_no").val("");
    }
    var getValues, patient_name, reg_no;
    function makeAjaxRequest(placeId) {
        $.ajax({
            type: 'POST',
            url: "view/admin/patient_admission_ticket/json.php",
            data: {"place_Id": placeId},
            success: function (response) {
                getValues = $.parseJSON(response)[0];
                patient_name = getValues.paitent_name;
                reg_no = getValues.reg_no;


                $("#patient_name").val(patient_name);
                $("#reg_no").val(reg_no);

            },
            error: function () {
                alert("Add Failed");
                return false;
            }
        });
    }
    




   $("#reg_no").on("change", function () {
       var id = $("#reg_no option:selected").val();
       if (id == "0"){
           clearForm();
       }else{
           makeAjaxRequest(id);
       }
   }) ;

    $("#patient_name").on("change", function () {
       var id = $("#patient_name option:selected").val();
       if (id == "0"){
           clearForm();
       }else{
           makeAjaxRequest(id);
       }
   }) ;
</script>




<!-- /.content-wrapper -->
<?php include_once '../include/footer.php'?>
