
<?php
include_once '../../../vendor/autoload.php';
$ipr = new \App\admin\IPR\IPR();
$iprs = $ipr->index();
//var_dump($iprs);
?>
<?php include_once '../include/header.php'?>

<div class="content-wrapper" style="">
    <section class="content-header">
        <!--        <h1>-->
        <!---->
        <!--        </h1>-->

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- Default box -->
            <div class="col-md-12">

                <div class="box box-primary">
                    <form class="form-horizontal" action="view/admin/patient_discharge_note_treatment/store.php" method="POST" enctype="multipart/form-data">
                        <div class="box-header with-border">
                            <center>
                                <h2>Consultancy</h2>
                            </center>
                        </div>

                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-10 col-md-offset-2">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="form-box6">

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-3 control-label">Invoice ID</label>
                                                            <div class="col-sm-8">
                                                                <input type="" class="form-control" name="invc_id" placeholder="Invoice ID" autofocus>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-4 control-label">Date</label>
                                                            <div class="col-sm-8">
                                                                <input type="" class="form-control" name="con_date" placeholder="Date" autofocus>
                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>

                                                <div class="row">
                                                    <hr>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-4 control-label">Reg. No</label>
                                                            <div class="col-sm-8">
                                                                <select name="reg_no" id="fuzzOptionsList" class="form-control js-example-basic-single searchable" data-placeholder="Registration No">

                                                                    <option value="0">Select One</option>
                                                                    <?php foreach ($iprs as $ipr){?>
                                                                        <option value="<?php echo $ipr['reg_no'];?>"> <?php echo $ipr['reg_no'];?> </option>
                                                                    <?php }?>

                                                                </select>
                                                            </div>
                                                        </div>


                                                    </div>
                                                    <div class="col-md-6">

                                                        <div class="form-group">
                                                            <label for="" class="col-sm-4 control-label">Name</label>
                                                            <div class="col-sm-8">
                                                                <input type="" class="form-control" name="name" id="name" placeholder="Name">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">

                                                        <div class="form-group">
                                                            <label for="" class="col-sm-4 control-label">Cabin No</label>
                                                            <div class="col-sm-8">
                                                                <input type="" class="form-control" name="bed_no" id="bed_no" placeholder="Bed/cabin No">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>




                                                <div class="row">
                                                    <hr>

                                                    <div class="col-md-6">

                                                        <div class="form-group">
                                                            <label for="" class="col-sm-4 control-label">C.Code</label>
                                                            <div class="col-sm-8">
                                                                <input type="" class="form-control" name="doc_id" id="doc_id" placeholder="C.Code" autofocus>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-4 control-label">C.Name</label>
                                                            <div class="col-sm-8">
                                                                <input type="" class="form-control" name="doc_name" id="doc_name" placeholder="C.Name" autofocus>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">

                                                        <div class="form-group">
                                                            <label for="" class="col-sm-4 control-label">Description</label>
                                                            <div class="col-sm-8">
                                                                <input type="" class="form-control" name="con_for" id="con_for" placeholder="Description" autofocus>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-4 control-label">Amount</label>
                                                            <div class="col-sm-8">
                                                                <input type="" class="form-control" name="con_amount" id="con_amount" placeholder="Amount" autofocus>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>



                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>




                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="reset" class="btn btn-default pull-right" style="margin-left:5px; "><i class="fa fa-refresh"></i> Cancel</button>
                            <button  type="submit" class="btn btn-info pull-right"><i class="fa fa-save"></i> Save</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                    <!-- /.box-footer-->
                </div>
                <!-- /.box -->
            </div>

        </div>

    </section>
    <!-- /.content -->



    <section class="content">
        <div class="row">
            <!-- Default box -->
            <div class="col-md-12">

                <div style="position: fixed; right: 35px; top: 100px; z-index: 111">
                    <?php
                    if(isset($_SESSION['msg'])){
                        echo "<div class='alert alert-success'>".$_SESSION['msg']."</div>";
                        session_unset();
                    }
                    if(isset($_SESSION['delete'])){
                        echo "<div class='alert alert-danger'>".$_SESSION['delete']."</div>";
                        session_unset();
                    }
                    if(isset($_SESSION['update'])){
                        echo "<div class='alert alert-info'>".$_SESSION['update']."</div>";
                        session_unset();
                    }


                    ?>
                </div>

                <div class="box box-primary">

                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>SL No</th>
                                <th>Reg No</th>
                                <th>Paitent Name</th>
                                <th>Department</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody style="">

                            <?php
                            $sl=1;
                            foreach ($iprs as $ipr){?>
                                <tr>
                                    <td><?php echo $sl++?></td>
                                    <td><?php echo $ipr['reg_no'];?></td>
                                    <td><?php echo $ipr['date_time'];?></td>
                                    <td><?php echo $ipr['consultant'];?></td>

                                    <td>
                                        <a class="btn btn-primary" href="view/admin/patient_discharge_note_treatment/edit.php?id=<?php echo $ipr['id']?>"> <i class="fa fa-edit"></i> Edit</a>
                                        <a class="btn btn-danger" data-toggle="modal" data-target="#myModal" href="" data-id="<?php echo $ipr['id']?>"> <i class="fa fa-trash"></i> Delete</a>
                                    </td>
                                </tr>

                            <?php }?>
                            </tbody>
                        </table>
                    </div>


                    <!-- /.box-body -->
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <form action="view/admin/patient_discharge_note_treatment/tmp_delete.php" method="get">
                                <input id="delete" type="hidden" name="id" value="<?php echo $ipr['id'];?>">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" id="myModalLabel">Are you sure want to Delete ?</h4>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </div>
                                </div>
                            </form>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.box-footer-->
                </div>
                <!-- /.box -->
            </div>

        </div>

    </section>
</div>

<script src="assets/admin2/js/jquery.min.js"></script>
<script src="assets/admin/bower_components/jquery-ui/jquery-ui.min.js"></script>
<script>

    function clearForm(){
        $("#bed_no, #name, #date_time").val("");
    }
    var getValues, bed_no, patient_name, date_time, reg_no;
    function makeAjaxRequest(placeId){
        //url: "src/admin/IPR/IPR.php/json",
        $.ajax({
            type: 'POST',
            url: "src/admin/json.php",
            data: {"place_Id": placeId},
            success: function (response) {
                getValues = $.parseJSON(response)[0];
                bed_no = getValues.bed_no;
                patient_name = getValues.paitent_name;
                date_time = getValues.date_time;
                reg_no = getValues.reg_no;


                $("#bed_no").val(bed_no);
                $("#name").val(patient_name);
                $("#date_time").val(date_time);

            },
            error: function () {
                alert("Add Failed");
                return false;
            }
        });

    }

    $("#fuzzOptionsList").on("change", function(){
        var id = $("#fuzzOptionsList option:selected").val();
        if (id == "0"){
            clearForm();
        } else {
            makeAjaxRequest(id);
        }
    });
</script>

<script>

    function clearForm(){
        $("#Ecode, #Ename").val("");
    }
    var getValues, Ecode, Ename;
    function makeAjaxRequest2(placeId){
        $("#Ename").val(placeId);
        //url: "src/admin/IPR/IPR.php/json",
//        var td = "";
//        $.ajax({
//            type: 'POST',
//            url: "view/admin/file/exist_list/json_exist_list.php",
//            data: {"place_Id": placeId},
//            success: function (response) {
//                getValues = $.parseJSON(response);
//                $.each(getValues,function (key,value) {
//                    td += "<option value='"+ value.Ecode +"' selected>"+ value.Ename +"</option>";
//                });
//                $("#Ename").empty();
//                $("#Ename").append(td);
//            },
//            error: function () {
//                alert("Add Failed");
//                return false;
//            }
//        });

    }
    function makeAjaxRequest3(placeId){
        $("#Ecode").val(placeId);
    }

    $("#Ecode").on("change", function(){
        var id = $("#Ecode option:selected").val();
        if (id == "0"){
            clearForm();
        } else {
            makeAjaxRequest2(id);
        }
    });
    $("#Ename").on("change", function(){
        var id = $("#Ename option:selected").val();
        if (id == "0"){
            clearForm();
        } else {
            makeAjaxRequest3(id);
        }
    });
</script>
<script>
    $("#date").datepicker(

        {
            dateFormat: "mm/dd/yy",
            showOtherMonths: true,
            selectOtherMonths: true,
            autoclose: true,
            changeMonth: true,
            changeYear: true
            //gotoCurrent: true
        }).datepicker("setDate", "0");

</script>

<!-- /.content-wrapper -->
<?php include_once '../include/footer.php'?>
