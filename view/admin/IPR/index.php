
<?php
include_once '../../../vendor/autoload.php';
$bed = new \App\admin\Bedcabin\Bedcabin();
$beds = $bed->index();
$corporate = new \App\admin\Corporate\Corporate();
$corporates = $corporate->index();
//var_dump($corporates);

$dept = new \App\admin\Department\Department();
$depts = $dept->index();
//var_dump($depts);
$doctor = new \App\admin\Doctor\Doctor();
$doctors = $doctor->index();
//var_dump($doctors);

$ipr = new \App\admin\IPR\IPR();
$iprs = $ipr->index();
//var_dump($iprs);
?>
<?php include_once '../include/header.php'?>

<div class="content-wrapper" style="">
    <section class="content-header">
        <h1>
            Paitent Registration

        </h1>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
        <!-- Default box -->
            <div class="col-md-12">

        <div class="box box-primary">
            <form class="form-horizontal" action="view/admin/IPR/store.php" method="POST" enctype="multipart/form-data">
            <div class="box-header with-border">

                <div class="col-md-6" style="padding: 24px">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label">Client Type</label>
                        <div class="col-sm-8">
                            <select name="client_type" id="client_type" class="form-control" >
                                <option value="0">Select One</option>
                                <option value="General Room">General Room</option>
                                <option value="Corporate">Corporate</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-4 control-label">Company Name</label>
                        <div class="col-sm-8">
                            <select name="company_name" id="company_name" class="form-control" >
                                <option value="0">Select One</option>
                                <?php foreach ($corporates as $corporate){?>
                                    <option value="<?php echo $corporate['name'];?>"> <?php echo $corporate['name'];?> </option>
                                <?php }?>
                            </select>
                        </div>
                    </div>

                </div>

                <div class="col-md-6" style="padding:24px;">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Reg No</label>
                                <div class="col-sm-8">
                                    <input type="" class="form-control" name="reg_no" placeholder="Reg No">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Bed No</label>
                                <div class="col-sm-8">
                                    <input type="" class="form-control" name="bed_no" placeholder="Bed No">
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group  has-feedback">
                                <label for="" class="col-sm-4 control-label">Date</label>
                                <div class="col-sm-8">
                                    <input type="" class="form-control" name="date_time" id="date_time" placeholder="Date">
                                    <span class="glyphicon glyphicon-calendar form-control-feedback"></span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group has-feedback input-group bootstrap-timepicker timepicker">
                                <label for="" class="col-sm-4 control-label">Time</label>
                                <div class="col-sm-8">
                                    <input type="" class="form-control" name="time" id="time" data-provide="timepicker" data-template="dropdown" data-minute-step="1" placeholder="Time">
                                    <span class="glyphicon glyphicon-time form-control-feedback"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>



            <div class="box-body">
                <div class="row">
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 ">Name</label>
                                    <div class="col-sm-8">
                                        <input type="" class="form-control" name="paitent_name" id="paitent_name" placeholder="Name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 ">Father/Husband Name</label>
                                    <div class="col-sm-8">
                                        <input type="" class="form-control" name="f_or_h_name" placeholder="Father/Husband Name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 ">Guardian Name</label>
                                    <div class="col-sm-8">
                                        <input type="" class="form-control" name="guardian_name" placeholder="Guardian Name">
                                    </div>
                                </div>

                            </div>


                            <div class="col-md-4">

                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label ">Gender</label>
                                    <div class="col-sm-8">
                                        <div class="radio">
                                            <label>
                                                <input name="gender" id="optionsRadios1" value="Male"  type="radio">
                                                Male
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label>
                                                <input name="gender" id="optionsRadios2" value="Female" type="radio">
                                                Female
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label>
                                                <input name="gender" id="optionsRadios3" value="Other"  type="radio">
                                                Other
                                            </label>
                                        </div>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Age</label>
                                    <div class="col-sm-8">
                                        <input type="" class="form-control" name="age" placeholder="Years">
                                    </div>
                                </div>

                            </div>

                        </div>


                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 ">Address Details</label>
                                    <div class="col-sm-8">
                                        <input type="" class="form-control" name="address" placeholder="Address">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Religion</label>
                                    <div class="col-sm-8">
                                        <select name="religion" id="" class="form-control">
                                            <option value="Islam">Islam</option>
                                            <option value="Sanatan">Sanatan</option>
                                            <option value="Christian">Christian</option>
                                            <option value="Buddah">Buddah</option>
                                            <option value="Others">Others</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">Contact No</label>
                                    <div class="col-sm-8">
                                        <input type="" class="form-control" name="contact_no" placeholder="Contact No">
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>


                    <div class="col-md-4">

                        <div class="form-group">
                            <div class="col-sm-6">
                                <label>Height</label>
                                <input type="" class="form-control" name="height" placeholder="Height">
                            </div>
                            <div class="col-sm-6">
                                <label>Weight</label>
                                <input type="" class="form-control" name="weight" placeholder="Weight">
                            </div>
                        </div>
                        <div style="border: 1px solid #f0f0f1; box-sizing: border-box">
                            <h4>Blood Pressure:</h4>
                        <div class="form-group">
                            <div class="col-sm-6">
                                <label>SBP</label>
                                <input type="" class="form-control" name="sbp" placeholder="SBP">
                            </div>
                            <div class="col-sm-6">
                                <label>DBP</label>
                                <input type="" class="form-control" name="dbp" placeholder="DBP">
                            </div>
                        </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <center><hr></center>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">Department</label>
                            <div class="col-sm-8">
                                <select name="department" id="" class="form-control" required>
                                    <option>Select One</option>
                                    <?php foreach ($depts as $dept){?>
                                        <option value="<?php echo $dept['name'];?>"> <?php echo $dept['name'];?> </option>
                                    <?php }?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">Diagnosis</label>
                            <div class="col-sm-8">
                                <input type=""  name="diagnosis" class="form-control" id="inputEmail3" placeholder="Diagnosis">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">Addmission Doctor</label>
                            <div class="col-sm-8">
                                <select name="admission_doc" id="" class="form-control" required>
                                    <option>Select One</option>
                                    <?php foreach ($doctors as $doctor){?>
                                        <option value="<?php echo $doctor['name'];?>"> <?php echo $doctor['name'];?> </option>
                                    <?php }?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">Referred By</label>
                            <div class="col-sm-8">
                                <select name="reffered_by" id="" class="form-control" required>
                                    <option>Select One</option>
                                    <?php foreach ($doctors as $doctor){?>
                                        <option value="<?php echo $doctor['name'];?>"> <?php echo $doctor['name'];?> </option>
                                    <?php }?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">Consultant Name</label>
                            <div class="col-sm-8">
                                <select name="consultant" id="" class="form-control" required>
                                    <option>Select One</option>
                                    <?php foreach ($doctors as $doctor){?>
                                        <option value="<?php echo $doctor['name'];?>"> <?php echo $doctor['name'];?> </option>
                                    <?php }?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">Occupation</label>
                            <div class="col-sm-8">
                                <input type="" class="form-control" name="occupation" placeholder="Client Type">
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <button type="reset" class="btn btn-default pull-right" style="margin-left:5px; ">Cancel</button>
                <button  type="submit" class="btn btn-info pull-right">Save</button>
            </div>
            <!-- /.box-footer -->
            </form>
            <!-- /.box-footer-->
        </div>
        <!-- /.box -->
            </div>

        </div>

    </section>
    <!-- /.content -->



    <section class="content">
        <div class="row">
            <!-- Default box -->
            <div class="col-md-12">

                <div style="position: fixed; right: 35px; top: 100px; z-index: 111">
                    <?php
                    if(isset($_SESSION['msg'])){
                        echo "<div class='alert alert-success'>".$_SESSION['msg']."</div>";
                        session_unset();
                    }
                    if(isset($_SESSION['delete'])){
                        echo "<div class='alert alert-danger'>".$_SESSION['delete']."</div>";
                        session_unset();
                    }
                    if(isset($_SESSION['update'])){
                        echo "<div class='alert alert-info'>".$_SESSION['update']."</div>";
                        session_unset();
                    }


                    ?>
                </div>

                <div class="box box-primary">

                                <div class="box-body">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>SL No</th>
                                            <th>Reg No</th>
                                            <th>Paitent Name</th>
                                            <th>Client Type</th>
                                            <th>Company Name</th>
                                            <th>Age</th>
                                            <th>Gender</th>
                                            <th>Department</th>
                                            <th>Bed No</th>
                                            <th>Contact No</th>
                                            <th>Admit Date</th>
                                            <th>Consultant Name</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody style="text-align: center">

                                        <?php
                                        $sl=1;
                                        foreach ($iprs as $ipr){?>
                                            <tr>
                                                <td><?php echo $sl++?></td>
                                                <td><?php echo $ipr['reg_no'];?></td>
                                                <td><?php echo $ipr['paitent_name'];?></td>
                                                <td><?php echo $ipr['client_type'];?></td>
                                                <td><?php echo $ipr['company_name'];?></td>
                                                <td><?php echo $ipr['age'];?></td>
                                                <td><?php echo $ipr['gender'];?></td>
                                                <td><?php echo $ipr['department'];?></td>
                                                <td><?php echo $ipr['bed_no'];?></td>
                                                <td><?php echo $ipr['contact_no'];?></td>
                                                <td><?php echo $ipr['date_time'];?></td>
                                                <td><?php echo $ipr['consultant'];?></td>

                                                <td>
                                                    <a class="txt text-primary" href="view/admin/IPR/edit.php?id=<?php echo $ipr['id']?>"> <i class="fa fa-edit"></i> Edit</a><br>
<!--                                                    <a class="txt text-danger" href="view/admin/IPR/view.php?id=--><?php //echo $ipr['id']?><!--"> <i class="fa fa-trash"></i>Delete</a>-->
                                                    <a class="txt text-danger" data-toggle="modal" data-target="#myModal" href="" data-id="<?php echo $ipr['id']?>"> <i class="fa fa-trash"></i> Delete</a>
                                                </td>
                                            </tr>

                                        <?php }?>
                                        </tbody>
                                    </table>
                                </div>


                        <!-- /.box-body -->
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <form action="view/admin/IPR/tmp_delete.php" method="get">
                                <input id="delete" type="hidden" name="id" value="<?php echo $ipr['id'];?>">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" id="myModalLabel">Are you sure want to Delete ?</h4>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </div>
                                </div>
                            </form>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.box-footer-->
                </div>
                <!-- /.box -->
            </div>

        </div>

    </section>
</div>
<!-- /.content-wrapper -->

<!--<script src="assets/admin2/js/jquery.min.js"></script>-->
<script src="assets/admin2/js/jquery-3.2.1.min.js"></script>
<script src="assets/admin/bower_components/jquery-ui/jquery-ui.min.js"></script>
<script>
    $("#client_type").on("change" ,function() {
        var id = $("#client_type option:selected").val();
        if(id == "General Room"){
            $("#company_name").prop('disabled', true);

        } else {
            $("#company_name").prop('disabled', false);

        }
    });
</script>






<script>
    $("#date_time").datepicker(

        {
            dateFormat: "mm/dd/yy",
            showOtherMonths: true,
            selectOtherMonths: true,
            autoclose: true,
            changeMonth: true,
            changeYear: true
            //gotoCurrent: true,
        }).datepicker("setDate", "0");

</script>


<script>

$(document).ready(function () {
    $('#time').timepicker({
        showInputs: false
    });
});

</script>


<?php include_once '../include/footer.php'?>
