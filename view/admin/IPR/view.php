<?php

include_once '../../../vendor/autoload.php';
$ipr = new \App\admin\IPR\IPR();
$ipr=$ipr->view($_GET['id']);
//var_dump($ipr);

?>
<?php
include_once '../include/header.php';

?>

<?php
include_once '../include/sidebar.php';

?>


    <div class="content-wrapper">

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Exmerimental Information</h3>

            </div>

        </div>

        <!-- Main content -->
        <section class="content " style="min-height: 902.8px;">
            <!-- Small boxes (Stat box) -->

            <!-- /.row -->
            <!-- Main row -->
            <div class="row">
                <div class="col-lg-12">
                    <!-- Left col -->
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">Details of Experiances</h3>
                        </div>



                        <div class="box-body">


                            <div class="col-sm-6 invoice-col">


                                <b>Paitent Detail</b> <br>
                                <br>
                                <b>Paitent Name:</b> <?php echo $ipr['paitent_name'];?> <br>
                                <b>Client Type:</b><?php echo $ipr['client_type'];?> <br>
                                <b>Company Name:</b> <?php echo $ipr['company_name'];?> <br>
                                <b>Age:</b> <?php echo $ipr['age'];?> <br>
                                <b>Gender:</b> <?php echo $ipr['gender'];?> <br>
                                <b>Father/Husband:</b><?php echo $ipr['f_or_h_name'];?> <br>
                                <b>Guardian Name:</b> <?php echo $ipr['guardian_name'];?> <br>
                                <b>Address Detail:</b> <?php echo $ipr['address'];?> <br>
                                <b>Religion:</b> <?php echo $ipr['religion'];?> <br>
                                <b>Contact No:</b><?php echo $ipr['contact_no'];?> <br>
                                <b>Department:</b> <?php echo $ipr['department'];?> <br>
                                <b>Admission Doctor:</b> <?php echo $ipr['admission_doc'];?> <br>
                                <b>Consultant Name:</b> <?php echo $ipr['consultant'];?> <br>
                                <b>Diagnosis:</b> <?php echo $ipr['diagnosis'];?> <br>
                                <b>Referred By:</b> <?php echo $ipr['reffered_by'];?> <br>
                                <b>Occupation:</b> <?php echo $ipr['occupation'];?> <br>
                                <b>Reg No:</b> <?php echo $ipr['reg_no'];?> <br>
                                <b>Bed No:</b> <?php echo $ipr['bed_no'];?> <br>
                                <b>Date Time:</b> <?php echo $ipr['date_time'];?> <br>
                                <b>Height:</b> <?php echo $ipr['height'];?> <br>
                                <b>Weight:</b> <?php echo $ipr['weight'];?> <br>

                                <b>Blood Pressure</b> <br>
                                <b>SBP:</b> <?php echo $ipr['sbp'];?> <br>
                                <b>DBP:</b> <?php echo $ipr['dbp'];?> <br>








                                <div  style="padding: 4%">
                                    <a class="btn btn-info" href="view/admin/IPR/edit.php?id=<?php echo $ipr['id']?>">Edit</a>
                                    <a class="btn btn-danger" data-toggle="modal" data-target="#myModal" href="" data-id="<?php echo $ipr['id']?>">Delete</a>
                                </div>


                            </div>



                        </div>


                        <!-- /.form-box -->
                        <div>


                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <form action="view/admin/IPR/tmp_delete.php" method="get">
                                        <input id="delete" type="hidden" name="id" value="<?php echo $ipr['id'];?>">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title" id="myModalLabel">Are you sure want to Delete ?</h4>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                                                <button type="submit" class="btn btn-danger">Delete</button>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>


                        </div>
                    </div>
                </div>
                <!-- right col (We are only adding the ID to make the widgets sortable)-->

                <!-- right col -->
            </div>
            <!-- /.row (main row) -->

        </section>
        <!-- /.content -->
    </div>

<?php
include_once '../include/footer.php';
?>