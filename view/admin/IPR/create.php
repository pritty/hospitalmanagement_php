
<?php
include_once '../../../vendor/autoload.php';
$bed = new \App\admin\Bedcabin\Bedcabin();
$beds = $bed->index();
$corporate = new \App\admin\Corporate\Corporate();
$corporates = $corporate->index();
//var_dump($corporates);

$dept = new \App\admin\Department\Department();
$depts = $dept->index();
//var_dump($depts);
$doctor = new \App\admin\Doctor\Doctor();
$doctors = $doctor->index();
//var_dump($doctors);
?>
<?php
include_once '../include/header.php';

?>

<?php
include_once '../include/sidebar.php';

?>


    <div class="content-wrapper">

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Paitent Registration</h3>

            </div>

        </div>

        <!-- Main content -->
        <section class="content " style="min-height: 902.8px;">
            <!-- Small boxes (Stat box) -->

            <!-- /.row -->
            <!-- Main row -->
            <div class="row">
                <div class="col-lg-12">
                    <!-- Left col -->
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <center>
                                <h3 class="box-title"><u>Enter Paitent Information</u></h3>
                            </center>

                        </div>

                        <div class="box-body">
                            <div class="row">
                                <form role="form" action="view/admin/IPR/store.php" method="POST" enctype="multipart/form-data">
                                    <div class="col-md-4">


                                        <div class="form-group">
                                            <label>Client Type</label>
                                            <select name="client_type" id="" class="form-control" required>
                                                <option>Select One</option>
                                                <option value="General Room">General Room</option>
                                                <option value="Corporate">Corporate</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Company</label>
                                            <select name="company_name" id="" class="form-control" >
                                                <option>Select One</option>
                                                <?php foreach ($corporates as $corporate){?>
                                                <option value="<?php echo $corporate['name'];?>"> <?php echo $corporate['name'];?> </option>
                                                <?php }?>
                                            </select>
                                        </div>



                                        <div class="form-group">
                                            <label>Paitent Name</label>
                                            <input name="paitent_name" class="form-control" placeholder="Paitent Name" required>
                                        </div>

                                        <div class="form-group">
                                            <label>Age</label>
                                            <input name="age" class="form-control" placeholder="Age" required>
                                        </div>

                                        <div class="form-group">
                                            <label>Gender:</label>
                                            <div>
                                                <input name="gender" type="radio" class="" value="Male">Male
                                                <input name="gender" type="radio" class="" value="Female">Female
                                                <input name="gender" type="radio" class="" value="Other">Other
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Father/Husband</label>
                                            <input name="f_or_h_name" class="form-control" placeholder="Father/Husband">
                                        </div>

                                        <div class="form-group">
                                            <label>Guardian Name</label>
                                            <input name="guardian_name" class="form-control" placeholder="Guardian Name" required>
                                        </div>

                                        <div class="form-group">
                                            <label>Address Detail</label>
                                            <input name="address" class="form-control" placeholder="Address Detail" required>
                                        </div>

                                    </div>

                                        <div class="col-md-4">

                                            <div class="form-group">
                                                <label>Religion</label>
                                                <select name="religion" id="" class="form-control">
                                                    <option value="Islam">Islam</option>
                                                    <option value="Sanatan">Sanatan</option>
                                                    <option value="Christian">Christian</option>
                                                    <option value="Buddah">Buddah</option>
                                                    <option value="Others">Others</option>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label>Contact No</label>
                                                <input name="contact_no" class="form-control" placeholder="Contact No" required>
                                            </div>
                                            <div class="form-group">
                                                <label>Department</label>
                                                <select name="department" id="" class="form-control" required>
                                                    <option>Select One</option>
                                                    <?php foreach ($depts as $dept){?>
                                                    <option value="<?php echo $dept['name'];?>"> <?php echo $dept['name'];?> </option>
                                                    <?php }?>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                            <label>Admission Doctor</label>
                                            <select name="admission_doc" id="" class="form-control" required>
                                                <option>Select One</option>
                                                <?php foreach ($doctors as $doctor){?>
                                                <option value="<?php echo $doctor['name'];?>"> <?php echo $doctor['name'];?> </option>
                                                <?php }?>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Consultant Name</label>
                                            <select name="consultant" id="" class="form-control" required>
                                                <option>Select One</option>
                                                <?php foreach ($doctors as $doctor){?>
                                                    <option value="<?php echo $doctor['name'];?>"> <?php echo $doctor['name'];?> </option>
                                                <?php }?>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Diagnosis</label>
                                            <select name="diagnosis" id="" class="form-control" required>
                                                <option>Select One</option>
                                                <option value="None">None</option>
                                                <option value="General Room">General Room</option>
                                                <option value="General Room">General Room</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Referred By</label>
                                            <select name="reffered_by" id="" class="form-control" required>
                                                <option>Select One</option>
                                                <?php foreach ($doctors as $doctor){?>
                                                    <option value="<?php echo $doctor['name'];?>"> <?php echo $doctor['name'];?> </option>
                                                <?php }?>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Occupation</label>
                                            <select name="occupation" id="" class="form-control" required>
                                                <option>Select One</option>
                                                <option value="General Room">General Room</option>
                                                <option value="General Room">General Room</option>
                                                <option value="General Room">General Room</option>
                                            </select>
                                        </div>
                                        </div>


                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <label>Reg No</label>
                                            <input name="reg_no" class="form-control" placeholder="Reg No" required>
                                        </div>

                                        <div class="form-group">
                                            <label>Bed No</label>
                                            <input name="bed_no" class="form-control" placeholder="Bed No" required>
                                        </div>

                                        <div class="form-group">
                                            <label>Date Time</label>
                                            <input name="date_time" class="form-control" placeholder="Date Time" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Height</label>
                                            <input name="height" class="form-control" placeholder="Height" >
                                        </div>
                                        <div class="form-group">
                                            <label>Weight</label>
                                            <input name="weight" class="form-control" placeholder="Weight" >
                                        </div>

                                        <div>
                                            <label for="">Blood Pressure</label>
                                        <div class="form-group">
                                            <label>SBP</label>
                                            <input name="sbp" class="form-control" placeholder="SBP" required>
                                        </div>

                                            <div class="form-group">
                                            <label>DBP</label>
                                            <input name="dbp" class="form-control" placeholder="DBP" required>
                                        </div>
                                        </div>

                                    </div>





                                    <div class="col-md-12" style="padding:4%;">
                                        <center>

                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <button type="reset" class="btn btn-default">Reset</button>
                                        </center>


                                    </div>

                                </form>

                            </div>

                        </div>
                        <!-- /.form-box -->
                        <div>

                        </div>
                    </div>
                </div>
                <!-- right col (We are only adding the ID to make the widgets sortable)-->

                <!-- right col -->
            </div>
            <!-- /.row (main row) -->

        </section>
        <!-- /.content -->
    </div>

<?php
include_once '../include/footer.php';
?>