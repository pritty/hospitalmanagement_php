
<?php

include_once '../../../vendor/autoload.php';
$ipr = new \App\admin\IPR\IPR();
$ipr=$ipr->view($_GET['id']);
//var_dump($ipr);
$bed = new \App\admin\Bedcabin\Bedcabin();
$beds = $bed->index();
$corporate = new \App\admin\Corporate\Corporate();
$corporates = $corporate->index();
//var_dump($corporates);

$dept = new \App\admin\Department\Department();
$depts = $dept->index();
//var_dump($depts);
$doctor = new \App\admin\Doctor\Doctor();
$doctors = $doctor->index();
//var_dump($doctors);
?>
<?php include_once '../include/header.php'?>

<div class="content-wrapper" style="">
    <section class="content-header">
        <h1>
            Paitent Registration

        </h1>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- Default box -->
            <div class="col-md-12">

                <div class="box box-primary">
                    <form class="form-horizontal" action="view/admin/IPR/update.php" method="POST" enctype="multipart/form-data">
                        <div class="box-header with-border">

                            <div class="col-md-6" style="padding: 24px">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-4 control-label">Client Type</label>
                                    <div class="col-sm-8">
                                        <select name="client_type" id="" class="form-control" required>

                                            <option <?php echo ($ipr['company_name']=='General Room')? 'selected':'' ?> value="General Room">General Room</option>
                                            <option <?php echo ($ipr['company_name']=='Corporate')? 'selected':'' ?> value="Corporate">Corporate</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-4 control-label">Company Name</label>
                                    <div class="col-sm-8">
                                        <select name="company_name" id="" class="form-control" >
                                            <?php foreach ($corporates as $corporate){?>

                                                <option <?php echo ($ipr['company_name']==$corporate['name'])? 'selected':'' ?>  value="<?php echo $corporate['name'];?>"> <?php echo $corporate['name'];?> </option>
                                            <?php }?>
                                        </select>
                                    </div>
                                </div>

                            </div>

                            <div class="col-md-6" style="padding:24px;">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="" class="col-sm-4 control-label">Reg No</label>
                                            <div class="col-sm-8">
                                                <input type="" class="form-control" name="reg_no" value="<?php echo $ipr['reg_no'];?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="" class="col-sm-4 control-label">Bed No</label>
                                            <div class="col-sm-8">
                                                <input type="" class="form-control" name="bed_no" value="<?php echo $ipr['bed_no'];?>">
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="" class="col-sm-4 control-label">Date</label>
                                            <div class="col-sm-8">
                                                <input type="" class="form-control" name="date_time" value="<?php echo $ipr['date_time'];?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="" class="col-sm-4 control-label">Time</label>
                                            <div class="col-sm-8">
                                                <input type="" class="form-control" id="inputPassword3" placeholder="Time">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>



                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 ">Name</label>
                                                <div class="col-sm-8">
                                                    <input type="" class="form-control" name="paitent_name" value="<?php echo $ipr['paitent_name'];?>">
                                                    <input name="id" type="hidden" class="form-control" value="<?php echo $ipr['id'];?>" >
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 ">Father/Husband Name</label>
                                                <div class="col-sm-8">
                                                    <input type="" class="form-control" name="f_or_h_name" value="<?php echo $ipr['f_or_h_name'];?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 ">Guardian Name</label>
                                                <div class="col-sm-8">
                                                    <input type="" class="form-control" name="guardian_name" value="<?php echo $ipr['guardian_name'];?>">
                                                </div>
                                            </div>

                                        </div>


                                        <div class="col-md-4">

                                            <div class="form-group">
                                                <label for="" class="col-sm-4 control-label ">Gender</label>
                                                <div class="col-sm-8">
                                                    <div class="radio">
                                                        <label>
                                                            <input <?php echo ($ipr['gender']=='Male')? 'checked':'' ?> name="gender" id="optionsRadios1" value="Male"  type="radio">
                                                            Male
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label>
                                                            <input <?php echo ($ipr['gender']=='Female')? 'checked':'' ?> name="gender" id="optionsRadios2" value="Female" type="radio">
                                                            Female
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label>
                                                            <input <?php echo ($ipr['gender']=='Other')? 'checked':'' ?> name="gender" id="optionsRadios3" value="Other" disabled="" type="radio">
                                                            Other
                                                        </label>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <label for="" class="col-sm-4 control-label">Age</label>
                                                <div class="col-sm-8">
                                                    <input type="" class="form-control" name="age" value="<?php echo $ipr['age'];?>">
                                                </div>
                                            </div>

                                        </div>

                                    </div>


                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 ">Address Details</label>
                                                <div class="col-sm-8">
                                                    <input type="" class="form-control" name="address" value="<?php echo $ipr['address'];?>">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 control-label">Religion</label>
                                                <div class="col-sm-8">
                                                    <select name="religion" id="" class="form-control">
                                                        <option <?php echo ($ipr['religion']=='Islam')? 'selected':'' ?> value="Islam">Islam</option>
                                                        <option <?php echo ($ipr['religion']=='Sanatan')? 'selected':'' ?> value="Sanatan">Sanatan</option>
                                                        <option <?php echo ($ipr['religion']=='Christian')? 'selected':'' ?> value="Christian">Christian</option>
                                                        <option <?php echo ($ipr['religion']=='Buddah')? 'selected':'' ?> value="Buddah">Buddah</option>
                                                        <option <?php echo ($ipr['religion']=='Others')? 'selected':'' ?> value="Others">Others</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="" class="col-sm-4 control-label">Contact No</label>
                                                <div class="col-sm-8">
                                                    <input type="" class="form-control" name="contact_no" value="<?php echo $ipr['contact_no'];?>">
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>


                                <div class="col-md-4">

                                    <div class="form-group">
                                        <div class="col-sm-6">
                                            <label>Height</label>
                                            <input type="" class="form-control" name="height" value="<?php echo $ipr['height'];?>">
                                        </div>
                                        <div class="col-sm-6">
                                            <label>Weight</label>
                                            <input type="" class="form-control" name="weight" value="<?php echo $ipr['weight'];?>">
                                        </div>
                                    </div>
                                    <div style="border: 1px solid #f0f0f1; box-sizing: border-box">
                                        <h4>Blood Pressure:</h4>
                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <label>SBP</label>
                                                <input type="" class="form-control" name="sbp" value="<?php echo $ipr['sbp'];?>">
                                            </div>
                                            <div class="col-sm-6">
                                                <label>DBP</label>
                                                <input type="" class="form-control" name="dbp" value="<?php echo $ipr['dbp'];?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <center><hr></center>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-4 control-label">Department</label>
                                        <div class="col-sm-8">
                                            <select name="department" id="" class="form-control" >

                                                <?php foreach ($depts as $dept){?>
                                                    <option <?php echo ($ipr['department']==$dept['name'])? 'selected':'' ?> value="<?php echo $dept['name'];?>"> <?php echo $dept['name'];?> </option>
                                                <?php }?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-4 control-label">Diagnosis</label>
                                        <div class="col-sm-8">
                                            <input type="" name="diagnosis" class="form-control" id="inputEmail3" value="<?php echo $ipr['diagnosis'];?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-4 control-label">Addmission Doctor</label>
                                        <div class="col-sm-8">
                                            <select name="admission_doc" id="" class="form-control" required>
                                                <?php foreach ($doctors as $doctor){?>
                                                    <option <?php echo ($ipr['admission_doc']==$doctor['name'])? 'selected':'' ?> value="<?php echo $doctor['name'];?>"> <?php echo $doctor['name'];?> </option>
                                                <?php }?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-4 control-label">Referred By</label>
                                        <div class="col-sm-8">
                                            <select name="reffered_by" id="" class="form-control" required>
                                                <?php foreach ($doctors as $doctor){?>
                                                    <option <?php echo ($ipr['reffered_by']==$doctor['name'])? 'selected':'' ?> value="<?php echo $doctor['name'];?>"> <?php echo $doctor['name'];?> </option>
                                                <?php }?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-4 control-label">Consultant Name</label>
                                        <div class="col-sm-8">
                                            <select name="consultant" id="" class="form-control" required>
                                                <?php foreach ($doctors as $doctor){?>
                                                    <option <?php echo ($ipr['consultant']==$doctor['name'])? 'selected':'' ?> value="<?php echo $doctor['name'];?>"> <?php echo $doctor['name'];?> </option>
                                                <?php }?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-4 control-label">Occupation</label>
                                        <div class="col-sm-8">
                                            <input type="" class="form-control" name="occupation" value="<?php echo $ipr['occupation'];?>">
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <a href="view/admin/IPR/index.php" type="submit" class="btn btn-default pull-right" style="margin-left:5px; ">Cancel</a>
                            <button href="" type="submit" class="btn btn-info pull-right">Update</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                    <!-- /.box-footer-->
                </div>
                <!-- /.box -->
            </div>

        </div>

    </section>
    <!-- /.content -->




</div>
<!-- /.content-wrapper -->
<?php include_once '../include/footer.php'?>
