<?php

include_once '../../../vendor/autoload.php';
$ipr = new \App\admin\IPR\IPR();
$ipr=$ipr->view($_GET['id']);
//var_dump($ipr);
$bed = new \App\admin\Bedcabin\Bedcabin();
$beds = $bed->index();
$corporate = new \App\admin\Corporate\Corporate();
$corporates = $corporate->index();
//var_dump($corporates);

$dept = new \App\admin\Department\Department();
$depts = $dept->index();
//var_dump($depts);
$doctor = new \App\admin\Doctor\Doctor();
$doctors = $doctor->index();
//var_dump($doctors);
?>

?>
<?php
include_once '../include/header.php';

?>

<?php
include_once '../include/sidebar.php';

?>


<div class="content-wrapper">

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Paitent Registration</h3>

        </div>

    </div>

    <!-- Main content -->
    <section class="content " style="min-height: 902.8px;">
        <!-- Small boxes (Stat box) -->

        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
            <div class="col-lg-12">
                <!-- Left col -->
                <div class="box box-default">
                    <div class="box-header with-border">
                        <center>
                            <h3 class="box-title"><u>Enter Paitent Information</u></h3>
                        </center>

                    </div>

                    <div class="box-body">
                        <div class="row">
                            <form role="form" action="view/admin/IPR/update.php" method="POST" enctype="multipart/form-data">
                                <div class="col-md-4">


                                    <div class="form-group">
                                        <label>Client Type</label>
                                        <select name="client_type" id="" class="form-control" required>

                                            <option <?php echo ($ipr['company_name']=='General Room')? 'selected':'' ?> value="General Room">General Room</option>
                                            <option <?php echo ($ipr['company_name']=='Corporate')? 'selected':'' ?> value="Corporate">Corporate</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Company</label>
                                        <select name="company_name" id="" class="form-control" >
                                            <?php foreach ($corporates as $corporate){?>

                                                <option <?php echo ($ipr['company_name']==$corporate['name'])? 'selected':'' ?>  value="<?php echo $corporate['name'];?>"> <?php echo $corporate['name'];?> </option>
                                            <?php }?>
                                        </select>
                                    </div>



                                    <div class="form-group">
                                        <label>Paitent Name</label>
                                        <input name="paitent_name" class="form-control" value="<?php echo $ipr['paitent_name'];?>">
                                        <input name="id" type="hidden" class="form-control" value="<?php echo $ipr['id'];?>" >
                                    </div>

                                    <div class="form-group">
                                        <label>Age</label>
                                        <input name="age" class="form-control" value="<?php echo $ipr['age'];?>">
                                    </div>

                                    <div class="form-group">
                                        <label>Gender:</label>
                                        <div>
                                            <input <?php echo ($ipr['gender']=='Male')? 'checked':'' ?> name="gender" type="radio" class="" value="Male">Male
                                            <input <?php echo ($ipr['gender']=='Female')? 'checked':'' ?> name="gender" type="radio" class="" value="Female">Female
                                            <input <?php echo ($ipr['gender']=='Other')? 'checked':'' ?> name="gender" type="radio" class="" value="Other">Other
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Father/Husband</label>
                                        <input name="f_or_h_name" class="form-control" value="<?php echo $ipr['f_or_h_name'];?>">
                                    </div>

                                    <div class="form-group">
                                        <label>Guardian Name</label>
                                        <input name="guardian_name" class="form-control" value="<?php echo $ipr['guardian_name'];?>">
                                    </div>

                                    <div class="form-group">
                                        <label>Address Detail</label>
                                        <input name="address" class="form-control" value="<?php echo $ipr['address'];?>">
                                    </div>

                                </div>

                                <div class="col-md-4">

                                    <div class="form-group">
                                        <label>Religion</label>
                                        <select name="religion" id="" class="form-control">
                                            <option <?php echo ($ipr['religion']=='Islam')? 'selected':'' ?> value="Islam">Islam</option>
                                            <option <?php echo ($ipr['religion']=='Sanatan')? 'selected':'' ?> value="Sanatan">Sanatan</option>
                                            <option <?php echo ($ipr['religion']=='Christian')? 'selected':'' ?> value="Christian">Christian</option>
                                            <option <?php echo ($ipr['religion']=='Buddah')? 'selected':'' ?> value="Buddah">Buddah</option>
                                            <option <?php echo ($ipr['religion']=='Others')? 'selected':'' ?> value="Others">Others</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Contact No</label>
                                        <input name="contact_no" class="form-control" value="<?php echo $ipr['contact_no'];?>">
                                    </div>
                                    <div class="form-group">
                                        <label>Department</label>
                                        <select name="department" id="" class="form-control" required>

                                            <?php foreach ($depts as $dept){?>
                                                <option <?php echo ($ipr['department']==$dept['name'])? 'selected':'' ?> value="<?php echo $dept['name'];?>"> <?php echo $dept['name'];?> </option>
                                            <?php }?>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Admission Doctor</label>
                                        <select name="admission_doc" id="" class="form-control" required>

                                            <?php foreach ($doctors as $doctor){?>
                                                <option <?php echo ($ipr['admission_doc']==$doctor['name'])? 'selected':'' ?> value="<?php echo $doctor['name'];?>"> <?php echo $doctor['name'];?> </option>
                                            <?php }?>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Consultant Name</label>
                                        <select name="consultant" id="" class="form-control" required>
                                            <?php foreach ($doctors as $doctor){?>
                                                <option <?php echo ($ipr['consultant']==$doctor['name'])? 'selected':'' ?> value="<?php echo $doctor['name'];?>"> <?php echo $doctor['name'];?> </option>
                                            <?php }?>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Diagnosis</label>
                                        <select name="diagnosis" id="" class="form-control" required>
                                            <option>Select One</option>
                                            <option <?php echo ($ipr['diagnosis']=='')? 'selected':'' ?> value="General Room">General Room</option>
                                            <option <?php echo ($ipr['diagnosis']=='')? 'selected':'' ?> value="General Room">General Room</option>
                                            <option <?php echo ($ipr['diagnosis']=='')? 'selected':'' ?> value="General Room">General Room</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Referred By</label>
                                        <select name="reffered_by" id="" class="form-control" >

                                            <?php foreach ($doctors as $doctor){?>
                                                <option <?php echo ($ipr['reffered_by']==$doctor['name'])? 'selected':'' ?> value="<?php echo $doctor['name'];?>"> <?php echo $doctor['name'];?> </option>
                                            <?php }?>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Occupation</label>
                                        <select name="occupation" id="" class="form-control" >
                                            <option>Select One</option>
                                            <option <?php echo ($ipr['occupation']=='General Room')? 'selected':'' ?> value="General Room">General Room</option>
                                            <option <?php echo ($ipr['occupation']=='General Room')? 'selected':'' ?> value="General Room">General Room</option>
                                            <option <?php echo ($ipr['occupation']=='General Room')? 'selected':'' ?> value="General Room">General Room</option>
                                        </select>
                                    </div>
                                </div>


                                <div class="col-md-4">

                                    <div class="form-group">
                                        <label>Reg No</label>
                                        <input name="reg_no" class="form-control" value="<?php echo $ipr['reg_no'];?>">
                                    </div>

                                    <div class="form-group">
                                        <label>Bed No</label>
                                        <input name="bed_no" class="form-control" value="<?php echo $ipr['bed_no'];?>">
                                    </div>

                                    <div class="form-group">
                                        <label>Date Time</label>
                                        <input name="date_time" class="form-control" value="<?php echo $ipr['date_time'];?>">
                                    </div>
                                    <div class="form-group">
                                        <label>Height</label>
                                        <input name="height" class="form-control" value="<?php echo $ipr['height'];?>" >
                                    </div>
                                    <div class="form-group">
                                        <label>Weight</label>
                                        <input name="weight" class="form-control" value="<?php echo $ipr['weight'];?>" >
                                    </div>

                                    <div>
                                        <label for="">Blood Pressure</label>
                                        <div class="form-group">
                                            <label>SBP</label>
                                            <input name="sbp" class="form-control" value="<?php echo $ipr['sbp'];?>">
                                        </div>

                                        <div class="form-group">
                                            <label>DBP</label>
                                            <input name="dbp" class="form-control" value="<?php echo $ipr['dbp'];?>">
                                        </div>
                                    </div>

                                </div>





                                <div class="col-md-12" style="padding:4%;">
                                    <center>

                                        <button type="submit" class="btn btn-primary">Update</button>

                                    </center>


                                </div>

                            </form>

                        </div>

                    </div>
                    <!-- /.form-box -->
                    <div>

                    </div>
                </div>
            </div>
            <!-- right col (We are only adding the ID to make the widgets sortable)-->

            <!-- right col -->
        </div>
        <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
</div>

<?php
include_once '../include/footer.php';
?>