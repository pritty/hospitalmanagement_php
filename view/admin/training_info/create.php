
<?php
include_once '../include/header.php';

?>

<?php
include_once '../include/sidebar.php';

?>


<div class="content-wrapper">

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Training Information</h3>

        </div>

    </div>

    <!-- Main content -->
    <section class="content " style="min-height: 902.8px;">
        <!-- Small boxes (Stat box) -->

        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
            <div class="col-lg-12">
                <!-- Left col -->
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">Enter Training Info</h3>
                    </div>

                    <div class="box-body">
                        <div class="row">
                            <form role="form" action="view/admin/training_info/store.php" method="POST" enctype="multipart/form-data">
                                <div class="col-md-6">


                                    <div class="form-group">
                                        <label>Doctor ID</label>
                                        <select name="doc_id" id="" class="form-control" required>
                                            <option>Select One</option>
                                            <option value="1001">10001</option>
                                        </select>

                                    </div>

                                    <div>
                                        <label for="">Training 1</label>
                                        <div class="form-group">
                                            <label>Institute Name</label>
                                            <input name="institute_name1" class="form-control" placeholder="Institute Name">
                                        </div>
                                        <div class="form-group">
                                            <label>Subject Name</label>
                                            <input name="subject1" class="form-control" placeholder="Subject Name">
                                        </div>
                                        <div class="form-group">
                                            <label>Joining Year</label>
                                            <input name="join_year1" class="form-control" placeholder="Joining Year">
                                        </div>
                                    </div>

                                    <div>
                                        <label for="">Training 2</label>
                                        <div class="form-group">
                                            <label>Institute Name</label>
                                            <input name="institute_name2" class="form-control" placeholder="Institute Name">
                                        </div>
                                        <div class="form-group">
                                            <label>Subject Name</label>
                                            <input name="subject2" class="form-control" placeholder="Subject Name">
                                        </div>
                                        <div class="form-group">
                                            <label>Joining Year</label>
                                            <input name="join_year2" class="form-control" placeholder="Joining Year">
                                        </div>
                                    </div>

                                    <div>
                                        <label for="">Training 1</label>
                                        <div class="form-group">
                                            <label>Institute Name</label>
                                            <input name="institute_name3" class="form-control" placeholder="Institute Name">
                                        </div>
                                        <div class="form-group">
                                            <label>Subject Name</label>
                                            <input name="subject3" class="form-control" placeholder="Subject Name">
                                        </div>
                                        <div class="form-group">
                                            <label>Joining Year</label>
                                            <input name="join_year3" class="form-control" placeholder="Joining Year">
                                        </div>
                                    </div>

                                            <div>

                                                <button type="submit" class="btn btn-primary">Submit</button>
                                                <button type="reset" class="btn btn-default">Reset Button</button>
                                            </div>
                                    </div>
                            </form>

                        </div>

                    </div>
                    <!-- /.form-box -->
                    <div>

                    </div>
                </div>
            </div>
            <!-- right col (We are only adding the ID to make the widgets sortable)-->

            <!-- right col -->
        </div>
        <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
</div>

<?php
include_once '../include/footer.php';
?>