<?php

include_once '../../../vendor/autoload.php';
$traininf_info = new \App\admin\Training_info\Training_info();
$traininf_info=$traininf_info->view($_GET['id']);
//var_dump($traininf_info);

?>
<?php
include_once '../include/header.php';

?>

<?php
include_once '../include/sidebar.php';

?>


<div class="content-wrapper">

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Training Information</h3>

        </div>

    </div>

    <!-- Main content -->
    <section class="content " style="min-height: 902.8px;">
        <!-- Small boxes (Stat box) -->

        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
            <div class="col-lg-12">
                <!-- Left col -->
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">Enter Training Info</h3>
                    </div>

                    <div class="box-body">
                        <div class="row">
                            <form role="form" action="view/admin/training_info/update.php" method="POST" enctype="multipart/form-data">
                                <div class="col-md-6">


                                    <div class="form-group">
                                        <label>Doctor ID</label>
                                        <select name="doc_id" id="" class="form-control" >
                                            <option>Select One</option>
                                            <option value="1001">10001</option>
                                        </select>

                                    </div>

                                    <div>
                                        <label for="">Training 1</label>
                                        <div class="form-group">
                                            <label>Institute Name</label>
                                            <input name="institute_name1" class="form-control" value="<?php echo $traininf_info['institute_name1']; ?>">
                                            <input name="id" type="hidden" class="form-control" value="<?php echo $traininf_info['id']; ?>">
                                        </div>
                                        <div class="form-group">
                                            <label>Subject Name</label>
                                            <input name="subject1" class="form-control" value="<?php echo $traininf_info['subject1']; ?>">
                                        </div>
                                        <div class="form-group">
                                            <label>Joining Year</label>
                                            <input name="join_year1" class="form-control" value="<?php echo $traininf_info['join_year1']; ?>">
                                        </div>
                                    </div>

                                    <div>
                                        <label for="">Training 2</label>
                                        <div class="form-group">
                                            <label>Institute Name</label>
                                            <input name="institute_name2" class="form-control" value="<?php echo $traininf_info['institute_name2']; ?>">
                                        </div>
                                        <div class="form-group">
                                            <label>Subject Name</label>
                                            <input name="subject2" class="form-control" value="<?php echo $traininf_info['subject2']; ?>">
                                        </div>
                                        <div class="form-group">
                                            <label>Joining Year</label>
                                            <input name="join_year2" class="form-control" value="<?php echo $traininf_info['join_year2']; ?>">
                                        </div>
                                    </div>

                                    <div>
                                        <label for="">Training 1</label>
                                        <div class="form-group">
                                            <label>Institute Name</label>
                                            <input name="institute_name3" class="form-control" value="<?php echo $traininf_info['institute_name3']; ?>">
                                        </div>
                                        <div class="form-group">
                                            <label>Subject Name</label>
                                            <input name="subject3" class="form-control" value="<?php echo $traininf_info['subject3']; ?>">
                                        </div>
                                        <div class="form-group">
                                            <label>Joining Year</label>
                                            <input name="join_year3" class="form-control" value="<?php echo $traininf_info['join_year3']; ?>">
                                        </div>
                                    </div>

                                    <div>

                                        <button type="submit" class="btn btn-primary">Update</button>

                                    </div>
                                </div>
                            </form>

                        </div>

                    </div>
                    <!-- /.form-box -->
                    <div>

                    </div>
                </div>
            </div>
            <!-- right col (We are only adding the ID to make the widgets sortable)-->

            <!-- right col -->
        </div>
        <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
</div>

<?php
include_once '../include/footer.php';
?>