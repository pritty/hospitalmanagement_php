
<?php
include_once '../../../vendor/autoload.php';
$bed = new \App\admin\Bedcabin\Bedcabin();
$beds = $bed->index();

?>
<?php include_once '../include/header.php'?>

<div class="content-wrapper" style="">
    <section class="content-header">
<!--        <h1>-->
<!--            Cabin/Bed Setup-->
<!---->
<!--        </h1>-->

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- Default box -->
            <div class="col-md-12">

                <div class="box box-primary">
                    <form class="form-horizontal" action="view/admin/bed_cabin/store.php" method="POST" enctype="multipart/form-data">
                        <div class="box-header with-border">
                            <center>
                                <h2> Cabin/Bed Setup</h2>
                            </center>
                        </div>

                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-8 col-md-offset-2">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="form-box">

                                                <div class="form-group">
                                                    <label for="" class="col-sm-3 control-label">Bed Cabin Name</label>
                                                    <div class="col-sm-8">
                                                        <input type="" class="form-control" name="name" placeholder="Bed Cabin Name" autofocus>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-3 control-label">Type</label>
                                                    <div class="col-sm-8">
                                                        <select name="bed_type" id="" class="form-control" required>
                                                            <option>Select One</option>
                                                            <option value="General Room">General Room</option>
                                                            <option value="General AC Room">General AC Room</option>
                                                            <option value="VIP Room">VIP Room</option>
                                                            <option value="Ward">Ward</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-3 control-label">Rate</label>
                                                    <div class="col-sm-8">
                                                        <input type="" class="form-control" name="rate" placeholder="Rate">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-3 control-label">Mark</label>
                                                    <div class="col-sm-8">
                                                        <select name="mark" id="" class="form-control" required>
                                                            <option>Select One</option>
                                                            <option value="Yes">Yes</option>
                                                            <option value="No">No</option>

                                                        </select>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>




                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="reset" class="btn btn-default pull-right" style="margin-left:5px; "><i class="fa fa-refresh"></i> Cancel</button>
                            <button  type="submit" class="btn btn-info pull-right"><i class="fa fa-save"></i> Save</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                    <!-- /.box-footer-->
                </div>
                <!-- /.box -->
            </div>

        </div>

    </section>
    <!-- /.content -->



    <section class="content">
        <div class="row">
            <!-- Default box -->
            <div class="col-md-12">

                <div style="position: fixed; right: 35px; top: 100px; z-index: 111">
                    <?php
                    if(isset($_SESSION['msg'])){
                        echo "<div class='alert alert-success'>".$_SESSION['msg']."</div>";
                        session_unset();
                    }
                    if(isset($_SESSION['delete'])){
                        echo "<div class='alert alert-danger'>".$_SESSION['delete']."</div>";
                        session_unset();
                    }
                    if(isset($_SESSION['update'])){
                        echo "<div class='alert alert-info'>".$_SESSION['update']."</div>";
                        session_unset();
                    }


                    ?>
                </div>

                <div class="box box-primary">

                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th  style="text-align: center">SL No</th>
                                <th  style="text-align: center">Name</th>
                                <th  style="text-align: center">Type</th>
                                <th  style="text-align: center">Rate</th>
                                <th  style="text-align: center">Mark</th>
                                <th  style="text-align: center">Action</th>
                            </tr>
                            </thead>
                            <tbody style="text-align: center">

                            <?php
                            $sl=1;
                            foreach ($beds as $bed){?>
                                <tr>
                                    <td><?php echo $sl++;?></td>
                                    <td><?php echo $bed['name'];?></td>
                                    <td><?php echo $bed['bed_type'];?></td>
                                    <td><?php echo $bed['rate'];?></td>
                                    <td><?php echo $bed['mark'];?></td>
                                    <td>
                                        <a class="btn btn-info" href="view/admin/bed_cabin/edit.php?id=<?php echo $bed['id']?>">Edit</a>
                                        <a class="btn btn-danger" data-toggle="modal" data-target="#myModal" href="" data-id="<?php echo $bed['id']?>">Delete</a>
                                    </td>
                                </tr>

                            <?php }?>
                            </tbody>
                        </table>
                    </div>


                    <!-- /.box-body -->
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <form action="view/admin/bed_cabin/tmp_delete.php" method="get">
                                <input id="delete" type="hidden" name="id" value="<?php echo $bed['id'];?>">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" id="myModalLabel">Are you sure want to Delete ?</h4>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </div>
                                </div>
                            </form>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.box-footer-->
                </div>
                <!-- /.box -->
            </div>

        </div>

    </section>
</div>
<!-- /.content-wrapper -->
<?php include_once '../include/footer.php'?>
