

<?php
include_once '../../../vendor/autoload.php';
$bed = new \App\admin\Bedcabin\Bedcabin();
$beds = $bed->index();

?>

<?php include_once '../include/header.php'?>
<?php include_once '../include/sidebar.php'?>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Menus Tables
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- /.box -->
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Menu Table With Full Features</h3>
                        </div>


                        <div style="position: fixed; right: 35px; top: 100px; z-index: 111">
                            <?php
                            if(isset($_SESSION['msg'])){
                                echo "<div class='alert alert-success'>".$_SESSION['msg']."</div>";
                                session_unset();
                            }
                            if(isset($_SESSION['delete'])){
                                echo "<div class='alert alert-danger'>".$_SESSION['delete']."</div>";
                                session_unset();
                            }
                            if(isset($_SESSION['update'])){
                                echo "<div class='alert alert-info'>".$_SESSION['update']."</div>";
                                session_unset();
                            }


                            ?>
                        </div>


                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th  style="text-align: center">Name</th>
                                    <th  style="text-align: center">Type</th>
                                    <th  style="text-align: center">Rate</th>
                                    <th  style="text-align: center">Mark</th>
                                    <th  style="text-align: center">Action</th>
                                </tr>
                                </thead>
                                <tbody style="text-align: center">

                                <?php foreach ($beds as $bed){?>
                                <tr>
                                    <td><?php echo $bed['name'];?></td>
                                    <td><?php echo $bed['bed_type'];?></td>
                                    <td><?php echo $bed['rate'];?></td>
                                    <td><?php echo $bed['mark'];?></td>
                                    <td>
                                        <a class="btn btn-info" href="view/admin/bed_cabin/edit.php?id=<?php echo $bed['id']?>">Edit</a>
                                        <a class="btn btn-danger" data-toggle="modal" data-target="#myModal" href="" data-id="<?php echo $bed['id']?>">Delete</a>
                                    </td>
                                </tr>

                                <?php }?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->


                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <form action="view/admin/bed_cabin/tmp_delete.php" method="get">
                                <input id="delete" type="hidden" name="id" value="<?php echo $bed['id'];?>">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" id="myModalLabel">Are you sure want to Delete ?</h4>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </div>
                                </div>
                            </form>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>


                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>





<?php include_once '../include/footer.php'?>