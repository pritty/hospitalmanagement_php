<?php
include_once '../include/header.php';

?>


    <div class="content-wrapper">

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Bed Cabin</h3>

            </div>

        </div>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->

            <!-- /.row -->
            <!-- Main row -->
            <div class="row">
                <div class="col-lg-12">
                    <!-- Left col -->
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">Enter Bed Cabin</h3>
                        </div>

                        <div class="box-body">
                            <div class="row">
                                <form role="form" action="view/admin/bed_cabin/store.php" method="POST" enctype="multipart/form-data">
                                    <div class="col-md-6">


                                        <div class="form-group">
                                            <label>Bed Cabin Name</label>
                                            <input name="name" class="form-control" placeholder="Bed Cabin Name" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Type</label>

                                                <select name="bed_type" id="" class="form-control" required>
                                                    <option>Select One</option>
                                            <option value="General Room">General Room</option>
                                            <option value="General AC Room">General AC Room</option>
                                            <option value="VIP Room">VIP Room</option>
                                            <option value="Ward">Ward</option>
                                                </select>


                                        </div>
                                        <div class="form-group">
                                            <label>Rate</label>
                                            <input name="rate" class="form-control" placeholder="Cabin Rate" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Mark</label>
                                            <select name="mark" id="" class="form-control" required>
                                                <option>Select One</option>
                                                <option value="Yes">Yes</option>
                                                <option value="No">No</option>

                                            </select>
                                        </div>

                                        <button type="submit" class="btn btn-primary">Submit</button>
                                        <button type="reset" class="btn btn-default">Reset Button</button>
                                    </div>

                                </form>

                            </div>

                        </div>
                        <!-- /.form-box -->
                        <div>

                        </div>
                    </div>
                </div>
                <!-- right col (We are only adding the ID to make the widgets sortable)-->

                <!-- right col -->
            </div>
            <!-- /.row (main row) -->

        </section>
        <!-- /.content -->
    </div>

<?php
include_once '../include/footer.php';
?>