<?php

include_once '../../../vendor/autoload.php';
$logo = new \App\admin\Logo\Logo();
$logo=$logo->view($_GET['id']);
//var_dump($doctors);
?>

<?php
include_once '../include/header.php';

?>

<?php
include_once '../include/sidebar.php';

?>


    <div class="content-wrapper">

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Menu & Logo</h3>

            </div>

        </div>

        <!-- Main content -->
        <section class="content " style="min-height: 902.8px;">
            <!-- Small boxes (Stat box) -->

            <!-- /.row -->
            <!-- Main row -->
            <div class="row">
                <div class="col-lg-12">
                    <!-- Left col -->
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add Your Menus and Logo</h3>
                        </div>

                        <div class="box-body">
                            <div class="row">
                                <form role="form" action="view/admin/logo/update.php" method="POST" enctype="multipart/form-data">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>LOGO</label>
                                            <input type="file" name="image" class="">
                                            <input type="hidden" name="id" class="" value="<?php echo $logo['id']?>">
                                            <input type="hidden" name="image" class="" value="<?php echo $logo['logo']?>">

                                        </div>


                                        <div class="form-group">
                                            <img src="view/admin/uploads/logo/<?php echo $logo['doctors']?>" width="150" height="150" alt="<?php echo $logo['logo_name']?>">

                                        </div>


                                        <div class="form-group">
                                            <label>LOGO Name</label>
                                            <input name="logo_name" class="form-control" value="<?php echo $logo['logo_name'];?>">
                                        </div>

                                        <button type="submit" class="btn btn-primary">Update</button>
                                        <button type="reset" class="btn btn-default">Reset Button</button>


                                    </div>

                                </form>

                            </div>

                        </div>
                        <!-- /.form-box -->
                        <div>

                        </div>
                    </div>
                </div>
                <!-- right col (We are only adding the ID to make the widgets sortable)-->

                <!-- right col -->
            </div>
            <!-- /.row (main row) -->

        </section>
        <!-- /.content -->
    </div>

<?php
include_once '../include/footer.php';
?>