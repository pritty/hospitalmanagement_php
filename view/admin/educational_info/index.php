

<?php
include_once '../../../vendor/autoload.php';
$edu_info = new \App\admin\Educational_info\Educational_info();
$edu_infos = $edu_info->index();
//var_dump($edu_infos);
?>

<?php include_once '../include/header.php'?>
<?php include_once '../include/sidebar.php'?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Menus Tables
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <!-- /.box -->
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Menu Table With Full Features</h3>
                    </div>


                    <div style="position: fixed; right: 35px; top: 100px; z-index: 111">
                        <?php
                        if(isset($_SESSION['msg'])){
                            echo "<div class='alert alert-success'>".$_SESSION['msg']."</div>";
                            session_unset();
                        }
                        if(isset($_SESSION['delete'])){
                            echo "<div class='alert alert-danger'>".$_SESSION['delete']."</div>";
                            session_unset();
                        }
                        if(isset($_SESSION['update'])){
                            echo "<div class='alert alert-info'>".$_SESSION['update']."</div>";
                            session_unset();
                        }


                        ?>
                    </div>


                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Doctor Id</th>
                                <th>SSC School</th>
                                <th>SSC Pass-year</th>
                                <th>HSC College</th>
                                <th>HSC Pass-year</th>
                                <th>MBBS College</th>
                                <th>MBBS Pass-year</th>
                                <th>FCPS Ins.</th>
                                <th>FCPS Pass-year</th>
                                <th>Other Ins</th>
                                <th>Pass-year</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody style="text-align: center">

                            <?php foreach ($edu_infos as $edu_info){?>
                                <tr>
                                    <td><?php echo $edu_info['doc_id'];?></td>
                                    <td><?php echo $edu_info['ssc_school_name'];?></td>
                                    <td><?php echo $edu_info['ssc_passing_year'];?></td>
                                    <td><?php echo $edu_info['hsc_college_name'];?></td>
                                    <td><?php echo $edu_info['hsc_passing_year'];?></td>
                                    <td><?php echo $edu_info['mbbs_college_name'];?></td>
                                    <td><?php echo $edu_info['mbbs_passing_year'];?></td>
                                    <td><?php echo $edu_info['fcps_inst_name'];?></td>
                                    <td><?php echo $edu_info['fcps_passing_year'];?></td>
                                    <td><?php echo $edu_info['other_inst_name'];?></td>
                                    <td><?php echo $edu_info['other_passing_year'];?></td>
                                    <td>
                                        <a class="btn btn-info" href="view/admin/educational_info/edit.php?id=<?php echo $edu_info['id']?>">Edit</a>
                                        <a class="btn btn-danger" data-toggle="modal" data-target="#myModal" href="" data-id="<?php echo $edu_info['id']?>">Delete</a>
                                    </td>
                                </tr>

                            <?php }?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->


                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <form action="view/admin/educational_info/tmp_delete.php" method="get">
                            <input id="delete" type="hidden" name="id" value="<?php echo $edu_info['id'];?>">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title" id="myModalLabel">Are you sure want to Delete ?</h4>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </div>
                            </div>
                        </form>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>


            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>





<?php include_once '../include/footer.php'?>