<?php
include_once '../include/header.php';

?>

<?php
include_once '../include/sidebar.php';

?>


    <div class="content-wrapper">

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Educational Information</h3>

            </div>

        </div>

        <!-- Main content -->
        <section class="content " style="min-height: 902.8px;">
            <!-- Small boxes (Stat box) -->

            <!-- /.row -->
            <!-- Main row -->
            <div class="row">
                <div class="col-lg-12">
                    <!-- Left col -->
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">Enter Educational Info</h3>
                        </div>

                        <div class="box-body">
                            <div class="row">
                                <form role="form" action="view/admin/educational_info/store.php" method="POST" enctype="multipart/form-data">
                                    <div class="col-md-6">


                                        <div class="form-group">
                                            <label>Doctor ID</label>

                                            <select name="doc_id" id="" class="form-control" required>
                                                <option>Select One</option>
                                                <option value="1001">10001</option>

                                            </select>


                                        </div>

                                    <div>

                                        <label for="">SSC Information </label>
                                        <div class="form-group">
                                            <label>School Name</label>
                                            <input name="ssc_school_name" class="form-control" placeholder="School Name" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Passing Year</label>
                                            <input name="ssc_passing_year" class="form-control" placeholder="Passing Year" required>
                                        </div>
                                    </div>

                                        <div>

                                        <label for="">HSC Information </label>
                                        <div class="form-group">
                                            <label>College Name</label>
                                            <input name="hsc_college_name" class="form-control" placeholder="College Name" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Passing Year</label>
                                            <input name="hsc_passing_year" class="form-control" placeholder="Passing Year" required>
                                        </div>
                                    </div>

                                        <div>

                                        <label for="">MBBS Information </label>
                                        <div class="form-group">
                                            <label>College Name</label>
                                            <input name="mbbs_college_name" class="form-control" placeholder="College Name" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Passing Year</label>
                                            <input name="mbbs_passing_year" class="form-control" placeholder="Passing Year" required>
                                        </div>
                                    </div>
                                    <div>

                                        <label for="">FCPS Information </label>
                                        <div class="form-group">
                                            <label>Institute Name</label>
                                            <input name="fcps_inst_name" class="form-control" placeholder="College Name" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Passing Year</label>
                                            <input name="fcps_passing_year" class="form-control" placeholder="Passing Year" required>
                                        </div>
                                    </div>
                                    <div>

                                        <label for="">Others Information </label>
                                        <div class="form-group">
                                            <label>Institute Name</label>
                                            <input name="other_inst_name" class="form-control" placeholder="College Name" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Passing Year</label>
                                            <input name="other_passing_year" class="form-control" placeholder="Passing Year" required>
                                        </div>
                                    </div>




                                        <button type="submit" class="btn btn-primary">Submit</button>
                                        <button type="reset" class="btn btn-default">Reset Button</button>
                                    </div>

                                </form>

                            </div>

                        </div>
                        <!-- /.form-box -->
                        <div>

                        </div>
                    </div>
                </div>
                <!-- right col (We are only adding the ID to make the widgets sortable)-->

                <!-- right col -->
            </div>
            <!-- /.row (main row) -->

        </section>
        <!-- /.content -->
    </div>

<?php
include_once '../include/footer.php';
?>