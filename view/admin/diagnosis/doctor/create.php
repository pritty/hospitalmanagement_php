<?php
include_once '../include/header.php';

?>

<?php
include_once '../include/sidebar.php';

?>


    <div class="content-wrapper">

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Doctor Details</h3>

            </div>

        </div>

        <!-- Main content -->
        <section class="content " style="min-height: 902.8px;">
            <!-- Small boxes (Stat box) -->

            <!-- /.row -->
            <!-- Main row -->
            <div class="row">
                <div class="col-lg-12">
                    <!-- Left col -->
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">Enter Doctor Details</h3>
                        </div>

                        <div class="box-body">
                            <div class="row">
                                <form role="form" action="view/admin/doctor/store.php" method="POST" enctype="multipart/form-data">
                                    <div class="col-md-6">

                                        <div class="form-group">
                                            <label>Doctor ID</label>
                                            <input name="doc_id" class="form-control" placeholder="Doctor Id" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Doctor Name</label>
                                            <input name="name" class="form-control" placeholder="Doctor Name" required>
                                        </div>

                                        <div class="form-group">
                                            <label>Doctor Pic</label>
                                            <input name="image" type="file" class="form-control" placeholder="Doctor Name" required>
                                        </div>

                                        <div class="form-group">
                                            <label>Specialist Option</label>
                                            <select name="specialist_op" id="" class="form-control" required>
                                                <option>Select One</option>
                                                <option value="Medicine">Medicine</option>
                                                <option value="Neurologist">Neurologist</option>
                                                <option value="Gynologist">Gynologist</option>
                                                <option value="Cardiologist">Cardiologist</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Contact No</label>
                                            <input name="contact_no" class="form-control" placeholder="Contact No" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Address</label>
                                            <input name="address" type="" class="form-control" placeholder="Address" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input name="email" type="email" class="form-control" placeholder="Email" required>
                                        </div>

                                        <div class="form-group">
                                            <label>Contact No(office)</label>
                                            <input name="contact_no_office" type="" class="form-control" placeholder="Contact No(office)" required>
                                        </div>

                                        <div class="form-group">
                                            <label>Date of Join</label>
                                            <input name="join_date" type="" class="form-control" placeholder="Date of Join" required>
                                        </div>


                                        <div class="form-group">
                                            <label>Leave Status</label>
                                            <select name="leave_status" id="" class="form-control" required>
                                                <option>Select One</option>
                                                <option value="Leave">Leave</option>
                                                <option value="Present">Present</option>
                                            </select>
                                        </div>

                                        <button type="submit" class="btn btn-primary">Submit</button>
                                        <button type="reset" class="btn btn-default">Reset Button</button>
                                    </div>

                                </form>

                            </div>

                        </div>
                        <!-- /.form-box -->
                        <div>

                        </div>
                    </div>
                </div>
                <!-- right col (We are only adding the ID to make the widgets sortable)-->

                <!-- right col -->
            </div>
            <!-- /.row (main row) -->

        </section>
        <!-- /.content -->
    </div>

<?php
include_once '../include/footer.php';
?>