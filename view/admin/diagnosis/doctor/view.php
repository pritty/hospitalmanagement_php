<?php

include_once '../../../../vendor/autoload.php';
$doctor = new \App\admin\Doctor\Doctor();
$doctor=$doctor->view($_GET['id']);
//var_dump($doctor);

?>
<?php
include_once '../../include/header.php';

?>



    <div class="content-wrapper">

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Doctor Information</h3>

            </div>

        </div>

        <!-- Main content -->
        <section class="content " style="min-height: 902.8px;">
            <!-- Small boxes (Stat box) -->

            <!-- /.row -->
            <!-- Main row -->
            <div class="row">
                <div class="col-lg-12">
                    <!-- Left col -->
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">Details of Doctor</h3>
                        </div>



                        <div class="box-body">


                            <div class="col-sm-6 invoice-col">

                                <b>Doctor ID:</b><?php echo $doctor['doc_id'];?> <br>
                                <br>

                                <b>Name:</b> <?php echo $doctor['name'];?> <br>
                                <b>Doctor Pic:</b>
                                <img src="view/admin/uploads/doctors/<?php echo $doctor['image']?>" width="150" height="150" alt="<?php echo $doctor['name']?>"> <br>
                                <b>Name of Specialist:</b> <?php echo $doctor['specialist_op'];?> <br>
                                <b>Contact No:</b> <?php echo $doctor['contact_no'];?> <br>
                                <b>Address:</b> <?php echo $doctor['address'];?> <br>
                                <b>Email:</b><?php echo $doctor['email'];?> <br>
                                <b>Contact No (office):</b> <?php echo $doctor['contact_no_office'];?> <br>
                                <b>Joining Date:</b> <?php echo $doctor['join_date'];?> <br>
                                <b>Leave Status:</b> <?php echo $doctor['leave_status'];?> <br>

                                <div  style="padding: 4%">
                                    <a class="btn btn-info" href="view/admin/diagnosis/doctor/edit.php?id=<?php echo $doctor['id']?>">Edit</a>
                                    <a class="btn btn-danger" data-toggle="modal" data-target="#myModal" href="" data-id="<?php echo $doctor['id']?>">Delete</a>
                                </div>


                            </div>



                        </div>


                        <!-- /.form-box -->
                        <div>


                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <form action="view/admin/diagnosis/doctor/tmp_delete.php" method="get">
                                        <input id="delete" type="hidden" name="id" value="<?php echo $doctor['id'];?>">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title" id="myModalLabel">Are you sure want to Delete ?</h4>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                                                <button type="submit" class="btn btn-danger">Delete</button>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>


                        </div>
                    </div>
                </div>
                <!-- right col (We are only adding the ID to make the widgets sortable)-->

                <!-- right col -->
            </div>
            <!-- /.row (main row) -->

        </section>
        <!-- /.content -->
    </div>

<?php
include_once '../../include/footer.php';
?>