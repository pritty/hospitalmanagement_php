

<?php
include_once '../../../../vendor/autoload.php';
$doctor = new \App\admin\Doctor\Doctor();
$doctors = $doctor->trash();
//var_dump($doctors);
?>

<?php include_once '../../include/header.php' ?>


<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Menus Tables
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <!-- /.box -->
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Menu Table With Full Features</h3>
                    </div>


                    <div style="position: fixed; right: 35px; top: 100px; z-index: 111">
                        <?php
                        if(isset($_SESSION['msg'])){
                            echo "<div class='alert alert-success'>".$_SESSION['msg']."</div>";
                            session_unset();
                        }
                        if(isset($_SESSION['delete'])){
                            echo "<div class='alert alert-danger'>".$_SESSION['delete']."</div>";
                            session_unset();
                        }
                        if(isset($_SESSION['update'])){
                            echo "<div class='alert alert-info'>".$_SESSION['update']."</div>";
                            session_unset();
                        }


                        ?>
                    </div>


                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Doctor Id</th>
                                <th>Name</th>
                                <th>Image</th>
                                <th>Specialist Option</th>
                                <th>Contact No</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody style="text-align: center">

                            <?php foreach ($doctors as $doctor){?>
                                <tr>
                                    <td><?php echo $doctor['doc_id'];?></td>
                                    <td><?php echo $doctor['name'];?></td>
                                    <td>
                                        <img src="view/admin/uploads/doctors/<?php echo $doctor['image']?>" width="150" height="150" alt="<?php echo $doctor['name']?>">

                                    </td>
                                    <td><?php echo $doctor['specialist_op'];?></td>
                                    <td><?php echo $doctor['contact_no'];?></td>
                                    <td>
                                        <a class="btn btn-info" href="view/admin/diagnosis/doctor/trash_view.php?id=<?php echo $doctor['id']?>">Detail</a>
                                    </td>
                                </tr>

                            <?php }?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->





            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>





<?php include_once '../../include/footer.php' ?>