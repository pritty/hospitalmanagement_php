
<?php

include_once '../../../../vendor/autoload.php';
$doctor = new \App\admin\Doctor\Doctor();
$doctor=$doctor->view($_GET['id']);
//var_dump($doctor);

?>
<?php
include_once '../../include/header.php';

?>




    <div class="content-wrapper">

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Doctor Details</h3>

            </div>

        </div>

        <!-- Main content -->
        <section class="content " style="min-height: 902.8px;">
            <!-- Small boxes (Stat box) -->

            <!-- /.row -->
            <!-- Main row -->
            <div class="row">
                <div class="col-lg-12">
                    <!-- Left col -->
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">Enter Doctor Details</h3>
                        </div>

                        <div class="box-body">
                            <div class="row">
                                <form role="form" action="view/admin/diagnosis/doctor/update.php" method="POST" enctype="multipart/form-data">
                                    <div class="col-md-6">

                                        <div class="form-group">
                                            <label>Doctor ID</label>
                                            <input name="doc_id" class="form-control" value="<?php echo $doctor['doc_id'];?>">
                                            <input name="id" type="hidden" class="form-control" value="<?php echo $doctor['id'];?>">
                                            <input name="image" type="hidden" class="form-control" value="<?php echo $doctor['image'];?>">
                                        </div>
                                        <div class="form-group">
                                            <label>Doctor Name</label>
                                            <input name="name" class="form-control" value="<?php echo $doctor['name'];?>">
                                        </div>

                                        <div class="form-group">
                                            <label>Doctor Pic</label>
                                            <input name="image" type="file" class="form-control" value="<?php echo $doctor['image'];?>">
                                        </div>

                                        <div>
                                            <img src="view/admin/uploads/doctors/<?php echo $doctor['image']?>" width="150" height="150" alt="<?php echo $doctor['name']?>">
                                        </div>

                                        <div class="form-group">
                                            <label>Specialist Option</label>
                                            <select name="specialist_op" id="" class="form-control" required>
                                                <option>Select One</option>
                                                <option <?php echo ($doctor['specialist_op']=='Medicine')? 'selected':'' ?> value="Medicine">Medicine</option>
                                                <option <?php echo ($doctor['specialist_op']=='Neurologist')? 'selected':'' ?> value="Neurologist">Neurologist</option>
                                                <option <?php echo ($doctor['specialist_op']=='Gynologist')? 'selected':'' ?> value="Gynologist">Gynologist</option>
                                                <option <?php echo ($doctor['specialist_op']=='Cardiologist')? 'selected':'' ?> value="Cardiologist">Cardiologist</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Contact No</label>
                                            <input name="contact_no" class="form-control" value="<?php echo $doctor['contact_no'];?>">
                                        </div>
                                        <div class="form-group">
                                            <label>Address</label>
                                            <input name="address" type="" class="form-control" value="<?php echo $doctor['address'];?>">
                                        </div>
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input name="email" type="email" class="form-control" value="<?php echo $doctor['email'];?>">
                                        </div>

                                        <div class="form-group">
                                            <label>Contact No(office)</label>
                                            <input name="contact_no_office" type="" class="form-control" value="<?php echo $doctor['contact_no_office'];?>">
                                        </div>

                                        <div class="form-group">
                                            <label>Date of Join</label>
                                            <input name="join_date" type="" class="form-control" value="<?php echo $doctor['join_date'];?>">
                                        </div>


                                        <div class="form-group">
                                            <label>Leave Status</label>
                                            <select name="leave_status" id="" class="form-control" required>
                                                <option>Select One</option>
                                                <option <?php echo ($doctor['leave_status']=='Leave')? 'selected':'' ?> value="Leave">Leave</option>
                                                <option <?php echo ($doctor['leave_status']=='Present')? 'selected':'' ?> value="Present">Present</option>
                                            </select>
                                        </div>

                                        <button type="submit" class="btn btn-primary">Update</button>

                                    </div>

                                </form>

                            </div>

                        </div>
                        <!-- /.form-box -->
                        <div>

                        </div>
                    </div>
                </div>
                <!-- right col (We are only adding the ID to make the widgets sortable)-->

                <!-- right col -->
            </div>
            <!-- /.row (main row) -->

        </section>
        <!-- /.content -->
    </div>

<?php
include_once '../../include/footer.php';
?>