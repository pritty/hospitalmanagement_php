
<?php
include_once '../../../../vendor/autoload.php';
$doctor = new \App\admin\Doctor\Doctor();
$doctors = $doctor->index();
//var_dump($doctors);

?>
<?php include_once '../../include/header.php' ?>

<div class="content-wrapper" style="">
    <section class="content-header">
        <h1>
            Paitent Registration

        </h1>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- Default box -->
            <div class="col-md-12">

                <div class="box box-primary">
                    <form role="form" action="view/admin/diagnosis/doctor/store.php" method="POST" enctype="multipart/form-data">
                        <div class="col-md-6">

                            <div class="form-group">
                                <label>Doctor ID</label>
                                <input name="doc_id" class="form-control" placeholder="Doctor Id" required>
                            </div>
                            <div class="form-group">
                                <label>Doctor Name</label>
                                <input name="name" class="form-control" placeholder="Doctor Name" required>
                            </div>

                            <div class="form-group">
                                <label>Doctor Pic</label>
                                <input name="image" type="file" class="form-control" placeholder="Doctor Name" required>
                            </div>

                            <div class="form-group">
                                <label>Specialist Option</label>
                                <select name="specialist_op" id="" class="form-control" required>
                                    <option>Select One</option>
                                    <option value="Medicine">Medicine</option>
                                    <option value="Neurologist">Neurologist</option>
                                    <option value="Gynologist">Gynologist</option>
                                    <option value="Cardiologist">Cardiologist</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Contact No</label>
                                <input name="contact_no" class="form-control" placeholder="Contact No" required>
                            </div>
                            <div class="form-group">
                                <label>Address</label>
                                <input name="address" type="" class="form-control" placeholder="Address" required>
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input name="email" type="email" class="form-control" placeholder="Email" required>
                            </div>

                            <div class="form-group">
                                <label>Contact No(office)</label>
                                <input name="contact_no_office" type="" class="form-control" placeholder="Contact No(office)" required>
                            </div>

                            <div class="form-group">
                                <label>Date of Join</label>
                                <input name="join_date" type="" class="form-control" placeholder="Date of Join" required>
                            </div>


                            <div class="form-group">
                                <label>Leave Status</label>
                                <select name="leave_status" id="" class="form-control" required>
                                    <option>Select One</option>
                                    <option value="Leave">Leave</option>
                                    <option value="Present">Present</option>
                                </select>
                            </div>

                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="reset" class="btn btn-default">Reset Button</button>
                        </div>

                    </form>
                    <!-- /.box-footer-->
                </div>
                <!-- /.box -->
            </div>

        </div>

    </section>
    <!-- /.content -->



    <section class="content">
        <div class="row">
            <!-- Default box -->
            <div class="col-md-12">

                <div style="position: fixed; right: 35px; top: 100px; z-index: 111">
                    <?php
                    if(isset($_SESSION['msg'])){
                        echo "<div class='alert alert-success'>".$_SESSION['msg']."</div>";
                        session_unset();
                    }
                    if(isset($_SESSION['delete'])){
                        echo "<div class='alert alert-danger'>".$_SESSION['delete']."</div>";
                        session_unset();
                    }
                    if(isset($_SESSION['update'])){
                        echo "<div class='alert alert-info'>".$_SESSION['update']."</div>";
                        session_unset();
                    }


                    ?>
                </div>

                <div class="box box-primary">

                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Doctor Id</th>
                                <th>Name</th>
                                <th>Image</th>
                                <th>Specialist Option</th>
                                <th>Contact No</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody style="text-align: center">

                            <?php foreach ($doctors as $doctor){?>
                                <tr>
                                    <td><?php echo $doctor['doc_id'];?></td>
                                    <td><?php echo $doctor['name'];?></td>
                                    <td>
                                        <img src="view/admin/uploads/doctors/<?php echo $doctor['image']?>" width="150" height="150" alt="<?php echo $doctor['name']?>">

                                    </td>
                                    <td><?php echo $doctor['specialist_op'];?></td>
                                    <td><?php echo $doctor['contact_no'];?></td>
                                    <td>
                                        <a class="btn btn-info" href="view/admin/diagnosis/doctor/view.php?id=<?php echo $doctor['id']?>">View</a>
                                    </td>
                                </tr>

                            <?php }?>
                            </tbody>
                        </table>
                    </div>


                    <!-- /.box-body -->
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <form action="view/admin/diagnosis/doctor/tmp_delete.php" method="get">
                                <input id="delete" type="hidden" name="id" value="<?php echo $dept['id'];?>">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" id="myModalLabel">Are you sure want to Delete ?</h4>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </div>
                                </div>
                            </form>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.box-footer-->
                </div>
                <!-- /.box -->
            </div>

        </div>

    </section>
</div>
<!-- /.content-wrapper -->
<?php include_once '../../include/footer.php' ?>
