
<?php
include_once '../../../vendor/autoload.php';
$ipr = new \App\admin\IPR\IPR();
$iprs = $ipr->index();
//var_dump($iprs);
?>
<?php include_once '../include/header.php'?>


<div class="content-wrapper" style="">
    <section class="content-header">
        <h1>
            Patient Bill

        </h1>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- Default box -->
            <div class="col-md-12">

                <div class="box box-primary">
                    <form class="form-horizontal" action="view/admin/patient_bill/store.php" method="POST" enctype="multipart/form-data">
                        <div class="box-header with-border">
                            <center>
                                <h2> Patient Closing Bill</h2>
                            </center>
                        </div>

                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12 ">
                                    <div class="form-box6">

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-6 control-label">Registration No</label>
                                                        <div class="col-sm-6">

                                                            <select name="reg_no" id="reg_no" class="form-control js-example-basic-single searchable" data-placeholder="Registration No" style="width:79%!important; background: #23313e;">

                                                                <option value="0">Select One</option>
                                                                <?php foreach ($iprs as $ipr){?>
                                                                    <option value="<?php echo $ipr['reg_no'];?>"> <?php echo $ipr['reg_no'];?> </option>
                                                                <?php }?>

                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-6 control-label">Bed/cabin No</label>
                                                        <div class="col-sm-6">
                                                            <input type="" class="form-control" name="bed_no" id="bed_no" placeholder="Contact No" style="background: #23313e;">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-3 control-label ">Name</label>
                                                    <div class="col-sm-7">
                                                        <input type="" class="form-control" name="name" id="name" placeholder="Name" style="background: #23313e;">
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="col-md-6">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-6 control-label">Registration Date</label>
                                                        <div class="col-sm-6">
                                                            <input type="" class="form-control" name="reg_date" id="reg_date" placeholder="Registration Date" style="background: #23313e;">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-6 control-label">Time</label>
                                                        <div class="col-sm-6">
                                                            <input type="" class="form-control" name="time" id="time" placeholder="Time" style="background: #23313e;">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-3 control-label ">F/H Name</label>
                                                    <div class="col-sm-7">
                                                        <input type="" class="form-control" name="f_h_name" id="f_h_name" placeholder="Name" style="background: #23313e;">
                                                    </div>
                                                </div>

                                            </div>


                                        </div>

                                        <div class="row">
                                            <hr>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-6 control-label ">Registration Fees</label>
                                                    <div class="col-sm-6">
                                                        <input type="" class="form-control" name="reg_fee" id="reg_fee" placeholder="Registration Fees" style="background: #23313e;">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-6 control-label ">Seat Rent (General Bed/Cabin)</label>
                                                    <div class="col-sm-6">
                                                        <input type="" class="form-control" name="srent_fee" id="srent_fee" placeholder="Seat Rent" style="background: #23313e;">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-6 control-label ">Consultation/Medical board Fee</label>
                                                    <div class="col-sm-6">
                                                        <input type="" class="form-control" name="con_fee" id="con_fee" placeholder="Consultation/Medical board Fee" style="background: #23313e;">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-6 control-label ">Operation Charge: Surgeon Fee </label>
                                                    <div class="col-sm-6">
                                                        <input type="" class="form-control" name="op_sur_fee" id="op_sur_fee" placeholder="Surgeon Fee" style="background: #23313e;">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-6 control-label ">Operation Charge: Ass. Surgeon Fee</label>
                                                    <div class="col-sm-6">
                                                        <input type="" class="form-control" name="op_as_fee" id="op_as_fee" placeholder="Ass. Surgeon Fee" style="background: #23313e;">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-6 control-label ">Operation Charge:Anaesthatist Fee</label>
                                                    <div class="col-sm-6">
                                                        <input type="" class="form-control" name="op_ans_fee" id="op_ans_fee" placeholder="Anaesthatist Fee" style="background: #23313e;">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-6 control-label ">Operation Charge:O.T Charge</label>
                                                    <div class="col-sm-6">
                                                        <input type="" class="form-control" name="op_ot_fee" id="op_ot_fee" placeholder="O.T Charge" style="background: #23313e;">
                                                    </div>
                                                </div>


                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label ">Pathology Charge</label>
                                                    <div class="col-sm-8">
                                                        <input type="" class="form-control" name="pathology" id="pathology" placeholder="Pathology Charge" style="background: #23313e;">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label ">E.C.G Charge</label>
                                                    <div class="col-sm-8">
                                                        <input type="" class="form-control" name="ecg_charge" id="ecg_charge" placeholder="E.C.G Charge" style="background: #23313e;">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label ">X-R Charge</label>
                                                    <div class="col-sm-8">
                                                        <input type="" class="form-control" name="x_ray" id="x_ray" placeholder="X-R Charge" style="background: #23313e;">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label ">Oxygen Charge</label>
                                                    <div class="col-sm-8">
                                                        <input type="" class="form-control" name="oxygen" id="oxygen" placeholder="Oxygen Charge" style="background: #23313e;">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label ">Monitoring Charge</label>
                                                    <div class="col-sm-8">
                                                        <input type="" class="form-control" name="monitoring" id="monitoring" placeholder="Monitoring Charge" style="background: #23313e;">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label ">Ambulance Fare</label>
                                                    <div class="col-sm-8">
                                                        <input type="" class="form-control" name="ambulance" id="ambulance" placeholder="Ambulance Fare" style="background: #23313e;">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label ">Dressing/ Stich Remove Charge</label>
                                                    <div class="col-sm-8">
                                                        <input type="" class="form-control" name="dresing" id="dresing" placeholder="Dressing Charge" style="background: #23313e;">
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label ">Blood Transfusion/ Nebulizer Charge</label>
                                                    <div class="col-sm-8">
                                                        <input type="" class="form-control" name="blood" id="blood" placeholder="" style="background: #23313e;">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label ">Ultrasonography Charge</label>
                                                    <div class="col-sm-8">
                                                        <input type="" class="form-control" name="ultra" id="ultra" placeholder="" style="background: #23313e;">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label ">Endoscopy Charge</label>
                                                    <div class="col-sm-8">
                                                        <input type="" class="form-control" name="endoscopy" id="endoscopy" placeholder="" style="background: #23313e;">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label ">Service Charge</label>
                                                    <div class="col-sm-8">
                                                        <input type="" class="form-control" name="service" id="service" placeholder="" style="background: #23313e;">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label ">Medicine Cost</label>
                                                    <div class="col-sm-8">
                                                        <input type="" class="form-control" name="medicine" id="medicine" placeholder="" style="background: #23313e;">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label ">Path. Vat/Nedle/Others Charge</label>
                                                    <div class="col-sm-8">
                                                        <input type="" class="form-control" name="others" id="others" placeholder="" style="background: #23313e;">
                                                    </div>
                                                </div>

                                            </div>

                                        </div>


                                        <div class="row">
                                            <hr>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-6 control-label ">Add 2.25% Vat:</label>
                                                    <div class="col-sm-6">
                                                        <input type="" class="form-control" name="vat" id="vat" placeholder="Registration Fees" style="background: #23313e;">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-6 control-label ">Total:</label>
                                                    <div class="col-sm-6">
                                                        <input type="" class="form-control" name="total" id="total" placeholder="Total" style="background: #23313e;">
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label ">Advance:</label>
                                                    <div class="col-sm-8">
                                                        <input type="" class="form-control" name="advance" id="advance" placeholder="Pathology Charge" style="background: #23313e;">
                                                    </div>
                                                </div>


                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label ">Previous Discount:</label>
                                                    <div class="col-sm-8">
                                                        <input type="" class="form-control" name="discount" id="discount" placeholder="" style="background: #23313e;">
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label ">Balance:</label>
                                                    <div class="col-sm-8">
                                                        <input type="" class="form-control" name="balance" id="balance" placeholder="" style="background: #23313e;">
                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                        <div class="row">

                                            <div class="col-md-6">

                                            </div>


                                            <div class="col-md-6">
                                                <div class="">

                                                    <a class="btn btn-default" style="border: 1px solid black" href="view/admin/patient_daily_expance/index.php?id=<?php echo $ipr['id']?>">  Daily Expanses</a>
                                                    <a class="btn btn-default" style="border: 1px solid black" href="view/admin/patient_daily_expance/index.php?id=<?php echo $ipr['id']?>">  Consultation Entry</a>
                                                    <a class="btn btn-default" style="border: 1px solid black" href="view/admin/patient_daily_expance/index.php?id=<?php echo $ipr['id']?>">  Discount</a>
                                                    <a class="btn btn-default" style="border: 1px solid black" href="view/admin/patient_daily_expance/index.php?id=<?php echo $ipr['id']?>">  Money Receipt</a>

                                                </div>


                                            </div>


                                        </div>

                                        <div class="row">
                                            <hr>
                                            <div class="col-md-8">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-6 control-label ">Close Date:</label>
                                                        <div class="col-sm-6">
                                                            <input type="" class="form-control" name="c_date" id="c_date" placeholder="Close Date" style="background: #23313e;">
                                                        </div>
                                                    </div>


                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-6 control-label ">Close Time:</label>
                                                        <div class="col-sm-6">
                                                            <input type="" class="form-control" name="c_date" id="c_date" placeholder="Close Time" style="background: #23313e;">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-6 control-label ">Guarantor:</label>
                                                        <div class="col-sm-6">
                                                            <select class="form-control" name="vat" id="vat" placeholder="Guarantor" style="background: #23313e;">
                                                                <option value="">Guarantor</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-md-4">
                                                <div class="">

                                                    <a class="btn btn-default" style="border: 1px solid black" href="view/admin/patient_daily_expance/index.php?id=<?php echo $ipr['id']?>">  Close Patient</a>
                                                    <a class="btn btn-default" style="border: 1px solid black" href="view/admin/patient_daily_expance/index.php?id=<?php echo $ipr['id']?>">  Print</a>
                                                    <a class="btn btn-default" style="border: 1px solid black" href="view/admin/patient_daily_expance/index.php?id=<?php echo $ipr['id']?>">  Exit</a>


                                                </div>


                                            </div>



                                        </div>

                                </div>

                            </div>




                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="reset" class="btn btn-default pull-right" style="margin-left:5px; "><i class="fa fa-refresh"></i> Cancel</button>
                            <button  type="submit" class="btn btn-info pull-right"><i class="fa fa-save"></i> Save</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                    <!-- /.box-footer-->
                </div>
                <!-- /.box -->
            </div>

        </div>

    </section>
    <!-- /.content -->



    <section class="content">
        <div class="row">
            <!-- Default box -->
            <div class="col-md-12">

                <div id="fade" style="position: fixed; right: 35px; top: 100px; z-index: 111">
                    <?php
                    if(isset($_SESSION['msg'])){
                        echo "<div class='alert alert-success'>".$_SESSION['msg']."</div>";
                        session_unset();
                    }
                    if(isset($_SESSION['delete'])){
                        echo "<div class='alert alert-danger'>".$_SESSION['delete']."</div>";
                        session_unset();
                    }
                    if(isset($_SESSION['update'])){
                        echo "<div class='alert alert-info'>".$_SESSION['update']."</div>";
                        session_unset();
                    }


                    ?>
                </div>

                <div class="box box-primary">

                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>SL No</th>
                                <th>Reg No</th>
                                <th>Paitent Name</th>
                                <th>Department</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody style="">

                            <?php
                            $sl=1;
                            foreach ($iprs as $ipr){?>
                                <tr>
                                    <td><?php echo $sl++?></td>
                                    <td><?php echo $ipr['reg_no'];?></td>
                                    <td><?php echo $ipr['date_time'];?></td>
                                    <td><?php echo $ipr['consultant'];?></td>

                                    <td>
                                        <a class="btn btn-primary" href="view/admin/patient_admission_ticket/edit.php?id=<?php echo $ipr['id']?>"> <i class="fa fa-edit"></i> Edit</a>
                                        <a class="btn btn-danger" data-toggle="modal" data-target="#myModal" href="" data-id="<?php echo $ipr['id']?>"> <i class="fa fa-trash"></i> Delete</a>
                                    </td>
                                </tr>

                            <?php }?>
                            </tbody>
                        </table>
                    </div>


                    <!-- /.box-body -->
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <form action="view/admin/patient_admission_ticket/tmp_delete.php" method="get">
                                <input id="delete" type="hidden" name="id" value="<?php echo $ipr['id'];?>">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" id="myModalLabel">Are you sure want to Delete ?</h4>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </div>
                                </div>
                            </form>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.box-footer-->
                </div>
                <!-- /.box -->
            </div>

        </div>

    </section>
</div>
<!-- /.content-wrapper -->

<script src="assets/admin2/js/jquery.min.js"></script>
<script src="assets/admin/bower_components/jquery-ui/jquery-ui.min.js"></script>
<script>

    function clearForm(){
        $("#bed_no, #name, #reg_date, #time, #f_h_name").val("");
    }
    var getValues, bed_no, patient_name, reg_date, time, f_h_name, reg_no;
    function makeAjaxRequest(placeId){
        //url: "src/admin/IPR/IPR.php/json",
        $.ajax({
            type: 'POST',
            url: "src/admin/json.php",
            data: {"place_Id": placeId},
            success: function (response) {
                getValues = $.parseJSON(response)[0];
                bed_no = getValues.bed_no;
                patient_name = getValues.paitent_name;
                reg_date = getValues.date_time;
                time = getValues.time;
                f_h_name = getValues.f_or_h_name;
                reg_no = getValues.reg_no;


                $("#bed_no").val(bed_no);
                $("#name").val(patient_name);
                $("#reg_date").val(reg_date);
                $("#time").val(time);
                $("#f_h_name").val(f_h_name);

            },
            error: function () {
                alert("Add Failed");
                return false;
            }
        });

    }

    $("#reg_no").on("change", function(){
        var id = $("#reg_no option:selected").val();
        if (id == "0"){
            clearForm();
        } else {
            makeAjaxRequest(id);
        }
    });
</script>

<script>

    function clearForm(){
        $("#Ecode, #Ename").val("");
    }
    var getValues, Ecode, Ename;
    function makeAjaxRequest2(placeId){
        $("#Ename").val(placeId);
        //url: "src/admin/IPR/IPR.php/json",
//        var td = "";
//        $.ajax({
//            type: 'POST',
//            url: "view/admin/file/exist_list/json_exist_list.php",
//            data: {"place_Id": placeId},
//            success: function (response) {
//                getValues = $.parseJSON(response);
//                $.each(getValues,function (key,value) {
//                    td += "<option value='"+ value.Ecode +"' selected>"+ value.Ename +"</option>";
//                });
//                $("#Ename").empty();
//                $("#Ename").append(td);
//            },
//            error: function () {
//                alert("Add Failed");
//                return false;
//            }
//        });

    }
    function makeAjaxRequest3(placeId){
        $("#Ecode").val(placeId);
    }

    $("#Ecode").on("change", function(){
        var id = $("#Ecode option:selected").val();
        if (id == "0"){
            clearForm();
        } else {
            makeAjaxRequest2(id);
        }
    });
    $("#Ename").on("change", function(){
        var id = $("#Ename option:selected").val();
        if (id == "0"){
            clearForm();
        } else {
            makeAjaxRequest3(id);
        }
    });
</script>



<script>
    var getValues, reg_fee, srent_fee, con_fee, op_sur_fee, op_as_fee, op_ans_fee, reg_no;
    $(document).ready(function () {

        $("#reg_no").on("change", function(){
            var id = $('#reg_no').val();

            if(id != ''){
                $.ajax({
                    url:"view/admin/patient_bill/fetch.php",
                    type: 'POST',
                    data:{id:id},
                    dataType : "JSON",
                    success: function (data) {

                        $("#date").text(data.date);

                        getValues = $.parseJSON(data)[0];
                        reg_fee = getValues.reg_fee;
                        srent_fee = getValues.srent_fee;
                        con_fee = getValues.con_fee;
                        op_sur_fee = getValues.op_sur_fee;
                        op_as_fee = getValues.op_as_fee;
                        op_ans_fee = getValues.op_ans_fee;
                        reg_no = getValues.reg_no;


                        $("#reg_fee").val(reg_fee);
                        $("#srent_fee").val(srent_fee);
                        $("#con_fee").val(con_fee);
                        $("#op_sur_fee").val(op_sur_fee);
                        $("#op_as_fee").val(op_as_fee);
                        $("#op_ans_fee").val(op_ans_fee);
                    },
                    error: function () {
                        alert("Add Failed");
                        return false;
                    }
                })
            }

            else{
                clearForm();
            }
        });
    });
</script>



<script>
    $("#date").datepicker(

        {
            dateFormat: "mm/dd/yy",
            showOtherMonths: true,
            selectOtherMonths: true,
            autoclose: true,
            changeMonth: true,
            changeYear: true
            //gotoCurrent: true
        }).datepicker("setDate", "0");

</script>
<?php include_once '../include/footer.php'?>
