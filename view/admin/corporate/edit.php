
<?php
include_once '../../../vendor/autoload.php';
$corporate = new \App\admin\Corporate\Corporate();
$corporate=$corporate->view($_GET['id']);
//var_dump($bed);
?>
<?php include_once '../include/header.php'?>

<div class="content-wrapper" style="">
    <section class="content-header">
        <h1>
            Corporate Registration

        </h1>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- Default box -->
            <div class="col-md-12">

                <div class="box box-primary">
                    <form class="form-horizontal" action="view/admin/corporate/update.php" method="POST" enctype="multipart/form-data">
                        <div class="box-header with-border">

                        </div>

                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-box6">

                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label ">Client ID</label>
                                                    <div class="col-sm-6">
                                                        <input type="" class="form-control" name="c_id" value="<?php echo $corporate['c_id']?>">
                                                        <input name="id" class="form-control" type="hidden" value="<?php echo $corporate['id']?>" >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label ">Company Name</label>
                                                    <div class="col-sm-8">
                                                        <input type="" class="form-control" name="name" value="<?php echo $corporate['name']?>">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Address</label>
                                                    <div class="col-sm-8">
                                                        <input type="" class="form-control" name="address" value="<?php echo $corporate['address']?>">
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Contact No</label>
                                                    <div class="col-sm-6">
                                                        <input type="" class="form-control" name="contact_no" value="<?php echo $corporate['contact_no']?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Email</label>
                                                    <div class="col-sm-6">
                                                        <input type="email" class="form-control" name="email" value="<?php echo $corporate['email']?>">
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="col-md-4">

                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Contact Person One</label>
                                                    <div class="col-sm-8">
                                                        <input type="" class="form-control" name="contact_person" value="<?php echo $corporate['contact_person']?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Contact Person Two</label>
                                                    <div class="col-sm-8">
                                                        <input type="" class="form-control" name="contact_person2" value="<?php echo $corporate['contact_person2']?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-sm-4 control-label">Balance</label>
                                                    <div class="col-sm-6">
                                                        <input type="" class="form-control" name="balance" value="<?php echo $corporate['balance']?>">
                                                    </div>
                                                </div>
                                            </div>

                                        </div>


                                    </div>

                                </div>

                            </div>




                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <a href="view/admin/corporate/index.php" class="btn btn-default pull-right" style="margin-left:5px; "><i class="fa fa-refresh"></i> Cancel</a>
                            <button  type="submit" class="btn btn-info pull-right"><i class="fa fa-save"></i> Save</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                    <!-- /.box-footer-->
                </div>
                <!-- /.box -->
            </div>

        </div>

    </section>
    <!-- /.content -->

</div>
<!-- /.content-wrapper -->
<?php include_once '../include/footer.php'?>
