<?php
include_once '../include/header.php';

?>

<?php
include_once '../include/sidebar.php';

?>


    <div class="content-wrapper">

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Bed Cabin</h3>

            </div>

        </div>

        <!-- Main content -->
        <section class="content " style="min-height: 902.8px;">
            <!-- Small boxes (Stat box) -->

            <!-- /.row -->
            <!-- Main row -->
            <div class="row">
                <div class="col-lg-12">
                    <!-- Left col -->
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">Enter Bed Cabin</h3>
                        </div>

                        <div class="box-body">
                            <div class="row">
                                <form role="form" action="view/admin/corporate/store.php" method="POST" enctype="multipart/form-data">
                                    <div class="col-md-6">

                                        <div class="form-group">
                                            <label>Corporate</label>
                                            <input name="c_id" class="form-control" placeholder="Corporate Id" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Company Name</label>
                                            <input name="name" class="form-control" placeholder="Company Name" required>
                                        </div>

                                        <div class="form-group">
                                            <label>Address</label>
                                            <input name="address" class="form-control" placeholder="Address" required>
                                        </div>

                                        <div class="form-group">
                                            <label>Contact No</label>

                                            <input name="contact_no" class="form-control" placeholder="Contact No" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input name="email" type="email" class="form-control" placeholder="Email" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Contact Person One</label>
                                            <input name="contact_person" type="" class="form-control" placeholder="Contact Person One" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Contact Person Two</label>
                                            <input name="contact_person2" type="" class="form-control" placeholder="Contact Person Two" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Balance</label>
                                            <input name="balance" type="" class="form-control" placeholder="Balance" required>
                                        </div>

                                        <button type="submit" class="btn btn-primary">Submit</button>
                                        <button type="reset" class="btn btn-default">Reset Button</button>
                                    </div>

                                </form>

                            </div>

                        </div>
                        <!-- /.form-box -->
                        <div>

                        </div>
                    </div>
                </div>
                <!-- right col (We are only adding the ID to make the widgets sortable)-->

                <!-- right col -->
            </div>
            <!-- /.row (main row) -->

        </section>
        <!-- /.content -->
    </div>

<?php
include_once '../include/footer.php';
?>