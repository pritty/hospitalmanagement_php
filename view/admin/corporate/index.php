
<?php
include_once '../../../vendor/autoload.php';
$corporate = new \App\admin\Corporate\Corporate();
$corporates = $corporate->index();
?>
<?php include_once '../include/header.php'?>

<div class="content-wrapper" style="">
    <section class="content-header">
        <h1>
            Corporate Client Setup

        </h1>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- Default box -->
            <div class="col-md-12">

                <div class="box box-primary">
                    <form class="form-horizontal" action="view/admin/corporate/store.php" method="POST" enctype="multipart/form-data">
                        <div class="box-header with-border">

                        </div>

                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-box6">

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-4 control-label ">Client ID</label>
                                                        <div class="col-sm-6">
                                                            <input type="" class="form-control" name="c_id" placeholder="Client ID">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-4 control-label ">Company Name</label>
                                                        <div class="col-sm-8">
                                                            <input type="" class="form-control" name="name" placeholder="Company Name">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="" class="col-sm-4 control-label">Address</label>
                                                        <div class="col-sm-8">
                                                            <input type="" class="form-control" name="address" placeholder="Address">
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-4 control-label">Contact No</label>
                                                        <div class="col-sm-6">
                                                            <input type="" class="form-control" name="contact_no" placeholder="Contact No">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-4 control-label">Email</label>
                                                        <div class="col-sm-6">
                                                            <input type="email" class="form-control" name="email" placeholder="Email">
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="col-md-4">

                                                    <div class="form-group">
                                                        <label for="" class="col-sm-4 control-label">Contact Person One</label>
                                                        <div class="col-sm-8">
                                                            <input type="" class="form-control" name="contact_person" placeholder="Contact Person One">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-4 control-label">Contact Person Two</label>
                                                        <div class="col-sm-8">
                                                            <input type="" class="form-control" name="contact_person2" placeholder="Contact Person Two">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-4 control-label">Balance</label>
                                                        <div class="col-sm-6">
                                                            <input type="" class="form-control" name="balance" placeholder="Balance">
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>


                                    </div>

                                </div>

                            </div>




                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="reset" class="btn btn-default pull-right" style="margin-left:5px; "><i class="fa fa-refresh"></i> Cancel</button>
                            <button  type="submit" class="btn btn-info pull-right"><i class="fa fa-save"></i> Save</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                    <!-- /.box-footer-->
                </div>
                <!-- /.box -->
            </div>

        </div>

    </section>
    <!-- /.content -->



    <section class="content">
        <div class="row">
            <!-- Default box -->
            <div class="col-md-12">

                <div style="position: fixed; right: 35px; top: 100px; z-index: 111">
                    <?php
                    if(isset($_SESSION['msg'])){
                        echo "<div class='alert alert-success'>".$_SESSION['msg']."</div>";
                        session_unset();
                    }
                    if(isset($_SESSION['delete'])){
                        echo "<div class='alert alert-danger'>".$_SESSION['delete']."</div>";
                        session_unset();
                    }
                    if(isset($_SESSION['update'])){
                        echo "<div class='alert alert-info'>".$_SESSION['update']."</div>";
                        session_unset();
                    }


                    ?>
                </div>

                <div class="box box-primary">

                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>SL No</th>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Address</th>
                                <th>Contact No</th>
                                <th>Email</th>
                                <th>Contact person(1)</th>
                                <th>Contact person(2)</th>
                                <th>Balance</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php
                            $sl=1;
                            foreach ($corporates as $corporate){?>
                                <tr>
                                    <td><?php echo $sl++;?></td>
                                    <td><?php echo $corporate['c_id'];?></td>
                                    <td><?php echo $corporate['name'];?></td>
                                    <td><?php echo $corporate['address'];?></td>
                                    <td><?php echo $corporate['contact_no'];?></td>
                                    <td><?php echo $corporate['email'];?></td>
                                    <td><?php echo $corporate['contact_person'];?></td>
                                    <td><?php echo $corporate['contact_person2'];?></td>
                                    <td><?php echo $corporate['balance'];?></td>
                                    <td>
                                        <a class="btn btn-info" href="view/admin/corporate/edit.php?id=<?php echo $corporate['id']?>">Edit</a>
                                        <a class="btn btn-danger" data-toggle="modal" data-target="#myModal" href="" data-id="<?php echo $corporate['id']?>">Delete</a>
                                    </td>
                                </tr>

                            <?php }?>
                            </tbody>
                        </table>
                    </div>


                    <!-- /.box-body -->
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <form action="view/admin/corporate/tmp_delete.php" method="get">
                                <input id="delete" type="hidden" name="id" value="<?php echo $corporate['id'];?>">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" id="myModalLabel">Are you sure want to Delete ?</h4>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </div>
                                </div>
                            </form>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.box-footer-->
                </div>
                <!-- /.box -->
            </div>

        </div>

    </section>
</div>
<!-- /.content-wrapper -->
<?php include_once '../include/footer.php'?>
