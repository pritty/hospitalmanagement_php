
<?php
include_once '../../../vendor/autoload.php';
$p_money_recipt = new \App\admin\Patient_money_recipt\Patient_money_recipt();
$p_money_recipts = $p_money_recipt->trash();
//var_dump($p_money_recipts);
?>
<?php include_once '../include/header.php'?>

<div class="content-wrapper" style="">
    <section class="content-header">
        <h1>
            Patient Money Recipt

        </h1>

    </section>

    <!-- Main content -->

    <!-- /.content -->



    <section class="content">
        <div class="row">
            <!-- Default box -->
            <div class="col-md-12">

                <div style="position: fixed; right: 35px; top: 100px; z-index: 111">
                    <?php
                    if(isset($_SESSION['msg'])){
                        echo "<div class='alert alert-success'>".$_SESSION['msg']."</div>";
                        session_unset();
                    }
                    if(isset($_SESSION['delete'])){
                        echo "<div class='alert alert-danger'>".$_SESSION['delete']."</div>";
                        session_unset();
                    }
                    if(isset($_SESSION['update'])){
                        echo "<div class='alert alert-info'>".$_SESSION['update']."</div>";
                        session_unset();
                    }


                    ?>
                </div>

                <div class="box box-primary">

                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>SL No</th>
                                <th>Invoice No</th>
                                <th>Reg No</th>
                                <th>Name</th>
                                <th>Bed No</th>
                                <th>Balance</th>
                                <th>Paid Amount</th>
                                <th>Date</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody style="">

                            <?php
                            $sl=1;
                            foreach ($p_money_recipts as $p_money_recipt){?>
                                <tr>
                                    <td><?php echo $sl++?></td>
                                    <td><?php echo $p_money_recipt['invc_no'];?></td>
                                    <td><?php echo $p_money_recipt['reg_no'];?></td>
                                    <td><?php echo $p_money_recipt['name'];?></td>
                                    <td><?php echo $p_money_recipt['bed_no'];?></td>
                                    <td><?php echo $p_money_recipt['balance'];?></td>
                                    <td><?php echo $p_money_recipt['amount'];?></td>
                                    <td><?php echo $p_money_recipt['date'];?></td>

                                    <td>
                                        <a class="btn btn-primary" href="view/admin/patient_money_recipt/trash_view.php?id=<?php echo $p_money_recipt['id']?>"> <i class="fa fa-edit"></i> View</a>
                                        <a class="btn btn-info" href="view/admin/patient_money_recipt/restore.php?id=<?php echo $p_money_recipt['id']?>">Restore</a>
                                        <a class="btn btn-danger" data-toggle="modal" data-target="#myModal" href="" data-id="<?php echo $p_money_recipt['id']?>"> <i class="fa fa-trash"></i> Delete</a>
                                    </td>
                                </tr>

                            <?php }?>
                            </tbody>
                        </table>
                    </div>


                    <!-- /.box-body -->
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <form action="view/admin/patient_money_recipt/delete.php" method="get">
                                <input id="delete" type="hidden" name="id" value="<?php echo $p_money_recipt['id'];?>">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" id="myModalLabel">Are you sure want to Delete ?</h4>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </div>
                                </div>
                            </form>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.box-footer-->
                </div>
                <!-- /.box -->
            </div>

        </div>

    </section>
</div>
<!-- /.content-wrapper -->
<?php include_once '../include/footer.php'?>
