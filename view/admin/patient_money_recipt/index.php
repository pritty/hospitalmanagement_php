
<?php
include_once '../../../vendor/autoload.php';
$p_money_recipt = new \App\admin\Patient_money_recipt\Patient_money_recipt();
$p_money_recipts = $p_money_recipt->index();
//var_dump($p_money_recipts);

$ipr = new \App\admin\IPR\IPR();
$iprs = $ipr->index();
 //$ipr->json('555');
//var_dump($iprs);

?>
<?php include_once '../include/header.php'?>

<div class="content-wrapper" style="">
    <section class="content-header">
        <h1>
             Patient Money Recipt

        </h1>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- Default box -->
            <div class="col-md-12">

                <div class="box box-primary">
                    <form class="form-horizontal" action="view/admin/patient_money_recipt/store.php" method="POST" enctype="multipart/form-data">
                        <div class="box-header with-border">
                            <center>
                                <h2>Patient Money Recipt</h2>
                            </center>
                        </div>

                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-10 col-md-offset-2">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="form-box6">


                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-4 control-label">Invoice No:</label>
                                                            <div class="col-sm-8">
                                                                <input type="" class="form-control" name="invc_no" placeholder="Invoice No" autofocus>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-4 control-label">Select Invoice:</label>
                                                            <div class="col-sm-8">
                                                                <select name="s_invc" id="" class="form-control">
                                                                    <option value="">Select Invoice</option>
                                                                    <option value="Cash">Cash</option>
                                                                    <option value="Cheque">Cheque</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-4 control-label">Reg. No</label>
                                                            <div class="col-sm-8">
                                                                <select name="reg_no" id="fuzzOptionsList" class="form-control js-example-basic-single searchable" data-placeholder="Select One">

                                                                    <option value="0">Select One</option>
                                                                    <?php foreach ($iprs as $ipr){?>
                                                                        <option value="<?php echo $ipr['reg_no'];?>"> <?php echo $ipr['reg_no'];?> </option>
                                                                    <?php }?>

                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-4 control-label">Bed/Cabin No</label>
                                                            <div class="col-sm-8">
                                                                <input type="" class="form-control" name="bed_no" id="bed_no" placeholder="Cabin" >
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group  has-feedback">
                                                            <label for="" class="col-sm-4 control-label">Date</label>
                                                            <div class="col-sm-8">
                                                                <input type="" class="form-control" name="date" id="date" placeholder="Date">
                                                                <span class="glyphicon glyphicon-calendar form-control-feedback"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group has-feedback input-group bootstrap-timepicker timepicker">
                                                            <label for="" class="col-sm-4 control-label">Time</label>
                                                            <div class="col-sm-8">
                                                                <input type="" class="form-control" name="time" id="time" data-provide="timepicker" data-template="dropdown" data-minute-step="1" placeholder="Time">
                                                                <span class="glyphicon glyphicon-time form-control-feedback"></span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-3 control-label">Name</label>
                                                            <div class="col-sm-8">
                                                                <input type="" class="form-control" name="name" id="name" placeholder="Name" autofocus>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>


                                                <div class="row">
                                                    <hr>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-6 control-label">Total</label>
                                                            <div class="col-sm-6">
                                                                <input type="" class="form-control" name="total" placeholder="Total" >
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-6 control-label">Discount</label>
                                                            <div class="col-sm-6">
                                                                <input type="" class="form-control" name="discount" placeholder="Discount">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-6 control-label">Advance</label>
                                                            <div class="col-sm-6">
                                                                <input type="" class="form-control" name="advance" placeholder="Advance" >
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-6 control-label">Balance</label>
                                                            <div class="col-sm-6">
                                                                <input type="" class="form-control" name="balance" placeholder="Balance">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <hr>
                                                        <div class="col-xs-2">
                                                            <label for="" class="">Type:</label>
                                                            <select name="chktype" id="chktype" class="form-control">
                                                                <option value="0">Select One</option>
                                                                <option value="Cash">Cash</option>
                                                                <option value="Cheque">Cheque</option>
                                                            </select>
                                                        </div>

                                                        <div class="col-xs-3">
                                                            <label for="" class="">Bank Name:</label>
                                                            <input class="form-control" name="bank_name" id="bank_name" placeholder="Bank Name" type="text">
                                                        </div>
                                                        <div class="col-xs-3">
                                                            <label for="" class="">Cheque No:</label>
                                                            <input class="form-control" name="chq_no" id="chq_no" placeholder="Cheque No" type="text">
                                                        </div>
                                                        <div class="col-xs-2">
                                                            <label for="" class="">Cq. Date:</label>
                                                            <input class="form-control" name="chq_date" id="chq_date" placeholder="Cq. Date" type="text">
                                                        </div>
                                                        <div class="col-xs-2">
                                                            <label for="" class="">Paid Amount:</label>
                                                            <input class="form-control" name="amount" id="amount" placeholder="Amount" type="text">
                                                        </div>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>




                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="reset" class="btn btn-default pull-right" style="margin-left:5px; "><i class="fa fa-refresh"></i> Cancel</button>
                            <button  type="submit" class="btn btn-success pull-right"><i class="fa fa-save"></i> Save</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                    <!-- /.box-footer-->
                </div>
                <!-- /.box -->
            </div>

        </div>

    </section>
    <!-- /.content -->



    <section class="content">
        <div class="row">
            <!-- Default box -->
            <div class="col-md-12">

                <div style="position: fixed; right: 35px; top: 100px; z-index: 111">
                    <?php
                    if(isset($_SESSION['msg'])){
                        echo "<div class='alert alert-success'>".$_SESSION['msg']."</div>";
                        session_unset();
                    }
                    if(isset($_SESSION['delete'])){
                        echo "<div class='alert alert-danger'>".$_SESSION['delete']."</div>";
                        session_unset();
                    }
                    if(isset($_SESSION['update'])){
                        echo "<div class='alert alert-info'>".$_SESSION['update']."</div>";
                        session_unset();
                    }


                    ?>
                </div>

                <div class="box box-primary">

                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>SL No</th>
                                <th>Invoice No</th>
                                <th>Reg No</th>
                                <th>Name</th>
                                <th>Bed No</th>
                                <th>Balance</th>
                                <th>Paid Amount</th>
                                <th>Date</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody style="">

                            <?php
                            $sl=1;
                            foreach ($p_money_recipts as $p_money_recipt){?>
                                <tr>
                                    <td><?php echo $sl++?></td>
                                    <td><?php echo $p_money_recipt['invc_no'];?></td>
                                    <td><?php echo $p_money_recipt['reg_no'];?></td>
                                    <td><?php echo $p_money_recipt['name'];?></td>
                                    <td><?php echo $p_money_recipt['bed_no'];?></td>
                                    <td><?php echo $p_money_recipt['balance'];?></td>
                                    <td><?php echo $p_money_recipt['amount'];?></td>
                                    <td><?php echo $p_money_recipt['date'];?></td>

                                    <td>
                                        <a class="btn btn-primary" href="view/admin/patient_money_recipt/edit.php?id=<?php echo $p_money_recipt['id']?>"> <i class="fa fa-edit"></i> Edit</a>
                                        <a class="btn btn-danger" data-toggle="modal" data-target="#myModal" href="" data-id="<?php echo $p_money_recipt['id']?>"> <i class="fa fa-trash"></i> Delete</a>
                                        <a type="" href="view/admin/patient_money_recipt/pdf.php" class="btn btn-info"><i class="fa fa-download"></i> Download</a>
                                    </td>
                                </tr>

                            <?php }?>
                            </tbody>
                        </table>
                    </div>


                    <!-- /.box-body -->
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <form action="view/admin/patient_money_recipt/tmp_delete.php" method="get">
                                <input id="delete" type="hidden" name="id" value="<?php echo $p_money_recipt['id'];?>">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" id="myModalLabel">Are you sure want to Delete ?</h4>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </div>
                                </div>
                            </form>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.box-footer-->
                </div>
                <!-- /.box -->
            </div>

        </div>

    </section>
</div>



<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>-->
<script src="assets/admin2/js/jquery-3.2.1.min.js"></script>
<script src="assets/admin/bower_components/jquery-ui/jquery-ui.min.js"></script>
<script>

    function clearForm(){
        $("#bed_no, #name").val("");
    }
var getValues, bed_no, patient_name, reg_no;
    function makeAjaxRequest(placeId){
        //url: "src/admin/IPR/IPR.php/json",
        $.ajax({
            type: 'POST',
            url: "src/admin/json.php",
            data: {"place_Id": placeId},
            success: function (response) {
              getValues = $.parseJSON(response)[0];
                bed_no = getValues.bed_no;
                patient_name = getValues.paitent_name;
                reg_no = getValues.reg_no;

                $("#bed_no").val(bed_no);
                $("#name").val(patient_name);

            },
            error: function () {
                alert("Add Failed");
                return false;
            }
        });

    }


    $("#fuzzOptionsList").on("change", function(){
        var id = $("#fuzzOptionsList option:selected").val();
        if (id == "0"){
            clearForm();
        } else {
            makeAjaxRequest(id);
        }
    });


</script>



<script>
    $("#chktype").on("change" ,function() {
        var id = $("#chktype option:selected").val();
        if(id == "Cash"){
            $("#bank_name").prop('disabled', true);
            $("#chq_no").prop('disabled', true);
            $("#chq_date").prop('disabled', true);
        } else {
            $("#bank_name").prop('disabled', false);
            $("#chq_no").prop('disabled', false);
            $("#chq_date").prop('disabled', false);
        }
    });
</script>


<script>
    $("#date").datepicker(

        {
            dateFormat: "mm/dd/yy",
            showOtherMonths: true,
            selectOtherMonths: true,
            autoclose: true,
            changeMonth: true,
            changeYear: true
            //gotoCurrent: true,
        }).datepicker("setDate", "0");

</script>


<script>

    $(document).ready(function () {
        $('#time').timepicker({
            showInputs: false
        });
    });

</script>
<!-- /.content-wrapper -->
<?php include_once '../include/footer.php'?>
