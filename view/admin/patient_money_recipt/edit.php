
<?php
include_once '../../../vendor/autoload.php';
$p_money_recipt = new \App\admin\Patient_money_recipt\Patient_money_recipt();
$p_money_recipt = $p_money_recipt->view($_GET['id']);
//var_dump($p_money_recipt);
?>
<?php include_once '../include/header.php'?>

<div class="content-wrapper" style="">
    <section class="content-header">
        <h1>
            Patient Money Recipt

        </h1>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- Default box -->
            <div class="col-md-12">

                <div class="box box-primary">
                    <form class="form-horizontal" action="view/admin/patient_money_recipt/update.php" method="POST" enctype="multipart/form-data">
                        <div class="box-header with-border">
                            <center>
                                <h2>Patient Money Recipt</h2>
                            </center>
                        </div>

                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-10 col-md-offset-2">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="form-box6">


                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-4 control-label">Invoice No:</label>
                                                            <div class="col-sm-8">
                                                                <input type="" class="form-control" name="invc_no" value="<?php echo $p_money_recipt['invc_no']; ?>" autofocus>
                                                                <input type="hidden" class="form-control" name="id" value="<?php echo $p_money_recipt['id']; ?>" >
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-4 control-label">Select Invoice:</label>
                                                            <div class="col-sm-8">
                                                                <select name="s_invc" id="" class="form-control">
                                                                    <option <?php echo ($p_money_recipt['s_invc']=='Cash')? 'selected':'' ?> value="Cash">Cash</option>
                                                                    <option <?php echo ($p_money_recipt['s_invc']=='Cheque')? 'selected':'' ?> value="Cheque">Cheque</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-5 control-label">Reg. No</label>
                                                            <div class="col-sm-7">
                                                                <input type="" class="form-control" name="reg_no" value="<?php echo $p_money_recipt['reg_no']; ?>" autofocus>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-5 control-label">Bed/Cabin No</label>
                                                            <div class="col-sm-7">
                                                                <input type="" class="form-control" name="bed_no" value="<?php echo $p_money_recipt['bed_no']; ?>" >
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-5 control-label">Date</label>
                                                            <div class="col-sm-7">
                                                                <input type="" class="form-control" name="date" value="<?php echo $p_money_recipt['date']; ?>" >
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-5 control-label">Time</label>
                                                            <div class="col-sm-7">
                                                                <input type="" class="form-control" name="time" value="<?php echo $p_money_recipt['time']; ?>" >
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-3 control-label">Name</label>
                                                            <div class="col-sm-8">
                                                                <input type="" class="form-control" name="name" value="<?php echo $p_money_recipt['name']; ?>" >
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>


                                                <div class="row">
                                                    <hr>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-6 control-label">Total</label>
                                                            <div class="col-sm-6">
                                                                <input type="" class="form-control" name="total" value="<?php echo $p_money_recipt['total']; ?>" >
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-6 control-label">Discount</label>
                                                            <div class="col-sm-6">
                                                                <input type="" class="form-control" name="discount" value="<?php echo $p_money_recipt['discount']; ?>">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-6 control-label">Advance</label>
                                                            <div class="col-sm-6">
                                                                <input type="" class="form-control" name="advance" value="<?php echo $p_money_recipt['advance']; ?>" >
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-6 control-label">Balance</label>
                                                            <div class="col-sm-6">
                                                                <input type="" class="form-control" name="balance" value="<?php echo $p_money_recipt['balance']; ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <hr>
                                                    <div class="col-xs-2">
                                                        <label for="" class="">Type:</label>
                                                        <select name="chktype" id="" class="form-control">

                                                            <option <?php echo ($p_money_recipt['chktype']=='Cash')? 'selected': ''; ?> value="Cash">Cash</option>
                                                            <option <?php echo ($p_money_recipt['chktype']=='Cheque')? 'selected':''; ?> value="Cheque">Cheque</option>
                                                        </select>
                                                    </div>

                                                    <div class="col-xs-3">
                                                        <label for="" class="">Bank Name:</label>
                                                        <input class="form-control" name="bank_name" value="<?php echo $p_money_recipt['bank_name']; ?>" type="text">
                                                    </div>
                                                    <div class="col-xs-3">
                                                        <label for="" class="">Cheque No:</label>
                                                        <input class="form-control" name="chq_no" value="<?php echo $p_money_recipt['chq_no']; ?>" type="text">
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <label for="" class="">Cq. Date:</label>
                                                        <input class="form-control" name="chq_date" value="<?php echo $p_money_recipt['chq_date']; ?>" type="text">
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <label for="" class="">Paid Amount:</label>
                                                        <input class="form-control" name="amount" value="<?php echo $p_money_recipt['amount']; ?>" type="text">
                                                    </div>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>




                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <a href="view/admin/patient_money_recipt/index.php" class="btn btn-default pull-right" style="margin-left:5px; "><i class="fa fa-refresh"></i> Cancel</a>
                            <button  type="submit" class="btn btn-info pull-right"><i class="fa fa-save"></i> Update</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                    <!-- /.box-footer-->
                </div>
                <!-- /.box -->
            </div>

        </div>

    </section>
    <!-- /.content -->




</div>
<!-- /.content-wrapper -->
<?php include_once '../include/footer.php'?>
