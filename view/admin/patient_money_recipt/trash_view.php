<?php

include_once '../../../vendor/autoload.php';
$p_money_recipt = new \App\admin\Patient_money_recipt\Patient_money_recipt();
$p_money_recipt=$p_money_recipt->view($_GET['id']);
//var_dump($p_money_recipt);

?>
<?php
include_once '../include/header.php';

?>


    <div class="content-wrapper">

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">  Patient Money Recipt</h3>

            </div>

        </div>

        <!-- Main content -->
        <section class="content " style="min-height: 902.8px;">
            <!-- Small boxes (Stat box) -->

            <!-- /.row -->
            <!-- Main row -->
            <div class="row">
                <div class="col-lg-12">
                    <!-- Left col -->
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">Details of Patient Money Recipt</h3>
                        </div>



                        <div class="box-body">


                            <div class="col-sm-6 invoice-col">

                                <b>Invoice No:</b> <?php echo $p_money_recipt['invc_no'];?> <br>
                                <b>Select Invoice:</b><?php echo $p_money_recipt['s_invc'];?> <br>
                                <b>Reg. No:</b> <?php echo $p_money_recipt['reg_no'];?> <br>
                                <b>Bed/Cabin No:</b> <?php echo $p_money_recipt['bed_no'];?> <br>
                                <b>Date:</b> <?php echo $p_money_recipt['date'];?> <br>
                                <b>Time:</b> <?php echo $p_money_recipt['time'];?> <br>
                                <b>Name:</b> <?php echo $p_money_recipt['name'];?> <br>
                                <b>Total:</b> <?php echo $p_money_recipt['total'];?> <br>
                                <b>Discount:</b> <?php echo $p_money_recipt['discount'];?> <br>
                                <b>Advance:</b> <?php echo $p_money_recipt['advance'];?> <br>
                                <b>Balance:</b> <?php echo $p_money_recipt['balance'];?> <br>
                                <b>Type:</b> <?php echo $p_money_recipt['chktype'];?> <br>
                                <b>Bank Name:</b> <?php echo $p_money_recipt['bank_name'];?> <br>
                                <b>Cheque No:</b> <?php echo $p_money_recipt['chq_no'];?> <br>
                                <b>Cq. Date:</b> <?php echo $p_money_recipt['chq_date'];?> <br>
                                <b>Paid Amount:</b> <?php echo $p_money_recipt['amount'];?> <br>









                                <div  style="padding: 4%">
                                    <a class="btn btn-info" href="view/admin/patient_money_recipt/restore.php?id=<?php echo $p_money_recipt['id']?>">Restore</a>
                                    <a class="btn btn-danger" data-toggle="modal" data-target="#myModal" href="" data-id="<?php echo $p_money_recipt['id']?>">Delete</a>
                                </div>


                            </div>



                        </div>


                        <!-- /.form-box -->
                        <div>


                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <form action="view/admin/patient_money_recipt/delete.php" method="get">
                                        <input id="delete" type="hidden" name="id" value="<?php echo $p_money_recipt['id'];?>">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title" id="myModalLabel">Are you sure want to Delete ?</h4>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                                                <button type="submit" class="btn btn-danger">Delete</button>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>


                        </div>
                    </div>
                </div>
                <!-- right col (We are only adding the ID to make the widgets sortable)-->

                <!-- right col -->
            </div>
            <!-- /.row (main row) -->

        </section>
        <!-- /.content -->
    </div>

<?php
include_once '../include/footer.php';
?>